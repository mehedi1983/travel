﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Travel.Data.Entities.Core;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Identity;

namespace Travel.Data.Entities.B2B
{
    public class B2BInformation : AuditableEntity
    {
        public B2BInformation()
        {
            ApplicationUsers = new HashSet<ApplicationUser>();
        }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(128)]
        public string CountryId { get; set; }

        [StringLength(128)]
        public string CityId { get; set; }

        [StringLength(50)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string ContactPerson { get; set; }

        [StringLength(50)]
        public string ContactPersonDesignation { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string MobileNumber { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }
        
        [StringLength(100)]
        public string Website { get; set; }

        [StringLength(100)]
        public string NatureOfBusiness { get; set; }

        [StringLength(100)]
        public string LicenseNo { get; set; }

        [StringLength(200)]
        public string LicenseCopy { get; set; }

        [StringLength(200)]
        public string Logo { get; set; }
        
        [StringLength(50)]
        public string Status { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }

    }
}