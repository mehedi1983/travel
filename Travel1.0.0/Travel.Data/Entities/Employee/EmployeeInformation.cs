﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Travel.Data.Entities.Core;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Identity;

namespace Travel.Data.Entities.Employee
{
    public class EmployeeInformation : AuditableEntity
    {
        public EmployeeInformation()
        {
            ApplicationUsers = new HashSet<ApplicationUser>();
        }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string FatherName { get; set; }

        [StringLength(100)]
        public string MotherName { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string MobileNo { get; set; }

        public string Address { get; set; }

        [StringLength(128)]
        public string CountryId { get; set; }

        [StringLength(128)]
        public string CityId { get; set; }

        [StringLength(50)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(200)]
        public string ProfilePicture { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }

    }
}