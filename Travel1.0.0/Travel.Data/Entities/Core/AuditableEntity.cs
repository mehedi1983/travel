﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Travel.Data.Entities.Core
{
    public abstract class AuditableEntity : BaseEntity, IAuditable
    {
        #region IAuditable Members

        [StringLength(2)]
        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }
        #endregion
    }
}
