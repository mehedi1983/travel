﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Travel.Data.Entities.Core
{
    public interface IAuditable
    {
        [StringLength(2)]
        string RecStatus { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
    }
}
