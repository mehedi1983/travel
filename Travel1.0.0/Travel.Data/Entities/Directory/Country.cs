﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Core;
using Travel.Data.Entities.Employee;

namespace Travel.Data.Entities.Directory
{
    public class Country : AuditableEntity
    {
        public Country()
        {
            Cities = new HashSet<City>();
            B2BInformations = new HashSet<B2BInformation>();
            EmployeeInformations = new HashSet<EmployeeInformation>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(2)]
        public string IsoCode2 { get; set; }

        [StringLength(3)]
        public string IsoCode3 { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [JsonIgnore]
        public virtual ICollection<B2BInformation> B2BInformations { get; set; }

        [JsonIgnore]
        public virtual ICollection<City> Cities { get; set; }

        [JsonIgnore]
        public virtual ICollection<EmployeeInformation> EmployeeInformations { get; set; }

    }
}