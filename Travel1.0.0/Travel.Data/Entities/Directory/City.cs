﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Core;
using Travel.Data.Entities.Employee;

namespace Travel.Data.Entities.Directory
{
    public class City : AuditableEntity
    {
        public City()
        {
            B2BInformations = new HashSet<B2BInformation>();
            EmployeeInformations = new HashSet<EmployeeInformation>();
        }

        [StringLength(128)]
        public string CountryId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Code { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        [JsonIgnore]
        public virtual ICollection<B2BInformation> B2BInformations { get; set; }

        [JsonIgnore]
        public virtual ICollection<EmployeeInformation> EmployeeInformations { get; set; }

    }
}