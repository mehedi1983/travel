﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Identity;
using Travel.Data.Entities.Utility;

namespace Travel.Data.Entities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        #region "B2B"
        public virtual DbSet<B2BInformation> B2BInformations { get; set; }
        #endregion

        
        #region "Directory"
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countrys { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region "Utility"
            modelBuilder.Entity<Status>().ToTable("Status");
            #endregion

            #region "B2B"
            modelBuilder.Entity<B2BInformation>().ToTable("B2BInformation");
            #endregion

            #region "Directory"
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Country>().ToTable("Country");
            #endregion

            #region "Seed Data"

            modelBuilder.Entity<Status>().HasData(
                new { Id = "1", Value = "Active" });
            modelBuilder.Entity<Status>().HasData(
                new { Id = "2", Value = "Inactive" });

            modelBuilder.Entity<ApplicationRole>().HasData(
                new { Id = "1", Name = "Admin", NormalizedName = "ADMIN" },
                new { Id = "2", Name = "B2B", NormalizedName = "B2B" },
                new { Id = "3", Name = "Customer", NormalizedName = "CUSTOMER" },
                new { Id = "4", Name = "BackOffice", NormalizedName = "BACKOFFICE" }
                );

            #endregion

            #region "Unique Key"

            modelBuilder.Entity<B2BInformation>()
                .HasIndex(p => new { p.Email })
                .IsUnique();

            modelBuilder.Entity<City>()
                .HasIndex(p => new { p.Name })
                .IsUnique();

            #endregion

        }

    }
}
