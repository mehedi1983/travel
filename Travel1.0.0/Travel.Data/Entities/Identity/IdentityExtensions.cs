﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using Newtonsoft.Json;

namespace Travel.Data.Entities.Identity
{
    public static class IdentityExtensions
    {
        public static ApplicationUser AdditionalInfo(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException("identity");


            var claim1 = ((ClaimsIdentity) identity).FindFirst("FirstName");


            ApplicationUser user = null;
            var claims = ((ClaimsIdentity) identity).Claims;
            //var appUserClaim = claims.
            foreach (var claim in claims)
                if (claim.Type == "ApplicationUser")
                {
                    user = JsonConvert.DeserializeObject<ApplicationUser>(claim.Value);
                    break;
                }

            return user;
        }
    }
}