﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Employee;

namespace Travel.Data.Entities.Identity
{
    public class ApplicationUser : IdentityUser
    {
        [StringLength(128)]
        public string B2BId { get; set; }

        [StringLength(128)]
        public string EmployeeId { get; set; }

        [MinLength(2)]
        [MaxLength(100)]
        public string DisplayName { get; set; }

        [ForeignKey("B2BId")]
        public virtual B2BInformation B2BInformation { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

        public string GetDisplayName()
        {
            return string.IsNullOrWhiteSpace(DisplayName) ? Email : DisplayName;
        }
    }
}