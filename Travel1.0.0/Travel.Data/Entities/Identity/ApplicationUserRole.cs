﻿using Microsoft.AspNetCore.Identity;

namespace Travel.Data.Entities.Identity
{
    public class ApplicationUserRole : IdentityRole
    {
        public string Description { get; set; }
    }
}
