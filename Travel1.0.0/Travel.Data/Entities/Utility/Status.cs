﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Travel.Data.Entities.Core;

namespace Travel.Data.Entities.Utility
{
    public class Status : AuditableEntity
    {        
        [StringLength(20)]
        public string Value { get; set; }
    }
}
