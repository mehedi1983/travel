﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.Utility;
using Travel.Repositories;

namespace Travel.Repositories.Utility
{
    public class StatusRepository : Repository<Status>
    {
        public StatusRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
