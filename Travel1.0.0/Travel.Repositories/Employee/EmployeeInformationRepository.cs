﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.Employee;

namespace Travel.Repositories.Order
{
    public class EmployeeInformationRepository : Repository<EmployeeInformation>
    {
        public EmployeeInformationRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
