﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.Directory;
using Travel.Repositories;

namespace Travel.Repositories.Directory
{
    public class CityRepository : Repository<City>
    {
        public CityRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
