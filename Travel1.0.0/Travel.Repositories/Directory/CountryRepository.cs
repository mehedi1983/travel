﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.Directory;
using Travel.Repositories;

namespace Travel.Repositories.Directory
{
    public class CountryRepository : Repository<Country>
    {
        public CountryRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
