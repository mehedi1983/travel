﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Travel.Data.Entities;

namespace Travel.Repositories
{
    public abstract class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        public readonly ApplicationDbContext _applicationDbContext;
        
        protected Repository(ApplicationDbContext context)
        {
            _applicationDbContext = context;
        }

        public List<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> queryable = this._applicationDbContext.Set<T>();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable.ToList();
        }

        public List<T> GetAllIncluding(Expression<Func<T, bool>> match, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> queryable = this._applicationDbContext.Set<T>().Where(match);
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable.ToList();
        }



        public void DisableLazyLoading()
        {
            //_appsDbContext.Configuration.LazyLoadingEnabled = false;
        }
        public void EnabledLazyLoading()
        {
            //_appsDbContext.Configuration.LazyLoadingEnabled = true;
        }
        /// <summary>
        /// Get All value Of table 
        /// </summary>
        /// <returns>List of table objet </returns>
        public virtual List<T> Get()
        {
            return _applicationDbContext.Set<T>().ToList();
        }

        /// <summary>
        /// Get selected object value 
        /// </summary>
        /// <param name="id">Intiger Type Primary key for search </param>
        /// <returns>Table object</returns>
        public virtual T Get(int id)
        {
            return _applicationDbContext.Set<T>().Find(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T Get(string id)
        {
            return _applicationDbContext.Set<T>().Find(id);
        }
        public virtual IEnumerable<T> GetData(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "")
        {
            IQueryable<T> query = _applicationDbContext.Set<T>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        public virtual void Update(T obj)
        {
            _applicationDbContext.Entry(obj).State = EntityState.Modified;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual T Insert(T obj)
        {
            //return _appsDbContext.Set<T>().Where(predicate); ;
            return _applicationDbContext.Set<T>().Add(obj).Entity;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual T Delete(T obj)
        {
            return _applicationDbContext.Set<T>().Remove(obj).Entity;
        }

        public virtual T DeleteById(int id)
        {
            var obj = Get(id);
            return Delete(obj);
        }

        public virtual T DeleteById(string id)
        {
            var obj = Get(id);
            return Delete(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        public virtual void Edit(T obj)
        {
            _applicationDbContext.Entry(obj).State = EntityState.Modified;
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _applicationDbContext.Set<T>().Where(predicate); ;
        }

        public void Save()
        {
            _applicationDbContext.SaveChanges();
        }
        
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // _appsDbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
