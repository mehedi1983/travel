﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.B2B;
using Travel.Repositories;

namespace Travel.Repositories.Chef
{
    public class B2BInformationRepository : Repository<B2BInformation>
    {
        public B2BInformationRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
