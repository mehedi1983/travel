﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Model.Account
{
    public class LoginSummaryModel
    {
        public string PersonName { get; set; }
        public string UserId { get; set; }
        public string ViewLanguage { get; set; }
    }
}
