﻿using System;
using System.Linq;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Employee;
using Travel.Data.Entities.Utility;
using Travel.Model.B2B;
using Travel.Model.Directory;
using Travel.Model.Employee;
using Travel.Model.Utility;

namespace Travel.Model.Factories
{
    public class ModelFactory : IModelFactory
    {

        #region "Utility"
        public Status Create(StatusModel obj)
        {
            return new Status
            {
                Id = obj.Id,
                Value = obj.Value,
            };
        }

        public StatusModel Create(Status obj)
        {
            return new StatusModel
            {
                Id = obj.Id,
                Value = obj.Value
            };
        }
        #endregion

        #region "B2B Information"

        public B2BInformation Create(B2BInformationModel obj)
        {
            return new B2BInformation()
            {
                Id = obj.Id,
                Name = obj.Name,
                Address = obj.Address,
                CountryId = obj.CountryId,
                CityId = obj.CityId,
                ZipCode = obj.ZipCode,
                ContactPerson = obj.ContactPerson,
                ContactPersonDesignation = obj.ContactPersonDesignation,
                Email = obj.Email,
                MobileNumber = obj.MobileNumber,
                PhoneNumber = obj.PhoneNumber,
                Fax = obj.Fax,
                Website = obj.Website,
                NatureOfBusiness = obj.NatureOfBusiness,
                LicenseNo = obj.LicenseNo,
                LicenseCopy = obj.LicenseCopy,
                Logo = obj.Logo,
                Status = obj.Status,
                CreatedDate = obj.CreatedDate,
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        public B2BInformationModel Create(B2BInformation obj)
        {
            return new B2BInformationModel
            {
                Id = obj.Id,
                Name = obj.Name,
                Address = obj.Address,
                CountryId = obj.CountryId,
                //CountryName = obj.Location != null ? obj.Location.Name : "",
                CityId = obj.CityId,
                //CityName = obj.Location != null ? obj.Location.City.Name : "",
                ZipCode = obj.ZipCode,
                ContactPerson = obj.ContactPerson,
                ContactPersonDesignation = obj.ContactPersonDesignation,
                Email = obj.Email,
                MobileNumber = obj.MobileNumber,
                PhoneNumber = obj.PhoneNumber,
                Fax = obj.Fax,
                Website = obj.Website,
                NatureOfBusiness = obj.NatureOfBusiness,
                LicenseNo = obj.LicenseNo,
                LicenseCopy = obj.LicenseCopy,
                Logo = obj.Logo,
                Status = obj.Status,
                EmailVerified = obj.ApplicationUsers.FirstOrDefault().EmailConfirmed,
                CreatedDate = obj.CreatedDate,
                CreatedDateView = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        #endregion

        #region "Directory"

        public Country Create(CountryModel obj)
        {
            return new Country
            {
                Id = obj.Id,
                Name = obj.Name,
                IsoCode2 = obj.IsoCode2,
                IsoCode3 = obj.IsoCode3,
                Status = obj.Status,
                CreatedDate = obj.CreatedDate,
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        public CountryModel Create(Country obj)
        {
            return new CountryModel
            {
                Id = obj.Id,
                Name = obj.Name,
                IsoCode2 = obj.IsoCode2,
                IsoCode3 = obj.IsoCode3,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedDate = obj.CreatedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedDate = obj.ModifiedDate,
                ModifiedBy = obj.ModifiedBy,
            };
        }

        public City Create(CityModel obj)
        {
            return new City
            {
                Id = obj.Id,
                Name = obj.Name,
                Code = obj.Code,
                CountryId = obj.CountryId,
                CreatedDate = obj.CreatedDate,
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        public CityModel Create(City obj)
        {
            return new CityModel
            {
                Id = obj.Id,
                Name = obj.Name,
                Code = obj.Code,
                CountryId = obj.CountryId,
                CountryName = obj.Country != null ? obj.Country.Name : "",
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedDate = obj.CreatedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedDate = obj.ModifiedDate,
                ModifiedBy = obj.ModifiedBy,
            };
        }

        #endregion        

        #region "Back end"

        public EmployeeInformation Create(EmployeeInformationModel obj)
        {
            return new EmployeeInformation
            {
                Id = obj.Id,
                Name = obj.Name,
                FatherName = obj.FatherName,
                MotherName = obj.MotherName,
                Email = obj.Email,
                MobileNo = obj.MobileNo,
                Address = obj.Address,
                CountryId = obj.CountryId,
                CityId = obj.CityId,
                ZipCode = obj.ZipCode,
                ProfilePicture = obj.ProfilePicture,
                Status = obj.Status,
                CreatedDate = obj.CreatedDate,
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        public EmployeeInformationModel Create(EmployeeInformation obj)
        {
            return new EmployeeInformationModel
            {
                Id = obj.Id,
                Name = obj.Name,
                FatherName = obj.FatherName,
                MotherName = obj.MotherName,
                Email = obj.Email,
                MobileNo = obj.MobileNo,
                Address = obj.Address,
                CountryId = obj.CountryId,
                CountryName = obj.Country != null ? obj.Country.Name : "",
                CityId = obj.CityId,
                CityName = obj.City != null ? obj.City.Name : "",
                ZipCode = obj.ZipCode,
                ProfilePicture = obj.ProfilePicture,
                Status = obj.Status,
                CreatedDate = obj.CreatedDate,
                ModifiedDate = obj.ModifiedDate,
                CreatedBy = obj.CreatedBy,
                ModifiedBy = obj.ModifiedBy,
                RecStatus = obj.RecStatus
            };
        }

        #endregion
    }
}