﻿

using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Employee;
using Travel.Data.Entities.Utility;
using Travel.Model.B2B;
using Travel.Model.Directory;
using Travel.Model.Employee;
using Travel.Model.Utility;

namespace Travel.Model.Factories
{
    public interface IModelFactory
    {

        #region "Utility"
        StatusModel Create(Status obj);
        Status Create(StatusModel obj);
        #endregion

        #region "B2B Information"
        B2BInformation Create(B2BInformationModel obj);
        B2BInformationModel Create(B2BInformation obj);
        #endregion

        #region "Directory"
        City Create(CityModel obj);
        CityModel Create(City obj);
        Country Create(CountryModel obj);
        CountryModel Create(Country obj);
        #endregion        

        #region "Back end"
        EmployeeInformation Create(EmployeeInformationModel obj);
        EmployeeInformationModel Create(EmployeeInformation obj);
        #endregion

    }
}