﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Model.Utility
{
    public class StatusModel
    {
        public string Id { get; set; }

        public string Value { get; set; }
        
    }
}
