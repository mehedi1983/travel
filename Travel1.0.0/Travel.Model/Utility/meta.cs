﻿namespace Travel.Model.Utility
{
    public class meta
    {
        public int page { get; set; }
        public int pages { get; set; }
        public int perpage { get; set; }
        public int total { get; set; }
        public string sort { get; set; }
        public string field { get; set; }
        public string SearchString { get; set; }
        public string Status { get; set; }
    }
}