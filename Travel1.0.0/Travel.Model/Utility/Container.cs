﻿using Travel.Model.Utility;

namespace Travel.Model.Utility
{
    public class DTReturnContainer
    {
        public meta meta { get; set; }
        public object data { get; set; }
    }

    public class DTSearchReturnContainer
    {
        public int TotalRowsCount { get; set; }
        public object Data { get; set; }
    }
}