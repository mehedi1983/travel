﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel.Model.Utility
{
    public class UserModel
    {
        public string EmployeeId { get; set; }

        public string DisplayName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
