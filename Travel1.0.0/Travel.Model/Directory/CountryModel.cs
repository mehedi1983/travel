﻿using System;
using System.Collections.Generic;
using Travel.Model.B2B;

namespace Travel.Model.Directory
{
    public class CountryModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string IsoCode2 { get; set; }

        public string IsoCode3 { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public List<B2BInformationModel> Companys { get; set; }

    }
}