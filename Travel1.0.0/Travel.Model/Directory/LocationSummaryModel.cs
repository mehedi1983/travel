﻿using System;
using System.Collections.Generic;

namespace Travel.Model.Directory
{
    public class CountrySummaryModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

    }
}