﻿using System;
using System.Collections.Generic;

namespace Travel.Model.Directory
{
    public class CitySummaryModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string CountryId { get; set; }

        public string CountryName { get; set; }


    }
}