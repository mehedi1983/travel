﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Travel.Model.B2B;

namespace Travel.Model.Employee
{
    public class AdminDashboardModel
    {
        public List<B2BSummaryModel> TopEarnedChef { get; set; }
    }
}