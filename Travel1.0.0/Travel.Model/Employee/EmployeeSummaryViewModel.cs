﻿using System;

namespace Travel.Model.Employee
{
    public class EmployeeSummaryViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string MobileNumber { get; set; }

        public DateTime? JoinDate { get; set; }

        public string CityName { get; set; }

        public string CountryName { get; set; }

        public string Address { get; set; }

        public string Picture { get; set; }

        public string Status { get; set; }
    }
}