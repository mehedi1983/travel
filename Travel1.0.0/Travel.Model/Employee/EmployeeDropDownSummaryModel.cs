﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Travel.Model.Employee
{
    public class EmployeeDropDownSummaryModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

    }
}