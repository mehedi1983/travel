﻿using System.Collections.Generic;
using Travel.Model.Directory;
using Travel.Model.Utility;

namespace Travel.Model.Employee
{
    public class EmployeeDefaultValueModel
    {
        public List<CountrySummaryModel> Countries { get; set; }

        public List<CitySummaryModel> Cities { get; set; }

        public List<StatusModel> Status { get; set; }

    }
}