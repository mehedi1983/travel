﻿using System;
using System.Collections.Generic;

namespace Travel.Model.Employee
{
    public class EmployeeInformationModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string Email { get; set; }

        public string MobileNo { get; set; }

        public string Address { get; set; }

        public string CountryId { get; set; }

        public string CountryName { get; set; }

        public string CityId { get; set; }

        public string CityName { get; set; }
        
        public string ZipCode { get; set; }

        public string Status { get; set; }

        public string ProfilePicture { get; set; }

        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

    }
}