﻿using System.Collections.Generic;
using Travel.Model.Directory;
using Travel.Model.Utility;

namespace Travel.Model.Employee
{
    public class EmployeeWithLocationListModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public string Address { get; set; }

        public string CityId { get; set; }

        public string CityName { get; set; }

        public string ZipCode { get; set; }

        public string CountryId { get; set; }

        public string CountryName { get; set; }

        public string Picture { get; set; }

        public string Status { get; set; }

        public List<CitySummaryModel> CitySummaryModels { get; set; }

        public List<CountrySummaryModel> CountrySummaryModels { get; set; }

        public List<StatusModel> StatusSummaryModels { get; set; }

    }
}