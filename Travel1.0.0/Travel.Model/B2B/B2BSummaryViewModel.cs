﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Travel.Model.B2B
{
    public class B2BSummaryViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string MobileNumber { get; set; }

        public string LicenseNo { get; set; }

        public string LicenseCopy { get; set; }

        public DateTime? JoinDate { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Logo { get; set; }

        public string Status { get; set; }
    }
}