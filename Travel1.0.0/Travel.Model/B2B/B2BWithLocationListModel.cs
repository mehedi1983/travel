﻿using System.Collections.Generic;
using Travel.Model.Directory;

namespace Travel.Model.B2B
{
    public class B2BWithLocationListModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Fax { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public string Address { get; set; }

        public string CountryId { get; set; }

        public string CountryName { get; set; }

        public string CityId { get; set; }

        public string CityName { get; set; }

        public string ZipCode { get; set; }

        public string ContactPerson { get; set; }

        public string ContactPersonDesignation { get; set; }

        public string NatureOfBusiness { get; set; }

        public string LicenseNo { get; set; }

        public string LicenseCopy { get; set; }

        public string Logo { get; set; }

        public string Status { get; set; }

        public List<CountrySummaryModel> CountrySummaryModels { get; set; }

        public List<CitySummaryModel> CitySummaryModels { get; set; }

    }
}