﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Travel.Model.B2B
{
    public class B2BSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Website { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string CountryId { get; set; }

        public string CityId { get; set; }

        public string Picture { get; set; }

    }
}