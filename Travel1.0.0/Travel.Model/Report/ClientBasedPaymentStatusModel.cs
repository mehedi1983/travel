﻿namespace Travel.Model.Report
{
    public class ClientBasedPaymentStatusModel
    {
        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public int Invoices { get; set; }

        public decimal AvInvoices { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal ReceivedAmount { get; set; }

        public decimal Outstanding { get; set; }

        public string Currency { get; set; }
    }
}