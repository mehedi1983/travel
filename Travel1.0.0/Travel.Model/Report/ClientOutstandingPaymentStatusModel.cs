﻿using System;

namespace Travel.Model.Report
{
    public class ClientOutstandingPaymentStatusModel
    {
        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string InvoiceId { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string InvoiceDateView { get; set; }

        public string Type { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal OutstandindAmount { get; set; }

        public string Currency { get; set; }
    }
}