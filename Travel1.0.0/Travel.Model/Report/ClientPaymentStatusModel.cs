﻿using System;

namespace Travel.Model.Report
{
    public class ClientPaymentStatusModel
    {
        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string InvoiceId { get; set; }

        public string InvoiceNo { get; set; }

        public string Type { get; set; }

        public decimal TotalAmount { get; set; }

        public DateTime ReceivedDate { get; set; }
        public string ReceivedDateView { get; set; }

        public decimal ReceivedAmount { get; set; }

        public string Currency { get; set; }
    }
}