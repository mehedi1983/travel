﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Travel.Business.Derived;
using Travel.Data.Entities.Identity;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace TastyQueen.UI.Areas.Report.Controllers
{
    [Area("Report")]
    [Authorize(Roles = "Admin")]
    public class ManagementController : Controller
    {
        private readonly BllReport _bllReport;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public ManagementController(UserManager<ApplicationUser> userManager, IReportService iReportService,
            IModelFactory iModelFactory,
            IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _bllReport = new BllReport(iReportService, iModelFactory, Configuration);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> Clients()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.ChefId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";
            return View();
        }

        public async Task<IActionResult> Payments()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.ChefId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";
            return View();
        }

        public async Task<IActionResult> Expenses()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.ChefId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";
            return View();
        }

        
    }
}