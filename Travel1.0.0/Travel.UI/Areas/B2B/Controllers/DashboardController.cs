﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Data.Entities.Identity;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.UI.Areas.B2B.Controllers
{
    [Area("B2B")]
    [Authorize(Roles = "B2B, Admin")]
    public class DashboardController : Controller
    {
        private readonly BllB2BInformation _bllB2BInformation;
        private readonly UserManager<ApplicationUser> _userManager;

        public DashboardController(UserManager<ApplicationUser> userManager, IB2BInformationService iB2BInformationService,
            IModelFactory iModelFactory, IConfiguration Configuration)
        {
            _userManager = userManager;
            _bllB2BInformation = new BllB2BInformation(iB2BInformationService, iModelFactory, Configuration);
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        [HttpGet]
        public async Task<JsonResult> LoadB2BDashboardInfo()
        {
            try
            {
                var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
                var val = await _bllB2BInformation.LoadB2BDashboardInfo(applicationuser.B2BId);
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        public async Task<IActionResult> Index()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.B2BId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";
            return View();
        }

    }
}