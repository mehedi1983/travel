﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Business.Utility;
using Travel.Data.Entities.Identity;
using Travel.Model.B2B;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.UI.Areas.B2B.Controllers
{
    [Area("B2B")]
    [Authorize(Roles = "B2B, Admin")]
    public class ProfileController : Controller
    {
        private readonly BllB2BInformation _bllB2BInformation;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProfileController(UserManager<ApplicationUser> userManager, IB2BInformationService iB2BInformationService,
            IModelFactory iModelFactory,
            IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _userManager = userManager;
            _bllB2BInformation = new BllB2BInformation(iB2BInformationService, iModelFactory, Configuration);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.B2BId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(IFormFile ProfilePicture, string Id)
        {
            
            var fileName = "";
            try
            {
                var gId = Guid.NewGuid().ToString();
                fileName = gId + ".jpg";
                if (ProfilePicture != null)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["CompanyLogoFolder"], fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await ProfilePicture.CopyToAsync(stream);
                    }
                    var val = await _bllB2BInformation.UpdateB2BLogo(fileName, Id,
                        User.FindFirst(ClaimTypes.Name).Value, DateTime.Now);
                    if (val != null)
                        return Json(new
                        {
                            success = true,
                            fileName,
                            val.Address,
                            uploadedUrl = filePath,
                            Message = MessageManager.COMPANY_UPLOAD_IMAGE_SUCCESS_MESSAGE
                        });
                    return Json(new
                    {
                        success = false,
                        uploadedUrl = filePath,
                        Message = MessageManager.COMPANY_UPLOAD_IMAGE_FAIL_MESSAGE
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    uploadedUrl = "",
                    Message = MessageManager.COMPANY_UPLOAD_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                success = false,
                uploadedUrl = "",
                Message = MessageManager.COMPANY_UPLOAD_IMAGE_FAIL_MESSAGE
            });
        }

        [HttpGet]
        public async Task<JsonResult> LoadB2BInformationWithCityAndLocationList(string Id)
        {
            try
            {
                var val = await _bllB2BInformation.LoadB2BInformationWithCityAndLocationList(Id);
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> ChangeBasicInformation(B2BInformationModel b2BInformationModel)
        {
            if (ModelState.IsValid)
                try
                {
                    var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
                    b2BInformationModel.CreatedBy = applicationuser.UserName;
                    var val = await _bllB2BInformation.ChangeBasicInformation(b2BInformationModel);
                    return Json(new
                    {
                        Success = true,
                        RedirectUrl = Url.Action("Index", "Dashboard", new { Area = "B2B" }).Trim(),
                        //B2BLogo = val.Logo,
                        //B2BId = val.Id,
                        //B2BAddress = val.Address,
                        Message = MessageManager.COMPANY_BASIC_INFORMATION_SAVE_MESSAGE
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = MessageManager.COMPANY_BASIC_INFORMATION_FAIL_MESSAGE
                    });
                }
            return Json(new
            {
                Success = false,
                Message = GetModelStateErrorMessage()
            });
        }

        [HttpPost]
        public async Task<JsonResult> DeleteLogo(string Id)
        {
            try
            {
                var tempImage = await _bllB2BInformation.LoadB2BInformation(Id);
                var result = await _bllB2BInformation.RemoveB2BLogo(Id);

                if (result)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["CompanyLogoFolder"], tempImage.Logo);
                    System.IO.File.Delete(filePath);
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.COMPANY_REMOVE_IMAGE_SUCCESS_MESSAGE
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.COMPANY_REMOVE_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.COMPANY_REMOVE_IMAGE_FAIL_MESSAGE
            });
        }

        //[HttpGet]
        //public async Task<JsonResult> KeepClientAlive()
        //{
        //    try
        //    {
        //        var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
        //        var ChefId = applicationuser.B2BInformation != null ? applicationuser.B2BInformation.Id : "";

        //        var val = await _bllB2BInformation.KeepClientAlive(ChefId);
        //        return Json(new
        //        {
        //            Success = true,
        //            value = val,
        //            Message = "Keep Client Alive"
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            Success = false
        //        });
        //    }
        //}

        [HttpGet]
        public async Task<JsonResult> LoadCountry()
        {
            try
            {
                var val = await _bllB2BInformation.LoadCountry();
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> LoadCity(string CountryId)
        {
            try
            {
                var val = await _bllB2BInformation.LoadCity(CountryId);
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

    }
}