﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TastyQueen.UI.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "Admin")]
    public class CountryController : Controller
    {
        public IActionResult Index()
        {
            //ViewBag.DashboardUrl = HttpContext.Session.GetString(OperationStatus.SESSION_DASHBOARD_URL);
            return View();
        }
    }
}