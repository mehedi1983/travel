﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Business.Derived;
using Travel.Business.Utility;
using Travel.Data.Entities.Identity;
using Travel.Model.B2B;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class B2BController : Controller
    {
        private readonly BllB2BInformation _bllB2BInformation;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public B2BController(UserManager<ApplicationUser> userManager, IB2BInformationService iB2BInformationService,
            IModelFactory iModelFactory, IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _userManager = userManager;
            _bllB2BInformation = new BllB2BInformation(iB2BInformationService, iModelFactory, Configuration);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> Search(int PageSize, int PageNo, string SearchString, string FilterByStatus)
        {
            try
            {
                var val = await _bllB2BInformation.B2BSummary(PageSize, PageNo, SearchString, FilterByStatus);
                return Json(new
                {
                    Success = true,
                    Value = (List<B2BSummaryViewModel>)val.Data,
                    val.TotalRowsCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> ResendEmailVerificationLink(string Id)
        {
            var user = _userManager.Users.Where(w => w.B2BId == Id).FirstOrDefault();

            var emailUtil = new EmailUtil();
            var Token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { Area = "", userId = user.Id, Token }, Request.Scheme);

            var message =
                            "<div style=\"width: 15%; float: left; color: transparent;\">---</div> <div style=\"width: 70%; min-height: 400px; background-color: ghostwhite;float: left; \">" +
                            "<div style=\"margin-bottom: 2%; width: 59%; margin-top: 5%; margin-left: 5%;\"><img src=\"" +
                            _configuration.GetSection("ApplicationInformation")["Logo"] + "\"></div>" +
                            "<div style=\"width: 100%; \">" +
                            "<div style =\"width: 5%; float: left; color: transparent;\"> ---</div>" +
                            "<div style =\"width: 88%; background-color: white; float: left; padding: 1%; \">" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Hi " + user.DisplayName + ",</div> " +
                            "<div style =\"width: 100%;\">Greeting from " +
                            _configuration.GetSection("ApplicationInformation")["Name"] + ".</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\"> To continue setting up your account, please click the button below:<br/>" +
                            "<a href=\"" + callbackUrl +
                            "\" style=\" margin-top: 5%; background-color: #4CAF50; border: none; color: white; padding: 10px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 5%;\">Link</a>" +
                            "</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Alternatively please click the link below:<br/>" +
                            "<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>" +
                            "</div>" +
                            "<div style =\"width: 100%;\">Thanks</div>" +
                            "<div style =\"width: 100%;\">" +
                            _configuration.GetSection("ApplicationInformation")["Name"] + " Support Team</div>" +
                            "</div> " +
                            "<div style =\"width: 2%; float: left; color: transparent;\">---</div>" +
                            "</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:1%; margin-left: 5%; margin-top: 2%;\">This is a no-reply email. To make an inquiry, please contact our help.</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:3%; margin-left: 5%;\">© " +
                            _configuration.GetSection("ApplicationInformation")["Name"] +
                            ". All rights reserved.</div>" +
                            "</div> <div style=\"width: 15%; float: left; color: transparent;\">---</div> ";

            await emailUtil.SendEmail(user.Email, "Verify Your Email Address", message, _configuration);

            return Json(new
            {
                Success = true,
                Message = "Successfully resent email verification link."
            });
        }

    }
}