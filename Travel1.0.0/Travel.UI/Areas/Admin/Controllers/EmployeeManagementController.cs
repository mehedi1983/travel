﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Business.Utility;
using Travel.Data.Entities.Identity;
using Travel.Model.Employee;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class EmployeeManagementController : Controller
    {
        private readonly BllEmployee _bllEmployee;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmployeeManagementController(UserManager<ApplicationUser> userManager, IEmployeeService iCompanyService,
            IModelFactory iModelFactory,
            IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _userManager = userManager;
            _bllEmployee = new BllEmployee(iCompanyService, iModelFactory, Configuration);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            //var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            //ViewBag.EmployeeId = applicationuser.EmployeeInformation != null ? applicationuser.EmployeeInformation.Id : "";
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> Search(int PageSize, int PageNo, string SearchString, string FilterByStatus)
        {
            try
            {
                var val = await _bllEmployee.EmployeeSummary(PageSize, PageNo, SearchString, FilterByStatus);
                return Json(new
                {
                    Success = true,
                    Value = (List<EmployeeSummaryViewModel>)val.Data,
                    val.TotalRowsCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> EmployeeDefaultValue()
        {
            try
            {
                var val = await _bllEmployee.EmployeeDefaultValue();
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
            ViewBag.EmployeeId = applicationuser.EmployeeInformation != null ? applicationuser.EmployeeInformation.Id : "";
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> LoadEmployeeInformationWithCityAndLocationList(string Id)
        {
            try
            {
                var val = await _bllEmployee.LoadEmployeeInformationWithCityAndLocationList(Id);
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveUpdate(EmployeeInformationModel employeeInformationModel)
        {
            try
            {
                var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
                //applicationuser.DisplayName = employeeInformationModel.Name;
                //await _userManager.UpdateAsync(applicationuser);

                string operationMessage = (employeeInformationModel.Id == null || employeeInformationModel.Id == "")
                    ? MessageManager.EMPLOYEE_SAVE_SUCCESS_MESSAGE
                    : MessageManager.EMPLOYEE_EDIT_SUCCESS_MESSAGE;

                employeeInformationModel.CreatedBy = applicationuser.UserName;
                var val = await _bllEmployee.InsertUpdate(employeeInformationModel);
                return Json(new
                {
                    Success = true,
                    EmployeeId = val.Id,
                    Message = operationMessage
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.EMPLOYEE_SAVE_FAIL_MESSAGE
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(IFormFile ProfilePicture, string EmployeeId)
        {
            var fileName = "";
            try
            {
                var gId = Guid.NewGuid().ToString();
                fileName = gId + ".jpg";
                if (ProfilePicture != null)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["EmployeeImageFolder"], fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await ProfilePicture.CopyToAsync(stream);
                    }
                    var val = await _bllEmployee.UpdateEmployeeLogo(fileName, EmployeeId,
                        User.FindFirst(ClaimTypes.Name).Value, DateTime.Now);
                    if (val != null)
                        return Json(new
                        {
                            success = true,
                            fileName,
                            val.Address,
                            uploadedUrl = filePath,
                            Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_SUCCESS_MESSAGE
                        });
                    return Json(new
                    {
                        success = false,
                        uploadedUrl = filePath,
                        Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    uploadedUrl = "",
                    Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                success = false,
                uploadedUrl = "",
                Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
            }); 
        }

        [HttpPost]
        public async Task<JsonResult> DeleteEmployeeLogo(string Id)
        {
            try
            {
                var tempImage = await _bllEmployee.LoadEmployeeInformation(Id);
                var result = await _bllEmployee.RemoveEmployeeLogo(Id);

                if (result)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["EmployeeImageFolder"], tempImage.ProfilePicture);
                    System.IO.File.Delete(filePath);
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.EMPLOYEE_REMOVE_IMAGE_SUCCESS_MESSAGE
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.EMPLOYEE_REMOVE_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.EMPLOYEE_REMOVE_IMAGE_FAIL_MESSAGE
            });
        }

        [HttpGet]
        public async Task<JsonResult> LoadCity(string CountryId)
        {
            try
            {
                var val = await _bllEmployee.LoadCity(CountryId);
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> LoadCountry()
        {
            try
            {
                var val = await _bllEmployee.LoadCountry();
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

    }
}