﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Travel.Business.Derived;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "B2B, Admin")]
    public class ConfigurationController : Controller
    {
        private readonly BllAdmin _bllAdmin;

        public ConfigurationController(IAdminSettingService iAdminSettingService, IModelFactory iModelFactory)
        {
            _bllAdmin = new BllAdmin(iAdminSettingService, iModelFactory);
        }

        [HttpGet]
        public async Task<JsonResult> GetStatus()
        {
            try
            {
                var val = await _bllAdmin.GetStatus();
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

    }
}