﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Business.Utility;
using Travel.Data.Entities.Identity;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;


namespace TastyQueen.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UserManagementController : Controller
    {
        private readonly BllUser _bllUser;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserManagementController(UserManager<ApplicationUser> userManager, IUserService iUserService,
            IModelFactory iModelFactory, IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _userManager = userManager;
            _bllUser = new BllUser(userManager, iUserService, iModelFactory);
            _hostingEnvironment = hostingEnvironment;
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> Search(int PageSize, int PageNo, string SearchString, string FilterByStatus)
        {
            try
            {
                var val = await _bllUser.GetList(PageSize, PageNo, SearchString, FilterByStatus);
                return Json(new
                {
                    Success = true,
                    Value = (List<ApplicationUser>) val.Data,
                    val.TotalRowsCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetEmployeeInformation()
        {
            try
            {
                var val = await _bllUser.GetEmployeeInformation();
                return Json(new
                {
                    Success = true,
                    Value = val
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> Save(UserModel userModel)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = userModel.UserName,
                    Email = userModel.UserName,
                    EmailConfirmed = true,
                    EmployeeId = userModel.EmployeeId,
                    DisplayName = userModel.DisplayName
                };
                var result = await _userManager.CreateAsync(user, userModel.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "BackOffice");
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.INSERT_USER_SUCCESS_MESSAGE
                    });
                }
                return Json(new
                {
                    Success = false,
                    Message = result.Errors.ToList().FirstOrDefault().Description
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.INSERT_USER_FAIL_MESSAGE
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string Id)
        {
            try
            {
                var userAsync = await _userManager.FindByIdAsync(Id);
                var result = await _userManager.DeleteAsync(userAsync);
                if (result.Succeeded)
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.DELETE_USER_SUCCESS_MESSAGE
                    });
                return Json(new
                {
                    Success = false,
                    Message = result.Errors.FirstOrDefault().Description
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.DELETE_USER_SUCCESS_MESSAGE
                });
            }
        }
    }
}