﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Travel.UI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "BackOffice, Admin")]
    public class AdminDashboardController : Controller
    {
        public IActionResult Index()
        {
            //ViewBag.DashboardUrl = HttpContext.Session.GetString(OperationStatus.SESSION_DASHBOARD_URL);
            return View();
        }
    }
}