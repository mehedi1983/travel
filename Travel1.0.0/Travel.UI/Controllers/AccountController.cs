﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Travel.Business.Derived;
using Travel.Business.Utility;
using Travel.Data.Entities;
using Travel.Data.Entities.Identity;
using Travel.Model.Account;
using Travel.Model.B2B;
using Travel.Model.Factories;
using Travel.Service.Interface;


namespace Travel.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _appContext;
        private readonly BllAuth _bllAuth;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IAuthService iAuthService, IModelFactory iModelFactory, IConfiguration Configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = Configuration;
            _bllAuth = new BllAuth(iAuthService, iModelFactory, Configuration);
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        #region "Registration"

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    EmailConfirmed = true,
                    PhoneNumber = model.PhoneNumber,
                    DisplayName = model.FirstName
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                    await _userManager.AddToRoleAsync(user, "Admin");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> RegisterUser(RegisterMinimumAttributeModel model)
        {
            if (ModelState.IsValid)
                try
                {
                    var B2BId = await _bllAuth.GetB2BInformationId();
                    var userProfileModel = new B2BInformationModel();
                    userProfileModel.Id = B2BId;
                    //userProfileModel.Name = model.Name;
                    userProfileModel.Email = model.Email;
                    userProfileModel.Status = OperationStatus.INACTIVE;
                    userProfileModel.CreatedBy = OperationStatus.SYSTEM_GENERATED;

                    _bllAuth.SaveB2BAdminUser(userProfileModel);

                    var user = new ApplicationUser
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        B2BId = B2BId,
                    };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        //Assign Role to user Here 
                        await _userManager.AddToRoleAsync(user, "B2B");
                        //Ends Here

                        var emailUtil = new EmailUtil();
                        var Token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, Token }, Request.Scheme);

                        var message =
                            "<div style=\"width: 15%; float: left; color: transparent;\">---</div> <div style=\"width: 70%; min-height: 400px; background-color: ghostwhite;float: left; \">" +
                            "<div style=\"margin-bottom: 2%; width: 59%; margin-top: 5%; margin-left: 5%;\"><img src=\"" +
                            _configuration.GetSection("ApplicationInformation")["Logo"] + "\"></div>" +
                            "<div style=\"width: 100%; \">" +
                            "<div style =\"width: 5%; float: left; color: transparent;\"> ---</div>" +
                            "<div style =\"width: 88%; background-color: white; float: left; padding: 1%; \">" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Hi " + user.DisplayName + ",</div> " +
                            "<div style =\"width: 100%;\">Greeting from " +
                            _configuration.GetSection("ApplicationInformation")["Name"] + ".</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\"> To continue setting up your account, please click the button below:<br/>" +
                            "<a href=\"" + callbackUrl +
                            "\" style=\" margin-top: 5%; background-color: #FF0800; border: none; color: white; padding: 10px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 5%;\">Link</a>" +
                            "</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Alternatively please click the link below:<br/>" +
                            "<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>" +
                            "</div>" +
                            "<div style =\"width: 100%;\">Thanks</div>" +
                            "<div style =\"width: 100%;\">" +
                            _configuration.GetSection("ApplicationInformation")["Name"] + " Support Team</div>" +
                            "</div> " +
                            "<div style =\"width: 2%; float: left; color: transparent;\">---</div>" +
                            "</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:1%; margin-left: 5%; margin-top: 2%;\">This is a no-reply email. To make an inquiry, please contact our help.</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:3%; margin-left: 5%;\">© " +
                            _configuration.GetSection("ApplicationInformation")["Name"] +
                            ". All rights reserved.</div>" +
                            "</div> <div style=\"width: 15%; float: left; color: transparent;\">---</div> ";

                        await emailUtil.SendEmail(user.Email, "Verify Your Email Address", message, _configuration);

                        return Json(new
                        {
                            Success = true,
                            RedirectUrl = Url.Action("Confirm", "Account", new { user.Email }),
                            Message = "Sign up successfully complete."
                        });
                    }
                    else
                    {
                        _bllAuth.Delete(B2BId);
                        return Json(new { Success = false, Message = result.Errors.ToList().FirstOrDefault().Description });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            return Json(new { Success = false, Message = GetModelStateErrorMessage() });
        }

        #endregion

        #region "Email Confirmation"

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string Token)
        {
            if (userId == null || Token == null)
                return RedirectToAction(nameof(HomeController.Index), "Home");
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            var result = await _userManager.ConfirmEmailAsync(user, Token);
            return View();
        }

        [AllowAnonymous]
        public ActionResult Confirm(string email)
        {
            ViewBag.Email = email;
            return View();
        }

        #endregion

        #region "Login/Logout"

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    var _user = await _userManager.FindByEmailAsync(model.Email);
                    if (_user != null)
                        if (await _userManager.IsInRoleAsync(_user, "User"))
                        {
                            //SessionManager.UserRole = "MsAdmin";
                            //SessionManager.DashBoard = Url.Action("Index", "MsAdmin", new { Area = "Master" });
                            //return RedirectToAction("Index", "UserDashboard", new { Area = "Master" });
                        }
                        else if (await _userManager.IsInRoleAsync(_user, "Saadmin"))
                        {
                            //SessionManager.UserRole = "MsAdmin";
                            //SessionManager.DashBoard = Url.Action("Index", "MsAdmin", new { Area = "Master" });
                            //return RedirectToAction("Index", "SaAdminDashboard", new { Area = "Master" });
                        }
                        else if (await _userManager.IsInRoleAsync(_user, "Admin"))
                        {
                            //SessionManager.UserRole = "Admin";
                            //SessionManager.DashBoard = Url.Action("Index", "AdminDashboard", new { Area = "Master" });
                            return RedirectToAction("Index", "AdminDashboard", new { Area = "Master" });
                        }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> LoginNew(LoginViewModel model)
        {
            if (ModelState.IsValid)
                try
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    if (user == null)
                        return Json(new { Success = false, Message = "Incorrect username." });

                    var validPassword = await _userManager.CheckPasswordAsync(user, model.Password);
                    if (!validPassword)
                        return Json(new { Success = false, Message = "Incorrect password." });

                    if (await _userManager.IsEmailConfirmedAsync(user))
                    {
                        await _signInManager.SignInAsync(user, new AuthenticationProperties
                        {
                            IsPersistent = true,
                            ExpiresUtc = DateTimeOffset.UtcNow.AddDays(30)
                        });
                        var DashboardUrl = "";
                        if (await _userManager.IsInRoleAsync(user, "B2B"))
                        {
                            DashboardUrl = Url.Action("Index", "Dashboard", new { Area = "B2B" });
                            var company = await _bllAuth.GetB2BInformation(user.B2BId);
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl.Trim(),
                                B2BLogo = company.Logo,
                                user.B2BId,
                                B2BAddress = company.Address,
                                Message = "Login successfully done."
                            });
                        }
                        if (await _userManager.IsInRoleAsync(user, "Customer"))
                        {
                            //DashboardUrl = Url.Action("Index", "CustomerDashboard", new { Area = "Customer" });
                            ////var company = await _bllAuth.GetChefInformation(user.ChefId);
                            //return Json(new
                            //{
                            //    Success = true,
                            //    RedirectUrl = DashboardUrl,
                            //    //ChefLogo = company.Picture,
                            //    //ChefAddress = company.Address,
                            //    Message = "Login successfully done."
                            //});
                        }
                        if (await _userManager.IsInRoleAsync(user, "Admin"))
                        {
                            DashboardUrl = Url.Action("Index", "AdminDashboard", new { Area = "Admin" });
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl,
                                Message = "Login successfully done."
                            });
                        }
                        if (await _userManager.IsInRoleAsync(user, "BackOffice"))
                        {
                            DashboardUrl = Url.Action("Index", "AdminDashboard", new { Area = "Admin" });
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl,
                                Message = "Login successfully done."
                            });
                        }
                        return Json(new { Success = false, Message = "Incorrect username or password." });
                    }
                    await _signInManager.SignOutAsync();
                    return Json(new { Success = false, Message = "Please verify your email address." });
                }
                catch (Exception e)
                {
                    return Json(new { Success = false, Message = e.Message });
                }
            return Json(new { Success = false, Message = GetModelStateErrorMessage() });
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Login), "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> LoginStatus()
        {
            try
            {
                var applicationuser = await _userManager.GetUserAsync(HttpContext.User);
                var DashboardUrl = Url.Action("Index", "Dashboard", new { Area = "B2B" });
                var company = await _bllAuth.GetB2BInformation(applicationuser.B2BId);
                return Json(new
                {
                    Success = true,
                    RedirectUrl = DashboardUrl.Trim(),
                    B2BLogo = company.Logo,
                    applicationuser.B2BId,
                    B2BAddress = company.Address
                });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Message = "Internal exception occured." });
            }
        }

        #endregion

        #region "Change Password"

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangePasswordConfirmation()
        {
            return View();
        }

        public ActionResult AdminPassword()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult AdminPasswordConfirmation()
        {
            return View();
        }

        public ActionResult CustomerPassword()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult CustomerPasswordConfirmation()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await _userManager.FindByIdAsync(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                var result =
                    await _userManager.ChangePasswordAsync(currentUser, model.CurrentPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByIdAsync(currentUser.Id);
                    if (user != null)
                        await _signInManager.SignInAsync(user, false);
                    return Json(new
                    {
                        Success = true,
                        RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                        Message = MessageManager.CHANGE_PASSWORD_SUCCESS
                    });
                }
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.CHANGE_PASSWORD_INCORRECT_CURRENT_PASSWORD
                });
            }
            return Json(new
            {
                Success = false,
                RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                Message = GetModelStateErrorMessage()
            });
        }

        #endregion

        #region "Reset Password"

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> ForgotPassword(string EmailAddress)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(EmailAddress);
                if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
                    return Json(new
                    {
                        Success = false,
                        RedirectUrl = "",
                        Message = MessageManager.RESET_PASSWORD_INVALID_USER
                    });

                var Token = await _userManager.GeneratePasswordResetTokenAsync(user);

                var callbackUrl = Url.Action("ResetPassword", "Account", new { EmailAddres = user.Email, Token },
                    Request.Scheme);

                var message =
                    "<div style=\"width: 15%; float: left; color: transparent;\">---</div> <div style=\"width: 70%; min-height: 400px; background-color: ghostwhite;float: left; \">" +
                    "<div style=\"margin-bottom: 2%; width: 59%; margin-top: 5%; margin-left: 5%;\"><img src=\"" +
                    _configuration.GetSection("ApplicationInformation")["Logo"] + "\"></div>" +
                    "<div style=\"width: 100%; \">" +
                    "<div style =\"width: 5%; float: left; color: transparent;\"> ---</div>" +
                    "<div style =\"width: 88%; background-color: white; float: left; padding: 1%; \">" +
                    "<div style =\"width: 100%; margin-bottom: 3%;\">Hi " + user.DisplayName + ",</div> " +
                    "<div style =\"width: 100%; margin-bottom: 3%;\"> Please click on the below button for reset your password<br/>" +
                    "<a href=\"" + callbackUrl +
                    "\" style=\" margin-top: 5%; background-color: #4b0082; border: none; color: white; padding: 10px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 5%;\">Reset</a>" +
                    "</div>" +
                    "<div style =\"width: 100%; margin-bottom: 3%;\">Alternatively please click the link below:<br/>" +
                    "<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>" +
                    "</div>" +
                    "<div style =\"width: 100%;\">Thanks</div>" +
                    "<div style =\"width: 100%;\">" + _configuration.GetSection("ApplicationInformation")["Name"] +
                    " Support Team</div>" +
                    "</div> " +
                    "<div style =\"width: 2%; float: left; color: transparent;\">---</div>" +
                    "</div>" +
                    "<div style=\"width: 100%; float: left; color: gray; margin-bottom:1%; margin-left: 5%; margin-top: 2%;\">This is a no-reply email. To make an inquiry, please contact our help.</div>" +
                    "<div style=\"width: 100%; float: left; color: gray; margin-bottom:3%; margin-left: 5%;\">Copyright © " +
                    _configuration.GetSection("ApplicationInformation")["Name"] + ". All rights reserved.</div>" +
                    "</div> <div style=\"width: 15%; float: left; color: transparent;\">---</div> ";

                var emailUtil = new EmailUtil();
                await emailUtil.SendEmail(user.Email, MessageManager.RESET_PASSWORD_EMAIL_SUBJECT, message,
                    _configuration);

                return Json(new
                {
                    Success = true,
                    RedirectUrl = Url.Action("ForgotPasswordConfirmation", "Account", null),
                    Message = MessageManager.RESET_PASSWORD_REQUEST_MESSAGE
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.RESET_PASSWORD_FAIL_MESSAGE
                });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string EmailAddres, string Token)
        {
            ViewBag.EmailAddress = EmailAddres;
            ViewBag.Token = Token;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null)
                    return Json(new
                    {
                        Success = false,
                        RedirectUrl = "",
                        Message = MessageManager.RESET_PASSWORD_INVALID_USER
                    });
                var result = await _userManager.ResetPasswordAsync(user, model.Token, model.NewPassword);
                if (result.Succeeded)
                    return Json(new
                    {
                        Success = true,
                        RedirectUrl = Url.Action("ResetPasswordConfirmation", "Account", null),
                        Message = MessageManager.RESET_PASSWORD_SUCCESS_MESSAGE
                    });
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.RESET_PASSWORD_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                Message = GetModelStateErrorMessage()
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        #endregion
    }
}