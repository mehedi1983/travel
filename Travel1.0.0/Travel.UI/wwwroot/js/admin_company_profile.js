﻿
var initCompanyDatatable = function () {
    var datatable = $('.company_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#companyListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'businessName',
                title: 'Business Name',
                width: 250
            },
            {
                field: 'mobileNumber',
                title: 'Mobile Number',
                width: 110
            },
            {
                field: 'cityName',
                title: 'City Name',
                width: 100
            },
            {
                field: 'countryName',
                title: 'Country Name',
                width: 100
            },
            {
                field: 'email',
                title: 'Email',
                width: 200
            },
            {
                field: 'emailVerified',
                title: 'Email Verification',
                width: 120,
                    template: function (row) {
                        var status = {
                            'true': { 'title': 'True', 'class': ' m-badge--success' },
                            'false': { 'title': 'False', 'class': ' m-badge--danger' },
                        };
                        return '<span class="m-badge ' +
                            status[row.emailVerified].class +
                            ' m-badge--wide">' +
                            status[row.emailVerified].title +
                            '</span>';
                    }
            },
            {
                field: 'createdDateView',
                title: 'Join Date',
                width: 100,
            },
            {
                field: 'Actions',
                title: 'Action',
                width: 80,
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return " <a onclick=\"javascript:if (confirm('Are you sure you want to send email verification link?') == false) return false; sendEmailVerificationLink('" + row.id + "','" + row.email + "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Send Email Verification Link\"><i class=\"la la-send\"></i></a>";

                }
            }
        ]
    });

    $('#m_form_status').on('change',
        function () {
            datatable.search($(this).val(), 'Status');
        });

};

function sendEmailVerificationLink(Id, EmailAddress) {
    blockUI_Page_Sending();
    
    var fd = new FormData();
    fd.append("Id", Id);
    fd.append("EmailAddress", EmailAddress);

    $.ajax({
        type: "POST",
        url: $("#sendEmailVerificationLinkUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {

            if (data.success == true) {
                alert(data.message);
            } else {
                alert(data.message);
            }
            unblockUI_Page_Sending();
        },
        failure: function (response) {
            unblockUI_Modal();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Modal();
        }
    });
}