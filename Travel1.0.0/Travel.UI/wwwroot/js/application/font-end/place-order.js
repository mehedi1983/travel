﻿
appModule.controller('placeOrderCtrl', ['$filter', '$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($filter, $http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "Place Your Order";
        $scope.path = '/Order/';
        $scope.currentUserInfoUrl = 'LoadUserInfoWithCityAndLocationList';
        $scope.loadLocationUrl = 'LoadLocation';
        $scope.placeOrderUrl = 'Create';
        $scope.confirmOrderUrl = 'Confirm';
        $scope.checkMenuScheduleUrl = $scope.path + '/CheckMenuSchedule';

        $scope.chkDeliverToday = false;
        $scope.availableDateRange = "";
        $scope.availableTimeRange = "";
        $scope.availableStartTime = "";
        $scope.availableEndTime = "";

        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.subTotal = 0;
        $scope.grandTotal = 0;
        $scope.MenuSchedule = null;
        $scope.orderMenu = {
            Id: null
            , Email: null
            , MobileNo: null
            , CityId: null
            , LocationId: null
            , Address: null
            , DeliveryDate: ApplicationConfiguration.currentDate_DD_MM_YYYY_FORMAT() //null
            , DeliveryTime: null
            , OrderDate: ApplicationConfiguration.currentDate()
            , OrderTime: ApplicationConfiguration.currentTime()
            , CreatedDate: ApplicationConfiguration.currentDate()
            , MenuOrderDetailsViewModels: []
            , MenuScheduleModel: []
            , AvailableStartTime: null
            , AvailableEndTime: null
        };

        $scope.orderMenuDetails = {
            MenuId: null
            , OrderQty: null
        };

        $scope.orderMenuDetailsList = [];

        //............................................. Get Current User Info...............................................
        $scope.getCurrentUserInfo = function () {
            $http({
                method: "GET",
                url: $scope.currentUserInfoUrl,
                params: {}
            }).then(function mySuccess(response) {
                var value = response.data.value;
                $scope.orderMenu.Email = value.email;
                $scope.orderMenu.MobileNo = value.mobileNumber;
                $scope.orderMenu.Address = value.address;
                $scope.CityList = value.citySummaryModels;
                $scope.LocationList = value.locationSummaryModels;
                //$scope.orderMenu.DeliveryDate = ApplicationConfiguration.currentDate_DD_MM_YYYY_FORMAT();

                if (value.locationId != null) {
                    $scope.SelectedCity = { id: value.cityId, name: value.cityName };
                    $scope.SelectedLocation = { id: value.locationId, name: value.locationName };
                }

                //$scope.MenuTypeList = value;
            });
        };
        //..............................................................End.........................................

        //...........City Select Box Selected Value Change Event..............
        $scope.CitySelectedChange = function () {
            var CityId = "";
            if (typeof $scope.SelectedCity === "undefined")
                CityId = "0";
            else
                CityId = $scope.SelectedCity.id;

            blockUI_Page_Loading_Info();
            $http({
                method: "GET",
                url: $scope.loadLocationUrl,
                params: { "CityId": CityId }// 
            }).then(function mySuccess(response) {
                $scope.LocationList = response.data.value;
                unblockUI_Page_Loading_Info();
            }, function myError(response) {
                unblockUI_Page_Loading_Info();
            });
        };
        //................End Selected Value Change Event.....................

        $scope.getSubTotal = function (item, index) {
            if (index == 0)
                $scope.subTotal = 0;
            if (item) {
                $scope.subTotal += item.SubTotal;
            }
        };

        $scope.placeOrder = function () {
            tempDeliveryDate = $("#DeliveryDate").val();
            if ($scope.menuOrderForm.$valid) {
                if ($("#DeliveryDate").val() == '') {
                    myNotification("Please select delivery date", 'Warning!', 'danger', 'la la-warning');
                    return false;
                }
                blockUI_Page();
                
                var deliveryDate = ApplicationConfiguration.convertDateFormat_DDMMYYYY_To_MMDDYYYY($("#DeliveryDate").val());
                var deliveryTime = $("#DeliveryTime").val();
                $scope.orderMenu.DeliveryDate = deliveryDate;
                $scope.orderMenu.DeliveryTime = deliveryTime;
                $scope.orderMenu.DeliveryCharge = $scope.MenuSchedule.deliveryCharge;
                $scope.orderMenu.CityId = $scope.SelectedCity.id;
                $scope.orderMenu.LocationId = $scope.SelectedLocation.id;
                $scope.orderMenu.MenuOrderDetailsViewModels = angular.toJson($rootScope.menuCartList);
                $scope.orderMenu.AvailableStartTime = $scope.MenuSchedule.startTime;
                $scope.orderMenu.AvailableEndTime = $scope.MenuSchedule.endTime;
                $http({
                    method: 'POST',
                    url: $scope.placeOrderUrl,
                    params: $scope.orderMenu
                    //headers: { 'Content-Type': "application/json" },
                    //dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        myNotification('Your order no is ' + response.data.orderNo, 'Order Placed Successfully', 'focus', 'la la-plus');
                        $scope.clearMenuCartList();
                        window.location.href = $scope.confirmOrderUrl + "/" + response.data.orderNo;
                    } else {
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    }
                    $scope.orderMenu.DeliveryDate = tempDeliveryDate;
                    unblockUI_Page();
                }), function errorCallback(response) {
                    unblockUI_Page();
                }
            }
        };

        $scope.getCurrentUserInfo();

        $scope.clearMenuCartList = function () {
            $rootScope.menuCartList = [];
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
        }

        $scope.getMenuAvailableSchedule = function () {
            blockUI_Page();
            $scope.orderMenu.MenuOrderDetailsViewModels = angular.toJson($rootScope.menuCartList);
            $http({
                method: 'POST',
                url: $scope.checkMenuScheduleUrl,
                params: { "MenuOrderDetailsViewModels": $scope.orderMenu.MenuOrderDetailsViewModels}
                //headers: { 'Content-Type': "application/json" },
                //dataType: 'JSON'
            }).then(function successCallback(response) {
                $scope.MenuSchedule = response.data.menuSchedule;
                $scope.grandTotal = $scope.subTotal + $scope.MenuSchedule.deliveryCharge;

                $scope.dateTimeInit($scope.MenuSchedule);

                unblockUI_Page();
            }), function errorCallback(response) {
                unblockUI_Page();
            }
        };

        $scope.getMenuAvailableSchedule();

        $scope.dateTimeInit = function(menuSchedule) {

            var startDate = new Date(menuSchedule.startDate);
            var endDate = new Date(menuSchedule.endDate);

            var currentDate = new Date();
            if (currentDate >= startDate) {
                if (menuSchedule.availAbleToday)
                    startDate.setDate(currentDate.getDate());
                else
                    startDate.setDate(currentDate.getDate() + 1);
                
            } else {
                if (menuSchedule.availAbleToday)
                    startDate.setDate(startDate.getDate());
                else
                    startDate.setDate(startDate.getDate() + 1);
            }

            if (startDate >= endDate) {
                $scope.availableDateRange = "Not Available";
            } else
                $scope.availableDateRange = "Available from " + startDate.getDate() + "/" + (startDate.getMonth() + 1) + "/" + startDate.getFullYear() + " to " + endDate.getDate() + "/" + (endDate.getMonth() + 1) + "/" + endDate.getFullYear();

            $('.m_datepicker_customer').datepicker({
                startDate: startDate,
                endDate: endDate,
                rtl: mUtil.isRTL(),
                todayHighlight: true,
                clearBtn: true,
                orientation: "bottom left",
                autoclose: true,
                format: 'dd/mm/yyyy'
            }); 
            
            $('.m_datepicker_customer').val('');

            var startTimeSplit = menuSchedule.startTime.split(':');
            var startTime = new Date();
            startTime.setHours(+startTimeSplit[0]); 
            startTime.setMinutes(startTimeSplit[1]);
            startTime.setSeconds(startTimeSplit[2]);
            $scope.availableStartTime = ApplicationConfiguration.getTime12HourFormat(startTime);

            var endTimeSplit = menuSchedule.endTime.split(':');
            var endTime = new Date();
            endTime.setHours(+endTimeSplit[0]);
            endTime.setMinutes(endTimeSplit[1]);
            endTime.setSeconds(endTimeSplit[2]);
            $scope.availableEndTime = ApplicationConfiguration.getTime12HourFormat(endTime);
            
            $scope.availableTimeRange = "Available from " + $scope.availableStartTime +
                " to " + $scope.availableEndTime;

            $('.m_timepicker_1, .m_timepicker_1_modal').timepicker();
            //$('.m_timepicker_1').val('');

            
        };
        
    }]);




















