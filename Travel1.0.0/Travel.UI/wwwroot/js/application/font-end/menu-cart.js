﻿
appModule.controller('menuCartCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "Place Your Order";
        $scope.path = '/Order/';
        $scope.loginUrl = '/Account/LoginNew';
        $scope.signupUrl = '/Account/B2cRegistration';
        $scope.forgetPasswordUrl = '/Account/ForgotPassword';
        $scope.orderUrl = $scope.path + 'Index';
        $scope.placeOrderUrl = $scope.path + 'Place';
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.subTotal = 0;

        //$scope.menuTempList = $rootScope.menuCartList;

        $scope.loginModel = {
            Email: null
            , Password: null
        };

        $scope.signupModel = {
            Name: null
            , Email: null
            , MobileNo: null
            , Password: null
            , ConfirmPassword: null
        };

        $scope.menuCartModel = {
            MenuId: null
            , MenuTitle: null
            , MOQ: null
            , Price: null
            , SubTotal: null
        };

        $scope.loadSignupMode = function() {
            $("#popupSignupName").val('');
            $("#popupSignupEmail").val('');
            $("#popupSignupPassword").val('');
            $("#popupSignupCheckPassword").val('');
            $("#lblB2cModalSignupMsg").empty();
            $("#b2c_sign_up_modal").modal();
        };

        $scope.loadLoginModal = function() {
            $("#popupLoginEmail").val('');
            $("#popupLoginPassword").val('');
            $("#lblModalLoginMsg").empty();
            $("#b2c_login_modal").modal();
        };

        $scope.forgetPassword = function () {
            window.location.href = $scope.forgetPasswordUrl;
        };

        //.............................................................Save.........................................
        $scope.b2cLogin = function () {
            if ($scope.b2c_login_modal_form.$valid) {
                blockUILogin();
                $http({
                    method: 'POST',
                    url: $scope.loginUrl,
                    params: $scope.loginModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success) {
                        window.location.href = $scope.placeOrderUrl;
                    } else {
                        $("#lblB2bModalLoginMsg").append(response.data.message);
                    }
                    unBlockLoginSignupUI();
                }), function errorCallback(response) {
                    unBlockLoginSignupUI();
                };
            }
        };
        //..............................................................End.........................................

        //.............................................................Save.........................................
        $scope.b2cSignup = function () {
            if ($scope.b2c_sign_up_modal_form.$valid) {
                blockUISignUp();
                $http({
                    method: 'POST',
                    url: $scope.signupUrl,
                    params: $scope.signupModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success) {
                        window.location.href = $scope.placeOrderUrl;
                    } else {
                        $("#lblB2cModalSignupMsg").append(response.data.message);
                    }
                    unBlockLoginSignupUI();
                }), function errorCallback(response) {
                    unBlockLoginSignupUI();
                };
            }
        };
        //..............................................................End.........................................

        $scope.increaseMenuQty = function (menuId) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            var index = $rootScope.menuCartList.findIndex(record => record.MenuId === menuId);
            $rootScope.menuCartList[index].OQ = $rootScope.menuCartList[index].OQ + 1;
            $rootScope.menuCartList[index].SubTotal = $rootScope.menuCartList[index].OQ * $rootScope.menuCartList[index].Price;
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.subTotal = 0;
        };

        $scope.decreaseMenuQty = function (menuId) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            var index = $rootScope.menuCartList.findIndex(record => record.MenuId === menuId);

            if ($rootScope.menuCartList[index].MOQ == $rootScope.menuCartList[index].OQ)
                myNotification('Minimum order quantity is ' + ' ' + $rootScope.menuCartList[index].MOQ, 'Warning!!!!', 'danger', 'la la-warning');
            else
                $rootScope.menuCartList[index].OQ = $rootScope.menuCartList[index].OQ - 1;

            $rootScope.menuCartList[index].SubTotal = $rootScope.menuCartList[index].OQ * $rootScope.menuCartList[index].Price;
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.subTotal = 0;
        };

        $scope.deleteMenuFromCart = function (menuId, $index) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            $rootScope.menuCartList.splice($index, 1);
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.subTotal = 0;
        };

        $scope.placeOrder = function () {
            window.location.href = $scope.placeOrderUrl;
        };

        $scope.getSubTotal = function (item, index) {
            if (index == 0)
                $scope.subTotal = 0;
            if (item) {
                $scope.subTotal += item.SubTotal;
            }
        };

    }]);




