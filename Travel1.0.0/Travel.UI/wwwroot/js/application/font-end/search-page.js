﻿
appModule.controller('searchCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "";
        $scope.menuId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.path = '/Search/';
        $scope.menuUrl = $scope.path + 'GetMenu';
        $scope.similarMenuUrl = $scope.path + 'GetSimilarMenu';
        $scope.searchByMenuTypeUrl = $scope.path + '';
        $scope.searchUrl = $scope.path + 'Search';
        $scope.searchSimilarMenuUrl = $scope.path + 'GetSimilarMenu';
        $scope.successFailMsg = "";
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.menuDetailsUrl = $scope.path + 'Index';
        $scope.MenuInfo = [];
        $scope.MenuPictures = [];
        
        $scope.menuCartModel = {
            MenuId: null
            , MenuTitle: null
            , MOQ: null
            , OQ: null
            , Price: null
            , SubTotal: null
        };

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 4;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];
        
        $scope.getDataTableValue = function () {
            dataTableConfiguration.getValueByMenuId($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchSimilarMenuUrl, $rootScope.filterByStatus, $scope.menuId).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
                //unblockUI_Loading_Menu();
            });
        };

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function () {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................


        $scope.addToCart = function () {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            var ExistChk = -1;
            if ($rootScope.menuCartList !== null)
                ExistChk = $rootScope.menuCartList.findIndex(record => record.MenuId === $scope.MenuInfo.id);
            else
                $rootScope.menuCartList = [];

            if (ExistChk !== -1) {
                myNotification($scope.MenuInfo.title + ' already added into cart', 'Warning!!!!', 'danger', 'la la-warning');
            } else {
                $scope.menuCartModel.MenuId = $scope.MenuInfo.id;
                $scope.menuCartModel.MenuTitle = $scope.MenuInfo.title;
                $scope.menuCartModel.MOQ = $scope.MenuInfo.minimumOrderQty;
                $scope.menuCartModel.OQ = $scope.MenuInfo.minimumOrderQty;
                $scope.menuCartModel.Price = $scope.MenuInfo.price;
                $scope.menuCartModel.SubTotal = ($scope.MenuInfo.minimumOrderQty * $scope.MenuInfo.price);
                $rootScope.menuCartList.push($scope.menuCartModel);
                sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));

                //$rootScope.menuCartItemCount = $rootScope.menuCartList.length;

                $('ul.m-topbar__nav').find('li.test').trigger('click');

                myNotification($scope.MenuInfo.title + ' added into cart', 'Success!!!', 'focus', 'la la-plus');
            }            
        };


        $scope.showrating = function() {
            $(".menu-rating").starRating({
                initialRating: 1,
                totalStars: 1,
                emptyColor: 'lightgray',
                ratedColor: '#9816f4',
                activeColor: '#9816f4',
                readOnly: true
            });
        };

        $scope.showrating();

        $scope.showSingleMenurating = function(score) {
            $(".single-menu-rating").starRating({
                initialRating: score,
                totalStars: 5,
                emptyColor: 'lightgray',
                ratedColor: '#9816f4',
                activeColor: '#9816f4',
                readOnly: true
            });
        };




        //............................................. Get Menu Type................................................
        $scope.getMenu = function () {
            blockUI_Loading_Menu();
            $http({
                method: "GET",
                url: $scope.menuUrl,
                params: { "Id": $scope.menuId}
            }).then(function mySuccess(response) {
                $scope.MenuInfo = response.data.value;
                $scope.MenuPictures = response.data.value.menuPictures;
                $rootScope.formTitle = $scope.MenuInfo.title;
                $scope.showSingleMenurating($scope.MenuInfo.ratingScore);

                $scope.getDataTableValue();
                unblockUI_Loading_Menu();
            });
        };
        //..................................................End......................................................

        $scope.getMenu();
        
        $scope.onSelect = function ($item, $model, $label) {
            alert($item.menuId);

            //$scope.$item = $item;
            //$scope.$model = $model;
            //$scope.$label = $label;
        };

        //.............................................Search By Menu type.........................................
        $scope.searchByMenuType = function (Id) {
            alert(Id);
            //$window.location.href = $scope.menuDetailsUrl + "/" + Id;
        };
        //..................................................End......................................................

        $scope.showMenuDetails = function (Id) {
            //alert(Id);
            $window.location.href = $scope.menuDetailsUrl + "/" + Id;

        };

    }]);


