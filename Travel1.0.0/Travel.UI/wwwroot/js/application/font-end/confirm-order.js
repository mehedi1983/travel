﻿
appModule.controller('confirmOrderCtrl', ['$filter', '$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($filter, $http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "Confirm Order";
        $scope.orderId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.path = '/Order/';
        $scope.confirmOrderUrl = $scope.path + 'LoadConfirmedOrder';
        $scope.sendOrderPlacedNotificationUrl = $scope.path + 'SendOrderPlacedNotification';
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.orderDetails=null;

        $scope.goBackToHome = function () {
            $window.location.href = "/";
        };

        //....................................................... Get Order Info...............................................
        $scope.getConfirmedOrder = function () {
            $http({
                method: "GET",
                url: $scope.confirmOrderUrl,
                params: { "Id": $scope.orderId}
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
                $scope.sendOrderPlacedNotification();
            });
        };
        //..............................................................End....................................................

        //....................................................... Get Order Info...............................................
        $scope.sendOrderPlacedNotification = function () {
            $http({
                method: "GET",
                url: $scope.sendOrderPlacedNotificationUrl,
                params: { "Id": $scope.orderId }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End....................................................

        $scope.getConfirmedOrder();


    }]);




















