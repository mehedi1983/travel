﻿
appModule.controller('homeSearchCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Order Takeaway Online From Local Chefs Kitchen";
        $scope.path = '/Home/';
        $scope.menuTypeUrl = $scope.path + 'GetMenuTypeList';
        $scope.menuSearchUrl = $scope.path + 'GetMenuList';
        $scope.backUrl = 'Search/Index';
        $scope.searchByMenuTypeUrl = 'Search/SearchByMenuType';
        $scope.searchUrl = $scope.path + 'Search';
        $scope.successFailMsg = "";
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.MenuTypeList = [];

        //............................................. Get Menu Type................................................
        $scope.getMenuType = function () {

            $http({
                method: "GET",
                url: $scope.menuTypeUrl,
                params: { }
            }).then(function mySuccess(response) {
                $scope.MenuTypeList = response.data.value;
            });
        };
        //..................................................End......................................................

        //$scope.getMenuType();

        //............................................. Get Menu Type...............................................
        $scope.loadMenuSearchType = function (val) {
            return $http.get($scope.menuSearchUrl, {
                params: {
                    "SearchString": val
                }
            }).then(function (response) {
                return response.data.value;
                });
        };
        //..............................................................End.........................................

        $scope.onSelect = function ($item, $model, $label) {
            //alert($item.menuId);

            $window.location.href = $scope.backUrl + "/" + $item.menuId;

            //$scope.$item = $item;
            //$scope.$model = $model;
            //$scope.$label = $label;
        };

        //.............................................Search By Menu type.........................................
        $scope.searchByMenuType = function (Id) {
            $window.location.href = $scope.searchByMenuTypeUrl + "/" + Id;
        };

        //..................................................End......................................................

    }]);


