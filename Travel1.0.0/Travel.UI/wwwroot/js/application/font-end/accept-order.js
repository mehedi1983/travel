﻿
appModule.controller('acceptOrderCtrl', ['$filter', '$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($filter, $http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "Accept Order";
        $scope.orderId = $("#OrderId").val(); // $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.chefId = $("#ChefId").val(); // $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.path = '/Order/';
        $scope.sendOrderAcceptNotificationUrl = $scope.path + 'AcceptReceivedOrder';
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.orderDetails=null;

        //....................................................... Get Order Info...............................................
        $scope.acceptOrder = function () {
            $http({
                method: "POST",
                url: $scope.sendOrderAcceptNotificationUrl,
                params: { "OrderId": $scope.orderId, "ChefId": $scope.chefId }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End....................................................

        $scope.acceptOrder();


    }]);




















