﻿
appModule.controller('searchByMenuTypeCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "Search By Menu Type";
        $scope.menuTypeId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.path = '/Search/';
        $scope.menuDetailsUrl = '/Search/Index';
        $scope.getMenuByMenuTypeUrl = $scope.path + 'GetMenuByMenuType';
        $scope.menuTypeUrl = '/Home/GetMenuTypeList';
        $scope.searchByMenuTypeUrl = '/Search/SearchByMenuType';
        $scope.searchUrl = $scope.path + 'SearchMenuByMenuType';
        $scope.successFailMsg = "";
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.SimilarmMenuInfo = [];
        //$scope.NoOfSearchResult = 0;

        //............................................. Get Menu Type................................................
        $scope.getMenuType = function () {

            $http({
                method: "GET",
                url: $scope.menuTypeUrl,
                params: {}
            }).then(function mySuccess(response) {
                $scope.MenuTypeList = response.data.value;
            });
        };
        //..................................................End......................................................

        //.............................................Search By Menu type.........................................
        $scope.searchByMenuType = function (Id) {
            $window.location.href = $scope.searchByMenuTypeUrl + "/" + Id;
        };
        //..................................................End......................................................

        $scope.getMenuType();

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 12;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];

        $scope.getDataTableValue = function () {
            blockUI_Loading_Menu();
            dataTableConfiguration.getValueById($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchUrl, $rootScope.filterByStatus, $scope.menuTypeId).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
                unblockUI_Loading_Menu();
            });
        };

        $scope.getDataTableValue();

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function () {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................


        $scope.showrating = function() {
            $(".menu-rating").starRating({
                initialRating: 1,
                totalStars: 1,
                emptyColor: 'lightgray',
                ratedColor: '#9816f4',
                activeColor: '#9816f4',
                readOnly: true
            });
        }

        $scope.showrating();


























        //............................................. Get Similar Menu.............................................
        $scope.getMenuByMenuType = function () {
            $http({
                method: "GET",
                url: $scope.getMenuByMenuTypeUrl,
                params: { "Id": $scope.menuTypeId }
            }).then(function mySuccess(response) {
                $scope.SimilarmMenuInfo = response.data.value;
            });
        };
        //..................................................End......................................................

        //$scope.getMenuByMenuType();

        $scope.showMenuDetails = function (Id) {
            $window.location.href = $scope.menuDetailsUrl + "/" + Id;
        };

    }]);


