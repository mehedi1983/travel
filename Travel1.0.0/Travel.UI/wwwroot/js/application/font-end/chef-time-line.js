﻿
appModule.controller('searchByMenuTypeCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $rootScope.formTitle = "";
        $scope.chefId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $scope.path = '/Search/';
        $scope.menuDetailsUrl = '/Search/Index';
        $scope.chefUrl = $scope.path + 'GetChef';
        $scope.searchUrl = $scope.path + 'SearchMenuByChef';
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();
        $scope.ChefInfo = [];

        //............................................. Get Menu Type................................................
        $scope.getChef = function () {

            $http({
                method: "GET",
                url: $scope.chefUrl,
                params: { "Id": $scope.chefId}
            }).then(function mySuccess(response) {
                $scope.ChefInfo = response.data.value;
                $rootScope.formTitle = $scope.ChefInfo.name;
            });
        };
        //..................................................End......................................................

        $scope.getChef();

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 12;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];

        $scope.getDataTableValue = function () {
            blockUI_Loading_Menu();
            dataTableConfiguration.getValueByChefId($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchUrl, $rootScope.filterByStatus, $scope.chefId).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
                unblockUI_Loading_Menu();
            });
        };

        $scope.getDataTableValue();

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function () {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................

        $scope.showMenuDetails = function (Id) {
            $window.location.href = $scope.menuDetailsUrl + "/" + Id;
        };





























        


        

    }]);


