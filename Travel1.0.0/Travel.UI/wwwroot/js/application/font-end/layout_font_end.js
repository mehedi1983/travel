﻿
appModule.controller('signUpSignInPopup', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $scope.path = '/Account';
        $scope.loginUrl = $scope.path + '/LoginNew';
        $scope.signupUrl = $scope.path + '/RegisterUser';
        $scope.orderUrl = '/Search/MenuCart';
        $scope.cartSubTotal = 0;
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

        $scope.loginModel = {
            Email: null
            , Password: null
        };

        $scope.signupModel = {
            Name: null
            , Email: null
            , Password: null
            , ConfirmPassword: null
        };

        $scope.menuCartModel = {
            MenuId: null
            , MenuTitle: null
            , MOQ: null
            , Price: null
            , SubTotal: null
        };

        $scope.loadModalInInsertMode = function() {
            $("#popupSignupName").val('');
            $("#popupSignupEmail").val('');
            $("#popupSignupPassword").val('');
            $("#popupSignupCheckPassword").val('');
            $("#lblModalSignupMsg").empty();
            $("#sign_up_modal").modal();
        };

        $scope.loadLoginModal = function() {
            $("#popupLoginEmail").val('');
            $("#popupLoginPassword").val('');
            $("#lblModalLoginMsg").empty();
            $("#login_modal").modal();
        };

        //.............................................................Save.........................................
        $scope.Login = function () {
            if ($scope.login_modal_form.$valid) {
                blockUILogin();
                $http({
                    method: 'POST',
                    url: $scope.loginUrl,
                    params: $scope.loginModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success) {
                        window.location.href = response.data.redirectUrl;
                    } else {
                        $("#lblModalLoginMsg").append(response.data.message);
                    }
                    unBlockLoginSignupUI();
                }), function errorCallback(response) {
                    unBlockLoginSignupUI();
                };
            }
        };
        //..............................................................End.........................................

        //.............................................................Save.........................................
        $scope.Signup = function () {
            if ($scope.sign_up_modal_form.$valid) {
                blockUISignUp();
                $http({
                    method: 'POST',
                    url: $scope.signupUrl,
                    params: $scope.signupModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success) {
                        window.location.href = response.data.redirectUrl;
                    } else {
                        $("#lblModalSignupMsg").append(response.data.message);
                    }
                    unBlockLoginSignupUI();
                }), function errorCallback(response) {
                    unBlockLoginSignupUI();
                };
            }
        };
        //..............................................................End.........................................

        //......................................................Check Cart Item.....................................
        $scope.CheckCartItem = function () {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
        };
        //..............................................................End.........................................
        $scope.CheckCartItem();

        //......................................................Check Cart Item.....................................
        $scope.showCartItem = function () {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
        };
        //..............................................................End.........................................

        $scope.increaseMenuQty = function (menuId) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            var index = $rootScope.menuCartList.findIndex(record => record.MenuId === menuId);
            $rootScope.menuCartList[index].OQ = $rootScope.menuCartList[index].OQ + 1;
            $rootScope.menuCartList[index].SubTotal = $rootScope.menuCartList[index].OQ * $rootScope.menuCartList[index].Price;
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.cartSubTotal = 0;
        };

        $scope.decreaseMenuQty = function (menuId) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            var index = $rootScope.menuCartList.findIndex(record => record.MenuId === menuId);

            if ($rootScope.menuCartList[index].MOQ == $rootScope.menuCartList[index].OQ)
                myNotification('Minimum order quantity is ' + ' ' + $rootScope.menuCartList[index].MOQ , 'Warning!!!!', 'danger', 'la la-warning');
            else
                $rootScope.menuCartList[index].OQ = $rootScope.menuCartList[index].OQ - 1;
            
            $rootScope.menuCartList[index].SubTotal = $rootScope.menuCartList[index].OQ * $rootScope.menuCartList[index].Price;
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.cartSubTotal = 0;
        };

        $scope.deleteMenuFromCart = function (menuId, $index) {
            $rootScope.menuCartList = angular.fromJson(sessionStorage.getItem(ApplicationConfiguration.addToCartSessionStorageName()));
            $rootScope.menuCartList.splice($index, 1);
            if ($rootScope.menuCartList.length == 0)
                $("#m_quick_sidebar_close").click();
            sessionStorage.setItem(ApplicationConfiguration.addToCartSessionStorageName(), angular.toJson($rootScope.menuCartList));
            $scope.cartSubTotal = 0;
        };

        $scope.placeOrder = function() {
            window.location.href = $scope.orderUrl;
        };

        $scope.getCartSubTotal = function (item, index) {
            if (index == 0)
                $scope.cartSubTotal = 0;
            if (item) {
                $scope.cartSubTotal += item.SubTotal;
            }
        };

    }]);

function myNotification (message, title, type, icon) {
    var content = {};
    //content.url = 'www.keenthemes.com';
    //content.target = '_blank';
    content.message = message;
    content.title = title;
    content.icon = 'icon ' + icon;
    var notify = $.notify(content, {
        type: type,
        allow_dismiss: true,
        newest_on_top: false,
        mouse_over: true,
        showProgressbar: false,
        spacing: 10,
        timer: 200,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 3000,
        z_index: 10000,
        animate: {
            enter: 'bounceIn',
            exit: 'bounceIn'
        }
    });
}