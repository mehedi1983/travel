﻿
appModule.controller('userCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "User Management";
        $scope.saveUpdateButtonCaption = 'Save';
        $scope.modalCaption = 'New';
        $rootScope.settingsTab = "m-menu__item--active-tab";
        $scope.path = '/Admin/UserManagement/';
        $scope.saveUpdateUrl = $scope.path + '/Save';
        $scope.deleteUrl = $scope.path + 'Delete';
        $scope.getUrl = $scope.path + 'GetEmployeeInformation';
        $scope.searchUrl = $scope.path + 'Search';
        $scope.successFailMsg = "";

        $scope.itemModel = {
            EmployeeId: null
            , DisplayName: null
            , UserName: null
            , Password: null
            , CreatedDate: null
        };
        $scope.insertNewMenuUrl = function () {
            $window.location.href = $scope.saveUrl;
        };
        ApplicationConfiguration.getStatus().then(function (response) {
            $scope.StatusList = response.data.value;
        });

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount=0;
        $rootScope.itemViewPerPage= 10;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];
        $scope.EmployeeList = [];
        $scope.Employee = null;
        $scope.getDataTableValue = function () {
            blockUI_Page();
            dataTableConfiguration.getValue($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchUrl, $rootScope.filterByStatus).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
                unblockUI_Page();
            });
        };

        $scope.getDataTableValue();

        $scope.setViewPerPage = function(no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function(pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function() {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................

        //......................................................Get Employee List....................................
        $scope.getEmployeeList = function () {
            $http({
                method: "GET",
                url: $scope.getUrl,
                params: {}
            }).then(function mySuccess(response) {
                $scope.EmployeeList = response.data.value;
            });
        };
        //..............................................................End.........................................
        $scope.getEmployeeList();

        //................................................City Select Box Selected Value Change Event..............
        $scope.employeeSelectedChange = function () {
            if ($scope.Employee != null)
                $scope.itemModel.UserName = $scope.Employee.email;
            else
                $scope.itemModel.UserName = '';
        };
        //.....................................................End Selected Value Change Event.....................


        //..............................................................Modal........................................
        $scope.loadInsertModal = function() {
            $scope.Clear();
            $("#user_modal").modal();
        };
        //...........................................................End Modal.......................................

        //.............................................................Clear...........................................
        $scope.Clear = function () {
            $scope.successFailMsg = "";
            $scope.saveUpdateButtonCaption = 'Save';
            $scope.modalCaption = 'New';
            $scope.itemModel = {};
            return true;
        };
        //...........................................................End Clear.........................................

        //.........................................................Show Message........................................
        $scope.ShowMessage = function (message, type) {
            $scope.OperationMessage = message;
            $scope.successFailMsg = type;
            return true;
        };
        //..............................................................End.............................................

        //.............................................................Save............................................
        $scope.save = function () {
            $scope.itemModel.EmployeeId = $scope.Employee.id;
            $scope.itemModel.DisplayName = $scope.Employee.name;
            $scope.itemModel.CreatedDate = ApplicationConfiguration.currentDate();
            if ($scope.itemForm.$valid) {
                blockUI_Modal();
                $http({
                    method: 'POST',
                    url: $scope.saveUpdateUrl,
                    params: $scope.itemModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        $scope.getDataTableValue();
                        myNotification(response.data.message, 'Create User', 'focus', 'la la-plus');
                        $("#user_modal .close").click();

                    } else {
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    }
                    unblockUI_Modal();
                    
                }), function errorCallback(response) {
                    unblockUI_Modal();
                }
            }
        };
        //...........................................................End Save..........................................

        //.............................................................Remove..........................................
        $scope.remove = function (id) {
            blockUI_Page();
            $http({
                method: "POST",
                url: $scope.deleteUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                if (response.data.success == true) {
                    $scope.getDataTableValue();
                    myNotification(response.data.message, 'Delete User', 'focus', 'la la-plus');
                    unblockUI_Page();
                } else {
                    myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                }
            }, function myError(response) {
                {
                    unblockUI_Page();
                    myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                }
            });
        };
        //..............................................................End............................................        

    }]);


