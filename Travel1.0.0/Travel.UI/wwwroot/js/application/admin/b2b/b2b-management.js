﻿

appModule.controller('chefCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Chef Management";
        $rootScope.dashboardTab = "m-menu__item--active-tab";
        $scope.path = '/Admin/Chef/';
        $scope.searchUrl = $scope.path + 'Search';
        $scope.sendEmailVerificationLintUrl = $scope.path + 'ResendEmailVerificationLink';
        $scope.successFailMsg = "";

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 10;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];

        $scope.getDataTableValue = function () {
            dataTableConfiguration.getValue($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchUrl, $rootScope.filterByStatus).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
            });
        };

        $scope.getDataTableValue();

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function () {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................       

        //.........................................................Show Message........................................
        $scope.ShowMessage = function (message, type) {
            $scope.OperationMessage = message;
            $scope.successFailMsg = type;
            return true;
        };
        //..............................................................End.............................................

        //.............................................................Send Email Verification Link..........................................
        $scope.sendEmailVerificationLink = function (Id) {
            blockUI_Page();
            $http({
                method: 'POST',
                url: $scope.sendEmailVerificationLintUrl,
                params: {"Id": Id},
                dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.ShowMessage(response.data.message, "success");
                } else {
                    $scope.ShowMessage(response.data.message, "fail");
                }
                unblockUI_Page();
            }), function errorCallback(response) {
                unblockUI_Page();
            }
        };
        //..............................................................End............................................        

    }]);