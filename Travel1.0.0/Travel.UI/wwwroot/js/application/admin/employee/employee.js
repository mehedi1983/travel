﻿
appModule.controller('employeeBasicInfoCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $scope.employeeId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $rootScope.formTitle = "Employee Management";
        $rootScope.settingsTab = "m-menu__item--active-tab";
        $scope.path = '/Admin/EmployeeManagement/';
        $scope.saveUrl = $scope.path + '/SaveUpdate';
        $scope.defaultValuePath = $scope.path + '/EmployeeDefaultValue';
        $scope.getEmployeeBasicInfoUrl = $scope.path + 'LoadEmployeeInformationWithCityAndLocationList';
        $scope.loadCityUrl = $scope.path + 'LoadCity';
        $scope.uploadProfilePictureUrl = $scope.path + 'UploadImage';
        $scope.deleteProfilePictureUrl = $scope.path + 'DeleteEmployeeLogo';
        $scope.backUrl = '/Admin/EmployeeManagement';

        $scope.CountryList = [];
        $scope.CityList = [];
        $scope.StatusList = [];
        $scope.employeeInformation = {
            Id: null
            , Name: null
            , Email: null
            , MobileNo: null
            , Address: null
            , CountryId: null
            , CityId: null
            , Status: null
            , CreatedDate: ApplicationConfiguration.currentDate()
        };

        $scope.back = function () {
            $window.location.href = $scope.backUrl;
        };

        //............................................. Load Profile Picture Upload popup...........................
        $scope.loadModalInInsertMode = function() {
            $scope.imgImage = null;
            $("#imgProfilePicture").val("");
            $("#imgImage").attr("src", "");
            $("#imgUploadImage").val('');
            $("#upload_image_modal").modal();
        };
        //...........................................................End............................................

        //............................................. Get form default value in insert mode.......................
        $scope.getFormDefaultValue = function () {
            $http({
                method: "GET",
                url: $scope.defaultValuePath,
                params: {}
            }).then(function mySuccess(response) {
                var value = response.data.value;
                $scope.CountryList = value.countries;
                $scope.StatusList = value.status;
            });
        };
        //..............................................................End.........................................

        //............................................. Get employee basic information..............................
        $scope.getEmployeeBasicInfo = function () {

            // get chef basic information
            $http({
                method: "GET",
                url: $scope.getEmployeeBasicInfoUrl,
                params: { "Id": $scope.employeeId }
            }).then(function mySuccess(response) {

                //...................Pulling chef basic information..................
                var value = response.data.value;
                $rootScope.formTitle = value.name + "'s Profile";
                $scope.employeeInformation.Id = value.id;
                $scope.employeeInformation.Name = value.name;
                $scope.employeeInformation.Email = value.email;
                $scope.employeeInformation.MobileNo = value.mobileNumber;
                $scope.employeeInformation.Address = value.address;
                $scope.CityList = value.citySummaryModels;
                $scope.CountryList = value.countrySummaryModels;
                $scope.StatusList = value.statusSummaryModels;
                $scope.ZipCode = value.zipCode;

                if (value.status == 'Active')
                    $scope.employeeInformation.Status = { id: '1', value: 'Active' };
                else
                    $scope.employeeInformation.Status = { id: '2', value: 'Inactive' };

                if (value.countryId != null) {
                    $scope.SelectedCountry = { id: value.countryId, name: value.countryName };
                }
                if (value.cityId != null) {
                    $scope.SelectedCity = { id: value.cityId, name: value.cityName };
                }

                if (value.picture != null && value.picture != "")
                    $scope.imgProfilePicture = "/ApplicationImage/Employee/" + value.picture;
                else
                    $scope.imgProfilePicture = "/images/avatar.jpg";
                //..........................End pulling...............................                

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
        }
        //..............................................................End.........................................

        //............................................. Form Insert or Update mode check............................
        if ($scope.employeeId == "" || $scope.employeeId == "Profile") {
            $scope.getFormDefaultValue();
            $scope.Action = 'Save';
            $rootScope.formTitle = "Insert New Employee";
        } else {
            $scope.getEmployeeBasicInfo();
            $scope.Action = 'Update';
            $rootScope.formTitle = "Update Employee";
        }
        //..............................................................End.........................................

        //................................................City Select Box Selected Value Change Event..............
        $scope.CountrySelectedChange = function () {

            var CountryId = "";
            if ($scope.SelectedCountry.toString() == 'undefined')
                CountryId = "0";
            else
                CountryId = $scope.SelectedCountry.id;

            blockUI_Page_Loading_Info();
            $http({
                method: "GET",
                url: $scope.loadCityUrl,
                params: { "CountryId": CountryId }// 
            }).then(function mySuccess(response) {
                $scope.CityList = response.data.value;
                unblockUI_Page_Loading_Info();
            }, function myError(response) {
                unblockUI_Page_Loading_Info();
            });
        };
        //.....................................................End Selected Value Change Event.....................
        
        //.............................................Save/Update Employee Basic Information.......................
        $scope.save = function () {
            if ($scope.employeeBasicInformationForm.$valid) {
                $scope.employeeInformation.CountryId = $scope.SelectedCountry.id;
                $scope.employeeInformation.CityId = $scope.SelectedCity.id;
                //$scope.employeeInformation.Status = $scope.Status.value;
                blockUI_Page_Saving_Info();
                $http({
                    method: "POST",
                    url: $scope.saveUrl,
                    params: $scope.employeeInformation
                }).then(function mySuccess(response) {
                    $scope.employeeInformation.Id = response.data.employeeId;
                    $scope.Action = 'Update';
                    unblockUI_Page_Loading_Info();
                    $scope.imgProfilePicture = "/images/avatar.jpg";
                    myNotification(response.data.message, 'Save Successfully', 'focus', 'la la-plus');
                }, function myError(response) {
                    unblockUI_Page_Loading_Info();
                    myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                });
            } else {
                alert("Please fill required fields.");
            }
        };
        //.............................................................End..........................................                    

        //................................................Upload Employee Profile Picture..........................
        $scope.uploadEmployeeProfilePicture = function () {
            var imgFiles = $("#imgUploadImage").get(0).files;

            if (imgFiles.length == '0') {
                alert("Please select a image file.");
                $("#imgUploadImage").focus();
            } else {
                blockUI_Upload_Modal();

                var fd = new FormData();
                fd.append("ProfilePicture", imgFiles[0], imgFiles[0].name);
                fd.append("EmployeeId", $scope.employeeInformation.Id);

                $http.post($scope.uploadProfilePictureUrl,
                    fd,
                    {
                        transformRequest: angular.identity,
                        //params: { "CompanyId": $scope.ChefId},
                        headers: { 'Content-Type': undefined }
                    }).then(function (response) {
                        if (response.data.success == true) {
                            $scope.imgImage = "/ApplicationImage/Employee/" + response.data.fileName;
                            $scope.imgProfilePicture = "/ApplicationImage/Employee/" + response.data.fileName;
                            myNotification(response.data.message, 'Upload Successfully', 'focus', 'la la-plus');
                        }
                        unblockUI_Upload_Modal();
                    }, function errorCallback(response) {
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                        unblockUI_Upload_Modal();
                    });
            }
        };
        //............................................................End Upload...................................  

        //...................................................Remove Employee Profile Picture.......................
        $scope.removeEmployeeProfilePicture = function () {
            if (confirm('Are you sure you want to delete?') == true) {
                blockUI_Page();
                $http({
                    method: "POST",
                    url: $scope.deleteProfilePictureUrl,
                    params: { "Id": $scope.employeeInformation.Id }
                }).then(function mySuccess(response) {
                    if (response.data.success == true) {
                        $scope.imgProfilePicture = "/images/avatar.jpg";
                        myNotification(response.data.message, 'Remove Successfully', 'focus', 'la la-plus');
                    } else {
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    }
                    unblockUI_Page();
                    }, function myError(response) {
                    myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    unblockUI_Page();
                });
            }
        };
        //.............................................................End Remove..................................  

    }]);