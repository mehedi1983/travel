﻿

appModule.controller('employeeCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Employee Management";
        $rootScope.settingsTab = "m-menu__item--active-tab";
        $scope.path = '/Admin/EmployeeManagement/';
        $scope.insertUpdateUrl = $scope.path + 'Profile';
        $scope.searchUrl = $scope.path + 'Search';
        $scope.sendEmailVerificationLintUrl = $scope.path + 'ResendEmailVerificationLink';
        $scope.successFailMsg = "";

        $scope.insertUrl = function () {
            $window.location.href = $scope.insertUpdateUrl;
        };

        $scope.editUrl = function (id) {
            $window.location.href = $scope.insertUpdateUrl + "/" + id;
        };

        //.........................................................Data Table.........................................
        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 10;
        $rootScope.pageNo = 1;
        $rootScope.filterByStatus = "All";
        $scope.itemList = [];
        $scope.StatusList = [];

        $scope.getDataTableValue = function () {
            blockUI_Page();
            dataTableConfiguration.getValue($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.searchUrl, $rootScope.filterByStatus).then(function (response) {
                $scope.itemList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
                unblockUI_Page();
            });
        };

        $scope.getDataTableValue();

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
        };

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.getDataTableValue();
        };

        $scope.setSearchString = function () {
            $scope.getDataTableValue();
        };

        $scope.filterByStatus = function (status) {
            $rootScope.filterByStatus = status;
            $scope.getDataTableValue();
        };

        //..............................................................End..........................................       

        //.........................................................Show Message........................................
        $scope.ShowMessage = function (message, type) {
            $scope.OperationMessage = message;
            $scope.successFailMsg = type;
            return true;
        };
        //..............................................................End.............................................
        
    }]);