﻿
appModule.controller('adminDashboard', ['$scope', '$rootScope', '$window', 'ApplicationConfiguration', '$http', function ($scope, $rootScope, $window, ApplicationConfiguration, $http) {

    $scope.path = '/Admin/AdminDashboard/';
    $scope.chefDashboardInfoUrl = $scope.path + 'LoadAdminDashboardInfo';
    $rootScope.formTitle = "Dashboard";
    $rootScope.dashboardTab = "m-menu__item--active-tab";
    $scope.menuDetailsUrl = '/Admin/MenuManagement/Create/';
    $scope.orderDetailsUrl = '/Admin/Order/ReceivedDetails/';
    $scope.AdminDashboardInfo = [];
    $scope.RecentMenuList = [];

    $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

    $scope.changeProfile = function () {
        $window.location.href = $scope.changeProfileUrl;
    };

    $scope.menuDetails = function (id) {
        $window.location.href = $scope.menuDetailsUrl + id;
    };

    $scope.orderDetails = function (id) {
        $window.location.href = $scope.orderDetailsUrl + id;
    };

    //.......................................................Chef Dashboard Info...............................................

    $scope.showDashboardInfo = function () {
        $http({
            method: "GET",
            url: $scope.chefDashboardInfoUrl,
            params: {}
        }).then(function mySuccess(response) {
            $scope.AdminDashboardInfo = response.data.value;
        });
    };

    //..............................................................End........................................................

    $scope.showDashboardInfo();

}]);


