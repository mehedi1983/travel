﻿

appModule.controller('receivedDetailsCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', '$location',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration, $location) {
        $scope.path = '/Admin/Order/';
        $scope.orderId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $rootScope.formTitle = "Order Details";
        $rootScope.orderTab = "m-menu__item--active-tab";
        $scope.confirmOrderUrl = $scope.path + 'GetReceivedOrderDetails';
        $scope.getCancelOrderUrl = $scope.path + 'CancelOrder';
        $scope.getCancelOrderUrl = $scope.path + 'CancelOrder';
        $scope.denyReceivedOrderDetailsUrl = $scope.path + 'DenyReceivedOrder';
        $scope.acceptReceivedOrderDetailsUrl = $scope.path + 'AcceptReceivedOrder';
        $scope.emailNotificationOfAcceptOrderUrl = $scope.path + 'EmailNotificationOfAcceptOrder';
        $scope.emailNotificationOfCancelOrderUrl = $scope.path + 'EmailNotificationOfCancelOrder';
        $scope.emailNotificationOfDenyOrderUrl = $scope.path + 'EmailNotificationOfDenyOrder';

        $scope.successFailMsg = "";
        $scope.orderDetails = [];
        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

        $scope.denyOrder = function (id) {
            blockUI_Page();
            $http({
                method: 'POST',
                url: $scope.denyReceivedOrderDetailsUrl,
                params: { "Id": id }
                //headers: { 'Content-Type': "application/json" },
                //dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.sendEmailNotificationOfDenyOrder(id);
                    myNotification(response.data.message, 'Order Deny Operation', 'focus', 'la la-close');
                } else {
                    myNotification(response.data.message, 'Order Deny Operation', 'danger', 'la la-warning');
                }
                unblockUI_Page();
            }), function errorCallback(response) {
                unblockUI_Page();
            }
        };

        $scope.acceptOrder = function (id) {
            blockUI_Page();
            $http({
                method: 'POST',
                url: $scope.acceptReceivedOrderDetailsUrl,
                params: { "Id": id }
                //headers: { 'Content-Type': "application/json" },
                //dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.sendEmailNotificationOfAcceptOrder(id);
                    myNotification(response.data.message, 'Order Accept Operation', 'focus', 'la la-check');
                } else {
                    myNotification(response.data.message, 'Order Accept Operation', 'danger', 'la la-warning');
                }
                unblockUI_Page();
            }), function errorCallback(response) {
                unblockUI_Page();
            }
        };

        $scope.cancelOrder = function (id) {
            blockUI_Page();
            $http({
                method: 'POST',
                url: $scope.getCancelOrderUrl,
                params: { "Id": id }
                //headers: { 'Content-Type': "application/json" },
                //dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.sendEmailNotificationOfCancelOrder(id);
                    myNotification(response.data.message, 'Order Cancel Operation', 'focus', 'la la-check');
                } else {
                    myNotification(response.data.message, 'Order Cancel Operation', 'danger', 'la la-warning');
                }
                unblockUI_Page();
            }), function errorCallback(response) {
                unblockUI_Page();
            }
        };

        //..........................................Accept Order Email Notification...............................................
        $scope.sendEmailNotificationOfAcceptOrder = function (id) {
            $http({
                method: "GET",
                url: $scope.emailNotificationOfAcceptOrderUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End.......................................................

        //..........................................Cancel Order Email Notification...............................................
        $scope.sendEmailNotificationOfCancelOrder = function (id) {
            $http({
                method: "GET",
                url: $scope.emailNotificationOfCancelOrderUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End......................................................

        //..........................................Deny Order Email Notification...............................................
        $scope.sendEmailNotificationOfDenyOrder = function (id) {
            $http({
                method: "GET",
                url: $scope.emailNotificationOfDenyOrderUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End......................................................

        //....................................................... Get Order Info...............................................
        $scope.getConfirmedOrder = function () {
            $http({
                method: "GET",
                url: $scope.confirmOrderUrl,
                params: { "Id": $scope.orderId }
            }).then(function mySuccess(response) {
                $scope.orderDetails = response.data.value;
            });
        };
        //..............................................................End......................................................

        $scope.getConfirmedOrder();
    }]);