﻿
appModule.controller('orderMasterCtrl', ['$scope', '$rootScope', '$http', '$window', 'ApplicationConfiguration', 'dataTableConfiguration', function ($scope, $rootScope, $http, $window, ApplicationConfiguration, dataTableConfiguration) {
    $scope.path = '/Admin/Order/';
    $rootScope.formTitle = "Order";
    $rootScope.orderTab = "m-menu__item--active-tab";
    $scope.getPlacedOrderListUrl = $scope.path + 'GetReceivedOrderList';
    $scope.getPlacedOrderDetailsUrl = $scope.path + 'ReceivedDetails';
    $scope.getCancelOrderUrl = $scope.path + 'CancelOrder';
    $scope.denyReceivedOrderDetailsUrl = $scope.path + 'DenyReceivedOrder';
    $scope.acceptReceivedOrderDetailsUrl = $scope.path + 'AcceptReceivedOrder';
    $scope.emailNotificationOfAcceptOrderUrl = $scope.path + 'EmailNotificationOfAcceptOrder';
    $scope.emailNotificationOfCancelOrderUrl = $scope.path + 'EmailNotificationOfCancelOrder';
    $scope.emailNotificationOfDenyOrderUrl = $scope.path + 'EmailNotificationOfDenyOrder';

    $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

    $scope.detailsUrl = function (id) {
        $window.location.href = $scope.getPlacedOrderDetailsUrl + "/" + id;
    };

    $scope.cancelOrder = function (id) {
        blockUI_Page();
        $http({
            method: 'POST',
            url: $scope.getCancelOrderUrl,
            params: { "Id": id }
            //headers: { 'Content-Type': "application/json" },
            //dataType: 'JSON'
        }).then(function successCallback(response) {
            if (response.data.success == true) {
                $scope.sendEmailNotificationOfCancelOrder(id);
                myNotification(response.data.message, 'Order Cancel Operation', 'focus', 'la la-check');
            } else {
                myNotification(response.data.message, 'Order Cancel Operation', 'danger', 'la la-warning');
            }
            unblockUI_Page();
        }), function errorCallback(response) {
            unblockUI_Page();
        }
    };

    $scope.denyOrder = function (id) {
        blockUI_Page();
        $http({
            method: 'POST',
            url: $scope.denyReceivedOrderDetailsUrl,
            params: { "Id": id }
            //headers: { 'Content-Type': "application/json" },
            //dataType: 'JSON'
        }).then(function successCallback(response) {
            if (response.data.success == true) {
                $scope.sendEmailNotificationOfDenyOrder(id);
                myNotification(response.data.message, 'Order Deny Operation', 'focus', 'la la-close');
            } else {
                myNotification(response.data.message, 'Order Deny Operation', 'danger', 'la la-warning');
            }
            unblockUI_Page();
        }), function errorCallback(response) {
            unblockUI_Page();
        }
    };

    $scope.acceptOrder = function (id) {
        blockUI_Page();
        $http({
            method: 'POST',
            url: $scope.acceptReceivedOrderDetailsUrl,
            params: { "Id": id }
            //headers: { 'Content-Type': "application/json" },
            //dataType: 'JSON'
        }).then(function successCallback(response) {
            if (response.data.success == true) {
                $scope.sendEmailNotificationOfAcceptOrder(id);
                myNotification(response.data.message, 'Order Accept Operation', 'focus', 'la la-check');
            } else {
                myNotification(response.data.message, 'Order Accept Operation', 'danger', 'la la-warning');
            }
            unblockUI_Page();
        }), function errorCallback(response) {
            unblockUI_Page();
        }
    };

    //..........................................Accept Order Email Notification...............................................
    $scope.sendEmailNotificationOfAcceptOrder = function (id) {
        $http({
            method: "GET",
            url: $scope.emailNotificationOfAcceptOrderUrl,
            params: { "Id": id }
        }).then(function mySuccess(response) {
            $scope.orderDetails = response.data.value;
        });
    };
    //..............................................................End.......................................................

    //..........................................Cancel Order Email Notification...............................................
    $scope.sendEmailNotificationOfCancelOrder = function (id) {
        $http({
            method: "GET",
            url: $scope.emailNotificationOfCancelOrderUrl,
            params: { "Id": id }
        }).then(function mySuccess(response) {
            $scope.orderDetails = response.data.value;
        });
    };
    //..............................................................End......................................................

    //..........................................Deny Order Email Notification...............................................
    $scope.sendEmailNotificationOfDenyOrder = function (id) {
        $http({
            method: "GET",
            url: $scope.emailNotificationOfDenyOrderUrl,
            params: { "Id": id }
        }).then(function mySuccess(response) {
            $scope.orderDetails = response.data.value;
        });
    };
    //..............................................................End......................................................

    //.........................................................Data Table.........................................
    $scope.searchString = "";
    $rootScope.totalItemCount = 0;
    $rootScope.itemViewPerPage = 10;
    $rootScope.pageNo = 1;
    $rootScope.filterByStatus = "All";
    $scope.itemList = [];
    $scope.StatusList = [];

    $scope.getDataTableValue = function () {
        blockUI_Page();
        dataTableConfiguration.getValue($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString, $scope.getPlacedOrderListUrl, $rootScope.filterByStatus).then(function (response) {
            $scope.itemList = response.data.value;
            $scope.totalItemCount = response.data.totalRowsCount;
            unblockUI_Page();
        });
    };

    $scope.getDataTableValue();

    $scope.setViewPerPage = function (no) {
        $rootScope.itemViewPerPage = no;
    };

    $scope.getPageNo = function (pNo) {
        $rootScope.pageNo = pNo;
        $scope.getDataTableValue();
    };

    $scope.setSearchString = function () {
        $scope.getDataTableValue();
    };

    $scope.filterByStatus = function (status) {
        $rootScope.filterByStatus = status;
        $scope.getDataTableValue();
    };

    //..............................................................End..........................................       


}]);


