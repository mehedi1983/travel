﻿
appModule.controller("b2bDashboardCtrl", ['$scope', '$rootScope', '$window', 'ApplicationConfiguration', '$http', '$location', '$sce',
    function ($scope, $rootScope, $window, ApplicationConfiguration, $http, $location, $sce) {
        $scope.path = '/B2B/Dashboard/';
        $scope.b2BDashboardInfoUrl = $scope.path + 'LoadB2BDashboardInfo';
        $rootScope.formTitle = "Dashboard";
        $rootScope.dashboardTab = "m-menu__item--active-tab";
        $scope.changeProfileUrl = '/B2B/Profile';
        $scope.bookingDetailsUrl = '/B2B/Booking/Details/';

        $scope.B2BDashboardInfo = [];
        $scope.RecentMenuList = [];
        $scope.itemModel = {
            ReceiverEmail: null
        };

        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

        $scope.changeProfile = function () {
            $window.location.href = $scope.changeProfileUrl;
        };

        $scope.bookingDetails = function (id) {
            $window.location.href = $scope.bookingDetailsUrl + id;
        };

        //.......................................................B2B Dashboard Info...............................................

        $scope.showDashboardInfo = function () {
            $http({
                method: "GET",
                url: $scope.b2BDashboardInfoUrl,
                params: { }
            }).then(function mySuccess(response) {
                $scope.B2BDashboardInfo = response.data.value;

                $rootScope.formTitle = $scope.B2BDashboardInfo.b2BInformationModel.name + "'s Dashboard";

                if ($scope.B2BDashboardInfo.b2BInformationModel.logo != null && $scope.B2BDashboardInfo.b2BInformationModel.logo != "")
                    $scope.B2BDashboardInfo.b2BInformationModel.logo = "/ApplicationImage/CompanyLogo/" + $scope.B2BDashboardInfo.b2BInformationModel.logo;
                else
                    $scope.B2BDashboardInfo.chefInformationModel.logo = "../images/avatar.jpg";
            });
        };
                
        //..............................................................End........................................................

        $scope.showDashboardInfo();
       
    }]);
