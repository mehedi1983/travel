﻿
appModule.factory('ApplicationConfiguration', ['$http', function ($http) {
    var configurationService = {};

    configurationService.baseCurrency = function() {
        return "Tk";
    };

    configurationService.addToCartSessionStorageName = function() {
        return "MenuToCart";
    };

    configurationService.currentDate = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = mm +
            '/' +
            dd +
            '/' +
            yyyy +
            ' ' +
            today.getHours() +
            ":" +
            today.getMinutes() +
            ":" +
            today.getSeconds();
        return today;
    }

    configurationService.getStatus = function () {
        return $http.get('/Admin/Configuration/GetStatus');
    }

    configurationService.currentDate_DD_MM_YYYY_FORMAT = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd +
            '/' +
            mm +
            '/' +
            yyyy;
        return today;
    }

    configurationService.convertDateFormat_DDMMYYYY_To_MMDDYYYY = function (cDate) {
        var tempVal = cDate.split("/");
        var d = tempVal[0];
        var m = tempVal[1];
        var y = tempVal[2];
        return m + "/" + d + "/" + y;
    }

    configurationService.convertDateFormat_YYYYMMDD_To_DDMMYYYY = function (cDate) {
        var tempVal = cDate.split("-");
        var y = tempVal[0];
        var m = tempVal[1];
        var d = tempVal[2];
        return d + "/" + m + "/" + y;
    }

    configurationService.getTime12HourFormat = function (cDate) {
        return (cDate.getHours() >= 13 ? (cDate.getHours() - 12) : (cDate.getHours())) + ":" + (cDate.getMinutes() < 10 ? '0' : '') + cDate.getMinutes() + (cDate.getHours() > 11 ? ' PM' : ' AM');        
    }

    configurationService.currentTime = function () {
        var cDate = new Date().toLocaleTimeString();
        return cDate;
    }

    return configurationService;

}]);