﻿
appModule.factory('dataTableConfiguration', ['$http', function ($http) {

    var configurationService = {};

    configurationService.getValue = function (pageSize, pageNo, searchStr, searchUrl, filterByStatus) {
        return $http.get(searchUrl + "?PageSize=" + pageSize + "&PageNo=" + pageNo + "&SearchString=" + searchStr + "&FilterByStatus=" + filterByStatus);
    };

    configurationService.getValueById = function (pageSize, pageNo, searchStr, searchUrl, filterByStatus, menuTypeId) {
        return $http.get(searchUrl + "?PageSize=" + pageSize + "&PageNo=" + pageNo + "&SearchString=" + searchStr + "&FilterByStatus=" + filterByStatus + "&MenuTypeId=" + menuTypeId);
    };

    configurationService.getValueByMenuId = function (pageSize, pageNo, searchStr, searchUrl, filterByStatus, menuId) {
        return $http.get(searchUrl + "?PageSize=" + pageSize + "&PageNo=" + pageNo + "&SearchString=" + searchStr + "&FilterByStatus=" + filterByStatus + "&MenuId=" + menuId);
    };

    configurationService.getValueByChefId = function (pageSize, pageNo, searchStr, searchUrl, filterByStatus, chefId) {
        return $http.get(searchUrl + "?PageSize=" + pageSize + "&PageNo=" + pageNo + "&SearchString=" + searchStr + "&FilterByStatus=" + filterByStatus + "&ChefId=" + chefId);
    };

    return configurationService;

}]);