﻿appModule.controller('resetPasswordCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Reset Password";
        $scope.path = '/Account/';
        $scope.resetPasswordUrl = $scope.path + 'ResetPassword';

        $scope.resetPasswordModel = {
            Email: null
            , Token: null
            , NewPassword: null
            , ConfirmPassword: null
        };

        $scope.back = function () {
            $window.location.href = "/Home/";
        };

        //................Reset password form.......................
        $scope.resetPassword = function() {
            if ($scope.reset_password_form.$valid) {
                blockUI_Page();
                $scope.resetPasswordModel.Email = $("#Email").val();
                $scope.resetPasswordModel.Token = $("#Token").val();
                $http({
                    method: 'POST',
                    url: $scope.resetPasswordUrl,
                    params: $scope.resetPasswordModel,
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        myNotification(response.data.message, 'Reset Password', 'focus', 'la la-key');
                    } else {
                        myNotification(response.data.message, 'Reset Password', 'danger', 'la la-key');
                    }
                    unblockUI_Page();
                }), function errorCallback(response) {
                    unblockUI_Page();
                };
            }
        };
        //........................End Update..................................    

    }]);