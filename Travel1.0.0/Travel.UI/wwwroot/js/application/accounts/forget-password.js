﻿appModule.controller('forgetPasswordCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Forget Password";
        $scope.path = '/Account/';
        $scope.forgetPasswordUrl = $scope.path + 'ForgotPassword';

        $scope.changePasswordModel = {
            CurrentPassword: null
            , NewPassword: null
            , ConfirmPassword: null
        };

        $scope.back = function () {
            $window.location.href = "/Home/";
        };

        //................Change password form.......................
        $scope.forgetPassword = function () {
            if ($scope.forget_password_form.$valid) {
                blockUI_Page();
                $http({
                    method: 'POST',
                    url: $scope.forgetPasswordUrl,
                    params: { "EmailAddress": $scope.Email},
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    $("#lblMsg").empty();
                    if (response.data.success == true) {
                        //$("#lblMsg").append(response.data.message);
                        //document.location.href = response.data.redirectUrl;
                        myNotification(response.data.message, 'Forget Password', 'focus', 'la la-key');
                    } else {
                        $("#lblMsg").append(response.data.message);
                        myNotification(response.data.message, 'Forget Password', 'danger', 'la la-key');
                    }
                    unblockUI_Page();
                }), function errorCallback(response) {
                    unblockUI_Page();
                }
            }           
        }
        //........................End Update..................................    
        
    }]);