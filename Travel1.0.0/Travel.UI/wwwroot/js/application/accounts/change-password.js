﻿appModule.controller('changePasswordCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "Change Password Management";
        $rootScope.dashboardTab = "m-menu__item--active-tab";
        $scope.path = '/Account/';
        $scope.changePasswordUrl = $scope.path + 'ChangePassword';

        $scope.changePasswordModel = {
            CurrentPassword: null
            , NewPassword: null
            , ConfirmPassword: null
        };

        $scope.customerBack = function () {
            $window.location.href = "/Customer/CustomerDashboard";
        };

        $scope.chefBack = function () {
            $window.location.href = "/Chef/Dashboard";
        };

        $scope.adminBack = function () {
            $window.location.href = "/Admin/AdminDashboard";
        };

        $scope.insertUrl = function () {
            $window.location.href = $scope.insertUpdateUrl;
        };
        
        //................Change password form.......................
        $scope.changePassword = function () {
            if ($scope.change_password_form.$valid) {
                blockUI_Page();
                $http({
                    method: 'POST',
                    url: $scope.changePasswordUrl,
                    params: $scope.changePasswordModel,
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        myNotification(response.data.message, 'Password Change', 'focus', 'la la-key');
                    } else {
                        myNotification(response.data.message, 'Password Change', 'danger', 'la la-key');
                    }
                    unblockUI_Page();
                }), function errorCallback(response) {
                    unblockUI_Page();
                }
            }           
        }
        //........................End Update..................................    
        
    }]);