﻿
function decimal_with_two_digit() {
    $(".twodigitdecimalnumber").format({ precision: 2, allow_negative: false, autofix: true });
}

function CurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = mm + '/' + dd + '/' + yyyy + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return today;
}

var initVendorDataTable = function () {
    var datatable = $('.vendor_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#getVendorListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'name',
                title: 'Name',
                width: 220
            },
            {
                field: 'createdDateView',
                title: 'Created Date',
                width: 120,
            },
            {
                field: 'status',
                title: 'Status',
                sortable: false,
                width: 70,
                template: function (row) {
                    var status = {
                        'Active': { 'title': 'Active', 'class': ' m-badge--success' },
                        'Inactive': { 'title': 'Inactive', 'class': ' m-badge--danger' },
                    };
                    return '<span class="m-badge ' +
                        status[row.status].class +
                        ' m-badge--wide">' +
                        status[row.status].title +
                        '</span>';
                }
            },
            {
                field: 'Actions',
                title: 'Action',
                width: 50,
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return "<a href=\"javascript: loadModalInEditMode('" +
                        row.id + "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Edit Item\"><i class=\"la la-edit\"></i></a>";
                }
            }
        ]
    });

    $('#m_form_status').on('change',
        function () {
            datatable.search($(this).val(), 'Status');
        });

};

function loadModalInInsertMode() {
    $("#Id").val("");
    $("#Name").val("");
    $("select#Status").val('Active');

    $("#vendor_information").text("New Vendor");
    $("#btnSave").text("Save");

    $("#m_maxlength_modal").modal();
}

function loadModalInEditMode(id) {
    $("#vendor_information").text("Edit Vendor");
    $("#btnSave").text("Update");

    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getVendorUrl").val(),
        data: { "Id": id },
        dataType: "html",
        success: function (result) {
            var parseData = JSON.parse(result);
            var receivedValue = parseData.value;

            $("#Id").val(receivedValue.id);
            $("#CompanyId").val(receivedValue.companyId);
            $("#Name").val(receivedValue.name);
            $("select#Status").val(receivedValue.status);
            $("#m_maxlength_modal").modal();
        },
        error: function (error) {
            var msg = 'Fail to load vendor.';
            alert(msg);
        }
    });
}

function formValidation() {
    $("#btnSave").on("click",
        function () {
            if ($("#save_information")[0].checkValidity()) {
                saveData();
            } else {
                $("#save_information")[0].reportValidity();
            }
        });
}

function saveData() {
    blockUI_Modal();

    blockUI_Modal();
    var model = {
        Id: $("#Id").val(),
        CompanyId: $("#CompanyId").val(),
        Name: $("#Name").val(),
        Status: $("#Status :selected").val(),
        CreatedDate: CurrentDate(),
        ModifiedDate: CurrentDate()
    };

    var val = JSON.stringify(model);

    $.ajax({
        type: "POST",
        url: $("#insertUpdateUrl").val(),
        data: val,
        cache: false,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $("#m_maxlength_modal .close").click();

            $("#Id").val("");
            $("#Name").val("");
            $("select#Status").val('Active');

            unblockUI_Modal();
            $('.vendor_datatable').mDatatable().search('', 'Status');
        },
        failure: function (response) {
            unblockUI_Modal();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Modal();
        }
    });

}