﻿
var initEmployeeDatatable = function () {
    var datatable = $('.employee_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#employeeListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'id',
                title: 'Id',
                sortable: 'asc', // default sort
                filterable: false, // disable or enable filtering
                width: 80
            },
            {
                field: 'name',
                title: 'Name',
                width: 300
            },
            {
                field: 'emailAddress',
                title: 'Email Address',
                width: 210
            },
            {
                field: 'mobileNo',
                title: 'Mobile No',
                width: 100
            },
            {
                field: 'createdViewDate',
                title: 'Created Date',
                type: 'date',
                width: 100
            },
            {
                field: 'status',
                title: 'Status',
                //sortable: false,
                template: function (row) {
                    var status = {
                        'Active': { 'title': 'Active', 'class': ' m-badge--success' },
                        'Inactive': { 'title': 'Inactive', 'class': ' m-badge--danger' },
                    };
                    return '<span class="m-badge ' +
                        status[row.status].class +
                        ' m-badge--wide">' +
                        status[row.status].title +
                        '</span>';
                },
                width: 80
            },
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return "<a href=\"Employee/Save/" +
                        row.id +
                        "\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Edit details\"><i class=\"la la-edit\"></i></a>";
                    //" <a onclick=\"javascript:if (confirm('Are you sure you want to delete?') == false) return false; Delete('" +
                    //row.id +
                    //"');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete details\"><i class=\"la la-trash\"></i></a>";
                },
                width: 80
            }
        ]
    });

    $('#m_form_status').on('change',
        function () {
            datatable.search($(this).val(), 'Status');
        });
};

function Delete(id) {
    blockUI_Page();
    var fd = new FormData();
    fd.append("Id", id);
    $.ajax({
        type: "DELETE",
        url: $("#employeeDeleteUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.employee_datatable').mDatatable().search('', 'Status');
            if (data.success)
                alert(data.message);
            else
                alert(data.message);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus) {
        }
    });
    unblockUI_Page();
}

function loadEmployeeInformation(id) {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#employeeLoadInformationUrl").val(),
        data: { "Id": id },
        dataType: "html",
        success: function (result) {
            var parseData = JSON.parse(result);
            var employee = parseData.value;
            $("#Id").val(employee.id);
            $("#Name").val(employee.name);
            $("#EmailAddress").val(employee.emailAddress);
            $("#MobileNo").val(employee.mobileNo);
            $("#Address").val(employee.address);
            $("#CityName").val(employee.cityName);
            $("#ZipCode").val(employee.zipCode);
            $("#CountryName").val(employee.countryName);
            $("select#Status").val(employee.status);

            if (employee.picture != null && employee.picture != "")
                $("#imgProfilePicture").attr("src", "/ApplicationImage/Employee/" + employee.picture);
        },
        error: function (error) {
            var msg = 'Fail to load employee information.';
            alert(msg);
        }
    });
    unblockUI_Page();
}

function uploadImage() {
    var imgFiles = $("#imgUploadImage").get(0).files;
    if (imgFiles.length == '0') {
        alert("Please select a image file.");
        $("#imgUploadImage").focus();
    } else {
        blockUI_Upload_Modal();
        var fd = new FormData();
        fd.append("uploadImage", imgFiles[0]);
        $.ajax({
            type: "POST",
            url: $("#employeeUploadLogoUrl").val(),
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success == true) {
                    $("#imgImage").attr("src", "/temp/" + data.fileName);
                    $("#imgProfilePicture").attr("src", "/temp/" + data.fileName);
                    $("#Picture").val(data.fileName);
                }
                unblockUI_Upload_Modal();
            },
            failure: function (response) {
                unblockUI_Upload_Modal();
            },
            error: function (XMLHttpRequest, textStatus) {
                unblockUI_Upload_Modal();
            }
        });
    }
}

function formValidation() {
    $("#btnSaveUpdate").on("click",
        function () {
            if ($("#save_information")[0].checkValidity()) {
                saveData();
            } else {
                $("#save_information")[0].reportValidity();
            }
        });
}

function saveData() {
    blockUI_Page();

    var model = {
        Id: $("#Id").val(),
        CompanyId: $("#CompanyId").val(),
        Name: $("#Name").val(),
        EmailAddress: $("#EmailAddress").val(),
        MobileNo: $("#MobileNo").val(),
        Address: $("#Address").val(),
        CityName: $("#CityName").val(),
        ZipCode: $("#ZipCode").val(),
        CountryName: $("#CountryName").val(),
        Picture: $("#Picture").val(),
        Status: $("#Status :selected").val(),
        CreatedDate: CurrentDate(),
        ModifiedDate: CurrentDate()
    };

    var val = JSON.stringify(model);

    $.ajax({
        type: "POST",
        url: $("#employeeInsertUpdateUrl").val(),
        data: val,
        cache: false,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            //$("#Id").val("");
            //$("#Name").val("");
            //$("#EmailAddress").val("");
            //$("#MobileNo").val("");
            //$("#Address").val("");
            //$("#CityName").val("");
            //$("#ZipCode").val("");
            //$("#CountryName").val("");
            //$("#Picture").val("");
            //$("select#Status").val('Active');
            //$("#imgProfilePicture").attr("src", "/images/avatar.jpg");
            window.location.href = data.redirectUrl;
            unblockUI_Page();
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}

function loadModalInInsertMode() {
    $("#imgProfilePicture").val("");
    $("#imgImage").attr("src", "");
    $("#imgUploadImage").val('');
    $("#upload_image_modal").modal();
}

function removeImage() {
    if (confirm('Are you sure you want to delete?') == true) {
        $("#imgProfilePicture").attr("src", "/images/avatar.jpg");
        $("#Picture").val("");
    }
}

function CurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = mm + '/' + dd + '/' + yyyy + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return today;
}