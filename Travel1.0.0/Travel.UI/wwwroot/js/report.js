﻿
function loadCustomer() {
    $("#CustomerId").empty();
    var options = $("#CustomerId");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#loadCustomerUrl").val(),
        data: {  },
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t.value,
                function () {
                    options.append($("<option />").val(this.id).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load customer list.';
            alert(msg);
        }
    });
}

function initInvoiceDetailsTable() {
    var table = $('#invoice_details_data_table');
    table.DataTable({
        responsive: true,
        paging: false,
        filter: false,
        info: false,
        sorting: false,
        columnDefs: [
            {
                targets: 0,
                width: 250
            }
        ]
    });
}

function getClientBasedPaymentStatus() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getClientBasedPaymentStatusUrl").val(),
        data: { "CustomerId": $("select#CustomerId").val(), "StartDate": rDate($('#DateRange').data('daterangepicker').startDate), "EndDate": rDate($('#DateRange').data('daterangepicker').endDate) },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;
            
            if (val.success) {
                
                $("#add_data_table_row").empty();

                var TotalAvInvoice = 0;
                var NoOfInvoices = 0;

                var Total = 0;
                var Received = 0;
                var Outstanding = 0;

                var tableRows = "";

                tableRows = tableRows +
                    "<table class=\"table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline\"" +
                    "    id=\"invoice_details_data_table\" role=\"grid\" aria-describedby=\"m_table_1_info\" style=\"width: 979px;\">" +
                    "    <thead>" +
                    "        <tr role=\"row\">" +
                    "            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 221.25px!important;\" aria-sort=\"ascending\" aria-label=\"Agent: activate to sort column descending\">Customer</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 43.25px; text-align: right;\" aria-label=\"Company Email: activate to sort column ascending\"># Invoices</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 68.25px; text-align: right;\" aria-label=\"Company Agent: activate to sort column ascending\">Av. Invoice</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 133.25px; text-align: right;\" aria-label=\"Company Name: activate to sort column ascending\">Total</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 54.25px; text-align: right;\" aria-label=\"Status: activate to sort column ascending\">Received</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 110.25px; text-align: right;\" aria-label=\"Type: activate to sort column ascending\">Outstanding</th>" +
                    "        </tr>" +
                    "    </thead>" +
                    "    <tbody>";

                $.each(val.value.result,
                    function () {
                        tableRows = tableRows + "<tr role=\"row\" class=\"even\"> <td>" + this.customerName + "</td>" +
                            "<td style=\" text-align: right;\">" + this.invoices + "</td> <td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.avInvoices).format('0,0.00') + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.totalAmount).format('0,0.00') + "</td><td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.receivedAmount).format('0,0.00') + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.outstanding).format('0,0.00') + "</td></tr >";

                        NoOfInvoices = NoOfInvoices + parseInt(this.invoices);
                        TotalAvInvoice = TotalAvInvoice + parseInt(this.avInvoices);
                        Total = Total + parseInt(this.totalAmount);
                        Received = Received + parseInt(this.receivedAmount);
                        Outstanding = Outstanding + parseInt(this.outstanding);

                    });

                tableRows = tableRows + "<tr role=\"row\" class=\"even\" style=\"border: 1px solid #CCE7FF; background: #FAFDFF;\"> <td><b>Totals</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + NoOfInvoices +"</b></td> <td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(TotalAvInvoice).format('0,0.00') + "</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Total).format('0,0.00') + "</b></td><td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Received).format('0,0.00') + "</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Outstanding).format('0,0.00') + "</b></td></tr >";

                tableRows = tableRows + "</tbody></table>";
                $("#add_data_table_row").append(tableRows);

                //$("#InvoiceSubTotal").val(Total);

                initInvoiceDetailsTable();






                unblockUI_Page();
            } else {
                alert("fail");
                unblockUI_Page();
            }
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}

function rDate(cDate) {

    var today = new Date(cDate);

    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = mm + '/' + dd + '/' + yyyy + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    return today;
}

function getPaymentStatus() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getClientBasedPaymentStatusUrl").val(),
        data: { "CustomerId": $("select#CustomerId").val(), "StartDate": rDate($('#DateRange').data('daterangepicker').startDate), "EndDate": rDate($('#DateRange').data('daterangepicker').endDate) },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;

            if (val.success) {

                $("#add_data_table_row").empty();

                var TotalAvInvoice = 0;
                var NoOfInvoices = 0;

                var Total = 0;
                var Received = 0;
                var Outstanding = 0;

                var tableRows = "";

                tableRows = tableRows +
                    "<table class=\"table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline\"" +
                    "    id=\"invoice_details_data_table\" role=\"grid\" aria-describedby=\"m_table_1_info\" style=\"width: 979px;\">" +
                    "    <thead>" +
                    "        <tr role=\"row\">" +
                    "            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 221.25px!important;\" aria-sort=\"ascending\" aria-label=\"Agent: activate to sort column descending\">Client</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 43.25px; text-align: right;\" aria-label=\"Company Email: activate to sort column ascending\"># Invoices</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 68.25px; text-align: right;\" aria-label=\"Company Agent: activate to sort column ascending\">Av. Invoice</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 133.25px; text-align: right;\" aria-label=\"Company Name: activate to sort column ascending\">Total</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 54.25px; text-align: right;\" aria-label=\"Status: activate to sort column ascending\">Received</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 110.25px; text-align: right;\" aria-label=\"Type: activate to sort column ascending\">Outstanding</th>" +
                    "        </tr>" +
                    "    </thead>" +
                    "    <tbody>";

                $.each(val.value.result,
                    function () {
                        tableRows = tableRows + "<tr role=\"row\" class=\"even\"> <td>" + this.customerName + "</td>" +
                            "<td style=\" text-align: right;\">" + this.invoices + "</td> <td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.avInvoices).format('0,0.00') + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.totalAmount).format('0,0.00') + "</td><td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.receivedAmount).format('0,0.00') + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.outstanding).format('0,0.00') + "</td></tr >";

                        NoOfInvoices = NoOfInvoices + parseInt(this.invoices);
                        TotalAvInvoice = TotalAvInvoice + parseInt(this.avInvoices);
                        Total = Total + parseInt(this.totalAmount);
                        Received = Received + parseInt(this.receivedAmount);
                        Outstanding = Outstanding + parseInt(this.outstanding);

                    });

                tableRows = tableRows + "<tr role=\"row\" class=\"even\" style=\"border: 1px solid #CCE7FF; background: #FAFDFF;\"> <td><b>Totals</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + NoOfInvoices + "</b></td> <td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(TotalAvInvoice).format('0,0.00') + "</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Total).format('0,0.00') + "</b></td><td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Received).format('0,0.00') + "</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Outstanding).format('0,0.00') + "</b></td></tr >";

                tableRows = tableRows + "</tbody></table>";
                $("#add_data_table_row").append(tableRows);

                initInvoiceDetailsTable();

                unblockUI_Page();
            } else {
                alert("fail");
                unblockUI_Page();
            }
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}

function showReport() {
    if ($("select#GenerateReportFor").val() == "Received Payments") {
        getReceivedPaymentStatus();
    } else {
        getClientOutstandingPaymentStatus();
    }
}

function getReceivedPaymentStatus() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getClientReceivedPaymentStatusUrl").val(),
        data: { "CustomerId": $("select#CustomerId").val(), "StartDate": rDate($('#DateRange').data('daterangepicker').startDate), "EndDate": rDate($('#DateRange').data('daterangepicker').endDate) },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;

            if (val.success) {

                $("#add_data_table_row").empty();

                var Received = 0;

                var tableRows = "";

                tableRows = tableRows +
                    "<table class=\"table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline\"" +
                    "    id=\"invoice_details_data_table\" role=\"grid\" aria-describedby=\"m_table_1_info\" style=\"width: 979px;\">" +
                    "    <thead>" +
                    "        <tr role=\"row\">" +
                    "            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 221.25px!important;\" aria-sort=\"ascending\" aria-label=\"Agent: activate to sort column descending\">Invoice</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 43.25px; text-align: right;\" aria-label=\"Company Email: activate to sort column ascending\">Invoice Total</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 68.25px; text-align: right;\" aria-label=\"Company Agent: activate to sort column ascending\">Type</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 133.25px; text-align: right;\" aria-label=\"Company Name: activate to sort column ascending\">Date</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 54.25px; text-align: right;\" aria-label=\"Status: activate to sort column ascending\">Received Amount</th>" +
                    "        </tr>" +
                    "    </thead>" +
                    "    <tbody>";

                $.each(val.value.result,
                    function () {
                        tableRows = tableRows + "<tr role=\"row\" class=\"even\"> <td>" + this.invoiceNo + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.totalAmount).format('0,0.00') + "</td> <td style=\" text-align: right;\">" + this.type + "</td>" +
                            "<td style=\" text-align: right;\">" + this.receivedDateView + "</td><td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.receivedAmount).format('0,0.00') + "</td>" +
                            "</tr >";

                        Received = Received + parseInt(this.receivedAmount);

                    });

                tableRows = tableRows + "<tr role=\"row\" class=\"even\" style=\"border: 1px solid #CCE7FF; background: #FAFDFF;\"> <td><b>Totals</b></td>" +
                    "<td style=\" text-align: right;\"></td> <td style=\" text-align: right;\"></td>" +
                    "<td style=\" text-align: right;\"></td><td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Received).format('0,0.00') + "</b></td>" ;

                tableRows = tableRows + "</tbody></table>";
                $("#add_data_table_row").append(tableRows);

                initInvoiceDetailsTable();

                unblockUI_Page();
            } else {
                alert("fail");
                unblockUI_Page();
            }
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}

function getClientOutstandingPaymentStatus() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getClientOutstandingPaymentStatusUrl").val(),
        data: { "CustomerId": $("select#CustomerId").val(), "StartDate": rDate($('#DateRange').data('daterangepicker').startDate), "EndDate": rDate($('#DateRange').data('daterangepicker').endDate) },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;

            if (val.success) {

                $("#add_data_table_row").empty();

                var Outstandind = 0;

                var tableRows = "";

                tableRows = tableRows +
                    "<table class=\"table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline\"" +
                    "    id=\"invoice_details_data_table\" role=\"grid\" aria-describedby=\"m_table_1_info\" style=\"width: 979px;\">" +
                    "    <thead>" +
                    "        <tr role=\"row\">" +
                    "            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 221.25px!important;\" aria-sort=\"ascending\" aria-label=\"Agent: activate to sort column descending\">Invoice</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 43.25px; text-align: right;\" aria-label=\"Company Email: activate to sort column ascending\">Invoice Total</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 68.25px; text-align: right;\" aria-label=\"Company Agent: activate to sort column ascending\">Date</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 133.25px; text-align: right;\" aria-label=\"Company Name: activate to sort column ascending\">Outstanding Amount</th>" +

                    //"            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 54.25px; text-align: right;\" aria-label=\"Status: activate to sort column ascending\">Received Amount</th>" +

                    "        </tr>" +
                    "    </thead>" +
                    "    <tbody>";

                $.each(val.value.result,
                    function () {
                        tableRows = tableRows + "<tr role=\"row\" class=\"even\"> <td>" + this.invoiceNo + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.totalAmount).format('0,0.00') + "</td> <td style=\" text-align: right;\">" + this.invoiceDateView + "</td>" +
                            "<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.outstandindAmount).format('0,0.00') + "</td>" +

                            //"<td style=\" text-align: right;\">" + val.baseCurrency.result + " " + numeral(this.receivedAmount).format('0,0.00') + "</td>" +

                            "</tr >";

                        Outstandind = Outstandind + parseInt(this.outstandindAmount);

                    });

                tableRows = tableRows + "<tr role=\"row\" class=\"even\" style=\"border: 1px solid #CCE7FF; background: #FAFDFF;\"> <td><b>Totals</b></td>" +
                    "<td style=\" text-align: right;\"></td>" +
                    "<td style=\" text-align: right;\"></td><td style=\" text-align: right;\"><b>" + val.baseCurrency.result + " " + numeral(Outstandind).format('0,0.00') + "</b></td>";

                tableRows = tableRows + "</tbody></table>";
                $("#add_data_table_row").append(tableRows);

                initInvoiceDetailsTable();

                unblockUI_Page();
            } else {
                alert("fail");
                unblockUI_Page();
            }
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}


function loadVendor() {
    $("#Vendor").empty();
    var options = $("#Vendor");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#loadVendorUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t.value,
                function () {
                    options.append($("<option />").val(this.name).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load vendor list.';
            alert(msg);
        }
    });
}

function loadExpensesCategory() {
    $("#Cateogry").empty();
    var options = $("#Cateogry");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#loadExpensesCategoryUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t.value,
                function () {
                    options.append($("<option />").val(this.name).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load category list.';
            alert(msg);
        }
    });
}

function getExpensesStatus() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getExpensesStatusUrl").val(),
        data: {
            "StartDate": rDate($('#DateRange').data('daterangepicker').startDate),
            "EndDate": rDate($('#DateRange').data('daterangepicker').endDate),
            "Vendor": $("select#Vendor").val(),
            "ExpensesCateogry": $("select#Cateogry").val()
        },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;

            if (val.success) {

                $("#add_data_table_row").empty();
                
                var NoOfExpenses = 0;
                var Total = 0;

                var tableRows = "";
                var currency = "";

                tableRows = tableRows +
                    "<table class=\"table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline\"" +
                    "    id=\"invoice_details_data_table\" role=\"grid\" aria-describedby=\"m_table_1_info\" style=\"width: 979px;\">" +
                    "    <thead>" +
                    "        <tr role=\"row\">" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 43.25px; text-align: right;\" aria-label=\"Company Email: activate to sort column ascending\">Vendor</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 68.25px; text-align: right;\" aria-label=\"Company Agent: activate to sort column ascending\">Category</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 133.25px; text-align: right;\" aria-label=\"Company Name: activate to sort column ascending\">Name</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 54.25px; text-align: right;\" aria-label=\"Status: activate to sort column ascending\">Date</th>" +
                    "            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"m_table_1\" rowspan=\"1\" colspan=\"1\" style=\"width: 110.25px; text-align: right;\" aria-label=\"Type: activate to sort column ascending\">Amount</th>" +
                    "        </tr>" +
                    "    </thead>" +
                    "    <tbody>";

                $.each(val.value.result,
                    function () {
                        tableRows = tableRows + "<tr role=\"row\" class=\"even\"> <td>" + this.vendor + "</td>" +
                            "<td style=\" text-align: right;\">" + this.category + "</td> <td style=\" text-align: right;\">" + this.name + "</td>" +
                            "<td style=\" text-align: right;\">" + this.expensesDateView + "</td><td style=\" text-align: right;\">" + this.currency + " "+ numeral(this.amount).format('0,0.00') + "</td>" +
                            "</tr >";
                        currency = this.currency;
                        NoOfExpenses = NoOfExpenses + 1;
                        Total = Total + parseInt(this.amount);
                    });

                tableRows = tableRows + "<tr role=\"row\" class=\"even\" style=\"border: 1px solid #CCE7FF; background: #FAFDFF;\"> <td><b>Totals</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + "</b></td> <td style=\" text-align: right;\"><b>" + "</b></td>" +
                    "<td style=\" text-align: right;\"><b>" + "</b></td><td style=\" text-align: right;\"><b>" +currency + " "+ numeral(Total).format('0,0.00') + "</b></td>" +
                    "</tr >";

                tableRows = tableRows + "</tbody></table>";
                $("#add_data_table_row").append(tableRows);

                initInvoiceDetailsTable();

                unblockUI_Page();
            } else {
                alert("fail");
                unblockUI_Page();
            }
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}
