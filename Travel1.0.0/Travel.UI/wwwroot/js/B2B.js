﻿
function loadModalInInsertMode() {
    $("#imgProfilePicture").val("");
    $("#imgImage").attr("src", "");
    $("#imgUploadImage").val('');
    $("#upload_image_modal").modal();
}

appModule.controller('b2bBasicInfoCtrl', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration', 'dataTableConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration, dataTableConfiguration) {
        $rootScope.formTitle = "";
        $rootScope.dashboardTab = "m-menu__item--active-tab";
        $scope.path = '/B2B/Profile/';
        $scope.getB2BBasicInfoUrl = $scope.path + 'LoadB2BInformationWithCityAndLocationList';
        $scope.updateB2BBasicInfoUrl = $scope.path + 'ChangeBasicInformation';
        $scope.loadCityUrl = $scope.path + 'LoadCity';
        $scope.uploadProfilePictureUrl = $scope.path + 'UploadImage';
        $scope.deleteProfilePictureUrl = $scope.path + 'DeleteLogo';
        $scope.backUrl = '/B2B/Dashboard';

        $scope.b2bBasicInfo = {
            Id: $("#TempInput").val()
            , Name: null
            , Address: null
            , CountryId: null
            , CityId: null
            , ZipCode: null
            , ContactPerson: null
            , ContactPersonDesignation: null
            , Email: null
            , PhoneNumber: null
            , MobileNumber: null
            , Fax: null
            , Website: null
            , NatureOfBusiness: null
            , LicenseNo: null
            , Status: null
            , CreatedDate: ApplicationConfiguration.currentDate()
        };
        
        $scope.getB2BBasicInfo = function () {
            $http({
                method: "GET",
                url: $scope.getB2BBasicInfoUrl,
                params: { "Id": $scope.b2bBasicInfo.Id }
            }).then(function mySuccess(response) {
                
                //...................Pulling b2b basic information..................
                var value = response.data.value;
                $rootScope.formTitle = value.name +"'s Profile";
                $scope.b2bBasicInfo.Name = value.name;
                $scope.b2bBasicInfo.Address = value.address;
                if (value.countryId != null) {
                    $scope.SelectedCountry = { id: value.countryId, name: value.countryName };
                }

                if (value.cityId != null) {
                    $scope.SelectedCity = { id: value.cityId, name: value.cityName };
                }
                $scope.b2bBasicInfo.ZipCode = value.zipCode;
                $scope.b2bBasicInfo.ContactPerson = value.contactPerson;
                $scope.b2bBasicInfo.ContactPersonDesignation = value.contactPersonDesignation;
                $scope.b2bBasicInfo.Email = value.email;
                $scope.b2bBasicInfo.PhoneNumber = value.phoneNumber;
                $scope.b2bBasicInfo.MobileNumber = value.mobileNumber;
                $scope.b2bBasicInfo.Fax = value.fax;
                $scope.b2bBasicInfo.Website = value.website;
                $scope.b2bBasicInfo.NatureOfBusiness = value.natureOfBusiness;
                $scope.b2bBasicInfo.LicenseNo = value.licenseNo;
                $scope.b2bBasicInfo.Status = value.status;
                
                $scope.CountryList = value.countrySummaryModels;
                $scope.CityList = value.citySummaryModels;
                
                if (value.logo != null && value.logo != "")
                    $scope.imgProfilePicture = "/ApplicationImage/CompanyLogo/" + value.logo;
                else
                    $scope.imgProfilePicture = "../images/avatar.jpg";
                //..........................End pulling...............................                

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
        }

        //.....................................................Update B2B Basic Information.......................
        $scope.updateB2BInformation = function () {
            if ($scope.b2bBasicInformationForm.$valid) {
                blockUI_Page_Saving_Info();
                $scope.b2bBasicInfo.CountryId = $scope.SelectedCountry.id;
                $scope.b2bBasicInfo.CityId = $scope.SelectedCity.id;

                $http({
                    method: "POST",
                    url: $scope.updateB2BBasicInfoUrl,
                    params: $scope.b2bBasicInfo
                    //{
                    //    "Id": $scope.B2BId, "Name": $scope.Name, "Email": $scope.Email, "PhoneNumber": $scope.PhoneNumber,
                    //    "MobileNumber": $scope.MobileNumber, "Address": $scope.Address, "LocationId": $scope.SelectedLocation.id, "ZipCode": $scope.ZipCode
                    //}
                }).then(function mySuccess(response) {
                    unblockUI_Page_Loading_Info();
                    myNotification(response.data.message, 'Update Successfully', 'focus', 'la la-plus');
                }, function myError(response) {
                    unblockUI_Page_Loading_Info();
                    myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                });
            } else {
                alert("Please fill required fields.");
            }
        };
        //.............................................................End Update..................................                    

        //..............................................Cancel Update B2B Basic Information.......................
        $scope.cancelUpdateB2BInformation = function () {
            window.location.href = $scope.backUrl;
        };
        //.........................................................End Cancel......................................  

        $scope.getB2BBasicInfo();

        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

        //................................................Country Select Box Selected Value Change Event..............
        $scope.CountrySelectedChange = function () {

            var CountryId = "";
            if ($scope.SelectedCountry.toString() == 'undefined')
                CountryId = "0";
            else
                CountryId = $scope.SelectedCountry.id;

            blockUI_Page_Loading_Info();
            $http({
                method: "GET",
                url: $scope.loadCityUrl,
                params: { "CountryId": CountryId }// 
            }).then(function mySuccess(response) {
                $scope.CityList = response.data.value;
                unblockUI_Page_Loading_Info();
            }, function myError(response) {
                unblockUI_Page_Loading_Info();
            });
        };
        //.....................................................End Selected Value Change Event.....................

        //....................................................Upload B2B Profile Picture..........................
        $scope.uploadB2BProfilePicture = function () {
            var imgFiles = $("#imgUploadImage").get(0).files;

            if (imgFiles.length == '0') {
                alert("Please select a image file.");
                $("#imgUploadImage").focus();
            } else {
                blockUI_Upload_Modal();

                var fd = new FormData();
                fd.append("ProfilePicture", imgFiles[0], imgFiles[0].name);
                fd.append("Id", $scope.b2bBasicInfo.Id);

                $http.post($scope.uploadProfilePictureUrl,
                    fd,
                    {
                        transformRequest: angular.identity,
                        //params: { "CompanyId": $scope.ChefId},
                        headers: { 'Content-Type': undefined }
                    }).then(function (response) {
                        if (response.data.success == true) {
                            $scope.imgImage = "/ApplicationImage/CompanyLogo/" + response.data.fileName;
                            $scope.imgProfilePicture = "/ApplicationImage/CompanyLogo/" + response.data.fileName;
                            myNotification(response.data.message, 'Upload Successfully', 'focus', 'la la-plus');
                        } else {
                            myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                        }
                        unblockUI_Upload_Modal();
                    }, function errorCallback(response) {
                        unblockUI_Upload_Modal();
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    });
            }
        };
        //............................................................End Upload...................................  

        //.......................................................Remove B2B Profile Picture.......................
        $scope.removeB2BProfilePicture = function () {
            if (confirm('Are you sure you want to delete?') == true) {
                blockUI_Page();
                $http({
                    method: "POST",
                    url: $scope.deleteProfilePictureUrl,
                    params: { "Id": $scope.b2bBasicInfo.Id }
                }).then(function mySuccess(response) {
                    if (response.data.success == true) {
                        $scope.imgProfilePicture = "../images/avatar.jpg";
                        myNotification(response.data.message, 'Delete Successfully', 'focus', 'la la-plus');
                    } else {
                        myNotification(response.data.message, 'Operation Failed!!!!', 'danger', 'la la-warning');
                    }
                    unblockUI_Page();
                }, function myError(response) {
                    unblockUI_Page();
                });
            }
        };
        //.............................................................End Remove..................................  
              

    }]);
























//function initSelectBox() {
//    $('#CityId').select2({
//        placeholder: "Select a city"
//    });

//    $('#CityId').on('select2:select', function (e) {
//        loadLocation($('#CityId').val());
//    });

//    $('#LocationId').select2({
//        placeholder: "Select a location"
//    });

//    $('#LocationId').on('select2:select', function (e) {

//    });

//}

//function loadChefInformation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#getChefUrl").val(),
//        data: { "Id": $("#Id").val() },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;
//            $("#Id").val(value.id);
//            $("#Name").val(value.name);
//            $("#Email").val(value.email);
//            $("#PhoneNumber").val(value.phoneNumber);
//            $("#MobileNumber").val(value.mobileNumber);
//            $("#Address").val(value.address);
//            $("#ZipCode").val(value.zipCode);
//            $("#Website").val(value.website);

//            $("#CityId").empty();
//            $.each(value.citySummaryModels,
//                function () {
//                    $("#CityId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#CityId").select2("val", [value.cityId]);

//            $("#LocationId").empty();
//            $.each(value.locationSummaryModels,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#LocationId").select2("val", [value.locationId]);

//            if (value.picture != null && value.picture != "")
//                $("#imgProfilePicture").attr("src", "/ApplicationImage/CompanyLogo/" + value.picture);

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load chef information.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function loadLocation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#loadLocationUrl").val(),
//        data: { "CityId": id },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;

//            $("#LocationId").empty();
//            $.each(value,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load location.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function formValidation() {
//    $("#btnSaveUpdate").on("click",
//        function () {
//            if ($("#save_information")[0].checkValidity()) {
//                saveData();
//            } else {
//                $("#save_information")[0].reportValidity();
//            }
//        });
//}

//function saveData() {
//    blockUI_Page_Saving_Info();
//    var fd = new FormData();
//    fd.append("Id", $("#Id").val());
//    fd.append("Name", $("#Name").val());
//    fd.append("Email", $("#Email").val());
//    fd.append("PhoneNumber", $("#PhoneNumber").val());
//    fd.append("MobileNumber", $("#MobileNumber").val());
//    fd.append("Address", $("#Address").val());
//    fd.append("LocationId", $('#LocationId').val());
//    fd.append("ZipCode", $("#ZipCode").val());
//    $.ajax({
//        type: "POST",
//        url: $("#saveChefInfoUrl").val(),
//        data: fd,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (data) {
//            if (data.success) {
//                jsonObj = [];
//                item = {};
//                item["ChefLogo"] = data.chefLogo;
//                item["ChefAddress"] = data.chefAddress;
//                item["DashboardUrl"] = data.redirectUrl;
//                item["ChefId"] = data.chefId;
//                jsonObj.push(item);
//                var jsonString = JSON.stringify(jsonObj);
//                sessionStorage.setItem("ChefBasicInfo", jsonString);

//                document.location.href = data.redirectUrl;
//            }
//            unblockUI_Page_Loading_Info();
//        },
//        failure: function (response) {
//            unblockUI_Page_Loading_Info();
//        },
//        error: function (XMLHttpRequest, textStatus) {
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}