﻿jQuery(document).ready(function () {
    loadDashboardurl();
    initMenu("Dashboard");
    getUsingPlan();
});

function getUsingPlan() {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getUsingPlanUrl").val(),
        data: { },
        dataType: "html",
        success: function (result) {
            var val = JSON.parse(result);
            var value = val.value;
            //$("#top_pricing_table").empty();
            $.each(val.value,
                function () {

                    var InvoiceNo = "";
                    if (this.noOfInvoices >= parseInt($("#unlimitedInvoiceLimit").val())) {
                        InvoiceNo = "Unlimited";
                    } else {
                        InvoiceNo = this.noOfInvoices;
                    }

                    var CustomerNo = "";
                    if (this.noOfCustomers >= parseInt($("#unlimitedCustomerLimit").val())) {
                        CustomerNo = "Unlimited";
                    } else {
                        CustomerNo = this.noOfCustomers;
                    }

                    var UserNo = "";
                    if (this.noOfUsers > parseInt($("#unlimitedUserLimit").val())) {
                        UserNo = "Unlimited";
                    } else {
                        UserNo = this.noOfUsers;
                    }

                    $("#top_pricing_table").append("<div class=\"m-pricing-table-4__top-item\">" +
                        "<h2 class=\"m-pricing-table-4__subtitle\">" + this.title + "</h2>" +
                        "<div class=\"m-pricing-table-4__features\">" +
                        "<span>"+ this.shortDescription+"</span><br>" +
                        "</div>" +
                        "    <span class=\"m-pricing-table-4__price\">" + this.currency + this.amount+"</span>" +
                        "    <span>/mo</span>" +
                        "    <div class=\"m-pricing-table-4__btn\">" +
                        "        <a href=\"Purchase/"+ this.id +"\" type=\"button\" class=\"btn m-btn--pill  btn-info m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg\">Select</a>" +
                        "    </div>" +
                        "    <div class=\"m-pricing-table-4__top-items-mobile\">" +
                        "        <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        "            <span>Number Of Invoice</span><br>" +
                        "                <span>" + this.noOfInvoices +"</span>" +
                        "   </div>" +
                        "          <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        "             <span>Number Of Customer</span><br>" +
                        "                <span>" + this.noOfCustomers +"</span>" +
                        "</div>" +
                        "           <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        "              <span>Number Of User</span><br>" +
                        "                 <span>" + this.noOfUsers +"</span>" +
                        "      </div>" +
                        "                     <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        "                        <span>Custom Logo</span><br>" +
                        "                           <span>" + this.customLogo +"</span>" +
                        "  </div>" +

                        //"                           <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        //"                              <span>No Of Allowed Template</span><br>" +
                        //"                                 <span>" + this.noOfAllowedTemplate +"</span>" +
                        //"    </div>" +

                        //"                           <div class=\"m-pricing-table-4__top-item-mobile\">" +
                        //"                              <span>Api Access</span><br>" +
                        //"                                 <span>" + this.apiAccess +"</span>" +
                        //"</div>" +
                        
                        "                                      <div class=\"m-pricing-table-4__top-btn\">" +
                        "                                         <a href=\"Purchase/"+ this.id +"\" type=\"button\" class=\"btn m-btn--pill  btn-info m-btn--wide m-btn--uppercase m-btn--bolder m-btn--lg\">Select</a>" +
                        "                                    </div>" +
                        "                               </div>" +
                        "                           </div>");

                    //$("#no_of_invoice").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfInvoices + "</div>");
                    //$("#no_of_customer").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfCustomers + "</div>");
                    //$("#no_of_user").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfUsers + "</div>");

                    if (this.noOfInvoices >= parseInt($("#unlimitedInvoiceLimit").val())) {
                        $("#no_of_invoice").append("<div class=\"m-pricing-table-4__bottom-item\">" + "Unlimited" + "</div>");
                    } else {
                        $("#no_of_invoice").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfInvoices + "</div>");
                    }

                    if (this.noOfCustomers >= parseInt($("#unlimitedCustomerLimit").val())) {
                        $("#no_of_customer").append("<div class=\"m-pricing-table-4__bottom-item\">" + "Unlimited" + "</div>");
                    } else {
                        $("#no_of_customer").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfCustomers + "</div>");
                    }

                    if (this.noOfUsers > parseInt($("#unlimitedUserLimit").val())) {
                        $("#no_of_user").append("<div class=\"m-pricing-table-4__bottom-item\">" + "Unlimited" + "</div>");
                    } else {
                        $("#no_of_user").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfUsers + "</div>");
                    }


                    $("#custom_logo").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.customLogo + "</div>");
                    //$("#no_of_allowed_template").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.noOfAllowedTemplate + "</div>");
                    //$("#api_access").append("<div class=\"m-pricing-table-4__bottom-item\">" + this.apiAccess + "</div>");

                    //alert(this.title);
                });

            //$('#company_name').text(value.companyName);
            //$('#company_address').text(value.companyAddress);
            //$('#InvoiceNo').val(value.invoiceNo);
            //$('.base_currency').text(value.baseCurrency);
            //$('#BaseCurrency').val(value.baseCurrency);

            //if (value.logo != null && value.logo != "")
            //    $("#company_logo").attr("src", "/ApplicationImage/CompanyLogo/" + value.logo);

            unblockUI_Page();
        },
        error: function (error) {
            unblockUI_Page();
        }
    });
}

