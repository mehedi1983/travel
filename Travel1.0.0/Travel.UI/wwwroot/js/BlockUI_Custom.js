﻿
//.........................................................Processing Sending................................
function blockUILogin() {
    mApp.block('.blockui_login_signup', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Login...'
    });
}

function blockUISignUp() {
    mApp.block('.blockui_login_signup', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Sign in...'
    });
}

function unBlockLoginSignupUI() {
    mApp.unblock('.blockui_login_signup');
}
//..................................................................End......................................






//.........................................................Processing Sending................................
function blockUI_Page() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Processing...'
    });
}

function unblockUI_Page() {
    mApp.unblockPage();
}
//..................................................................End......................................


//.............................................................Page Loading..................................
function blockUI_Page_Loading_Info() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Loading...'
    });
}

function unblockUI_Page_Loading_Info() {
    mApp.unblockPage();
}
//..................................................................End......................................


//.............................................................Page Saving..................................
function blockUI_Page_Saving_Info() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Saving...'
    });
}

function unblockUI_Page_Saving_Info() {
    mApp.unblockPage();
}
//..................................................................End......................................


//.............................................................Page Sending..................................
function blockUI_Page_Sending() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Sending...'
    });
}

function unblockUI_Page_Sending() {
    mApp.unblockPage();
}
//..................................................................End......................................


//--------------------------------------------------------------Saving Modal.................................
function blockUI_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Saving...'
        });
}

function unblockUI_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}
//..................................................................End......................................


//--------------------------------------------------------------Loading Modal.................................
function blockUI_Modal_Loading() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Loading...'
        });
}

function unblockUI_Modal_Loading() {
    mApp.unblock('.blockui_modal .modal-content');
}
//..................................................................End.......................................


//--------------------------------------------------------------Sending Modal.................................
function blockUI_Modal_Sending() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Sending...'
        });
}

function unblockUI_Modal_Sending() {
    mApp.unblock('.blockui_modal .modal-content');
}
//..................................................................End.......................................


//--------------------------------------------------------------Upload Modal..................................
function blockUI_Upload_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Uploading...'
        });
}

function unblockUI_Upload_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}
//..................................................................End.......................................

//.........................................................Processing Sending................................
function blockUI_Loading_Menu() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Loading Menu...'
    });
}

function unblockUI_Loading_Menu() {
    mApp.unblockPage();
}
//..................................................................End......................................