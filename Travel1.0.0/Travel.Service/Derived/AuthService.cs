﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Travel.Data.Entities;
using Travel.Data.Entities.B2B;
using Travel.Repositories;
using Travel.Repositories.Chef;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class AuthService: IAuthService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public AuthService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Employee"
        private Repository<B2BInformation> _B2BInformationRepository;
        #endregion

        #endregion

        #region "Master"

        public Repository<B2BInformation> B2BInformationRepository
        {
            get { return _B2BInformationRepository ?? (_B2BInformationRepository = new B2BInformationRepository(_context)); }
        }
        
        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
