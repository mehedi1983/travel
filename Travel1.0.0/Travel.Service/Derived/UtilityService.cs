﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Travel.Data.Entities;
using Travel.Data.Entities.Utility;
using Travel.Repositories;
using Travel.Repositories.Utility;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class UtilityService : IUtilityService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public UtilityService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        private Repository<Status> _StatusRepository;

        #endregion

        #region "Utility"

        public Repository<Status> StatusRepository
        {
            get { return _StatusRepository ?? (_StatusRepository = new StatusRepository(_context)); }
        }
        
        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
