﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Directory;
using Travel.Repositories;
using Travel.Repositories.Chef;
using Travel.Repositories.Directory;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class B2BInformationService : IB2BInformationService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public B2BInformationService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"
        private Repository<B2BInformation> _B2BInformationRepository;
        private Repository<City> _CityRepository;
        private Repository<Country> _CountryRepository;
        #endregion

        public Repository<B2BInformation> B2BInformationRepository
        {
            get { return _B2BInformationRepository ?? (_B2BInformationRepository = new B2BInformationRepository(_context)); }
        }

        public Repository<City> CityRepository
        {
            get { return _CityRepository ?? (_CityRepository = new CityRepository(_context)); }
        }

        public Repository<Country> CountryRepository
        {
            get { return _CountryRepository ?? (_CountryRepository = new CountryRepository(_context)); }
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
