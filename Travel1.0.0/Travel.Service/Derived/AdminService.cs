﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.B2B;
using Travel.Repositories;
using Travel.Repositories.Chef;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class AdminService : IAdminService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public AdminService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "B2B"
        private Repository<B2BInformation> _B2BInformationRepository;
        #endregion

        #endregion

        #region "B2B"

        public Repository<B2BInformation> B2BInformationRepository
        {
            get { return _B2BInformationRepository ?? (_B2BInformationRepository = new B2BInformationRepository(_context)); }
        }

        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
