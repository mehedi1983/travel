﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;
using Travel.Data.Entities;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Employee;
using Travel.Data.Entities.Utility;
using Travel.Repositories;
using Travel.Repositories.Directory;
using Travel.Repositories.Order;
using Travel.Repositories.Utility;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class EmployeeService : IEmployeeService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public EmployeeService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"
        private Repository<EmployeeInformation> _EmployeeInformationRepository;
        private Repository<City> _CityRepository;
        private Repository<Country> _CountryRepository;
        private Repository<Status> _StatusRepository;
        #endregion

        public Repository<EmployeeInformation> EmployeeInformationRepository
        {
            get { return _EmployeeInformationRepository ?? (_EmployeeInformationRepository = new EmployeeInformationRepository(_context)); }
        }

        public Repository<Status> StatusRepository
        {
            get { return _StatusRepository ?? (_StatusRepository = new StatusRepository(_context)); }
        }

        public Repository<City> CityRepository
        {
            get { return _CityRepository ?? (_CityRepository = new CityRepository(_context)); }
        }

        public Repository<Country> CountryRepository
        {
            get { return _CountryRepository ?? (_CountryRepository = new CountryRepository(_context)); }
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
