﻿using System;
using Travel.Data.Entities;
using Travel.Data.Entities.Employee;
using Travel.Repositories;
using Travel.Repositories.Order;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class UserService : IUserService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public UserService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Employee Information"
        private Repository<EmployeeInformation> _EmployeeInformationRepository;
        #endregion

        #endregion

        #region "Employee Information"

        public Repository<EmployeeInformation> EmployeeInformationRepository
        {
            get { return _EmployeeInformationRepository ?? (_EmployeeInformationRepository = new EmployeeInformationRepository(_context)); }
        }

        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
