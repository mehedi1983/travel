﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities;
using Travel.Data.Entities.B2B;
using Travel.Repositories;
using Travel.Repositories.Chef;
using Travel.Service.Interface;

namespace Travel.Service.Derived
{
    public class ReportService: IReportService
    {
        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public ReportService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        public Repository<B2BInformation> _B2BInformationRepository;
        
        public Repository<B2BInformation> B2BInformationRepository
        {
            get { return _B2BInformationRepository ?? (_B2BInformationRepository = new B2BInformationRepository(_context)); }
        }
        
        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
