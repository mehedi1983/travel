﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Directory;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IB2BInformationService
    {
        Repository<B2BInformation> B2BInformationRepository { get; }
        Repository<City> CityRepository { get; }
        Repository<Country> CountryRepository { get; }
        int Commit();
    }
}
