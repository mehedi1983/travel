﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities.B2B;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IAdminService
    {
        #region "Admin Information"
        Repository<B2BInformation> B2BInformationRepository { get; }
        #endregion

        int Commit();
    }
}
