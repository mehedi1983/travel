﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities.Employee;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IUserService
    {
        Repository<EmployeeInformation> EmployeeInformationRepository { get; }

        int Commit();
    }
}
