﻿
using Travel.Data.Entities.B2B;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IReportService
    {
        Repository<B2BInformation> B2BInformationRepository { get; }
        int Commit();
    }
}