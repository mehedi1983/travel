﻿using System;
using System.Collections.Generic;
using System.Text;
using Travel.Data.Entities.Utility;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IAdminSettingService
    {
        Repository<Status> StatusRepository { get; }

        int Commit();
    }
}
