﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;
using Travel.Data.Entities.Directory;
using Travel.Data.Entities.Employee;
using Travel.Data.Entities.Utility;
using Travel.Repositories;

namespace Travel.Service.Interface
{
    public interface IEmployeeService
    {
        Repository<EmployeeInformation> EmployeeInformationRepository { get; }
        Repository<City> CityRepository { get; }
        Repository<Country> CountryRepository { get; }
        Repository<Status> StatusRepository { get; }
        int Commit();
    }
}
