﻿using System;

namespace Travel.Business.Utility
{
    public class OperationStatus
    {

        public static string CHEF
        {
            get
            {
                const string val = "Chef";
                return val;
            }
        }

        public static string CUSTOMER
        {
            get
            {
                const string val = "Customer";
                return val;
            }
        }

        #region "Reward Point Table"

        public static string CUSTOMER_REGISTRATION
        {
            get
            {
                const string val = "Customer Registration";
                return val;
            }
        }

        public static string CHEF_REGISTRATION
        {
            get
            {
                const string val = "Chef Registration";
                return val;
            }
        }

        #endregion


        #region "Reset Password"

        public static string RESET_PASSWORD_INVALID_USER
        {
            get
            {
                const string val = "Invalid user.";
                return val;
            }
        }

        public static string RESET_PASSWORD_REQUEST_MESSAGE
        {
            get
            {
                const string val = "Password reset request sent successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_FAIL_MESSAGE
        {
            get
            {
                const string val = "Password fail to reset.";
                return val;
            }
        }

        public static string RESET_PASSWORD_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Password reset successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_EMAIL_SUBJECT
        {
            get
            {
                const string val = "Password reset request";
                return val;
            }
        }

        #endregion

        #region "Chance password"

        public static string CHANGE_PASSWORD_SUCCESS
        {
            get
            {
                const string val = "Password change successfully.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_FAIL
        {
            get
            {
                const string val = "Password fail to change.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_INCORRECT_CURRENT_PASSWORD
        {
            get
            {
                const string val = "Incorrect current password.";
                return val;
            }
        }

        #endregion

        public static string TRUE
        {
            get
            {
                const string val = "True";
                return val;
            }
        }

        public static string FALSE
        {
            get
            {
                const string val = "False";
                return val;
            }
        }

        public static string SYSTEM_GENERATED
        {
            get
            {
                const string val = "System Generated";
                return val;
            }
        }

        public static string CANCEL
        {
            get
            {
                const string val = "Cancel";
                return val;
            }
        }

        public static string CONFIRM
        {
            get
            {
                const string val = "Confirm";
                return val;
            }
        }

        public static string ALL
        {
            get
            {
                const string val = "All";
                return val;
            }
        }

        public static string ACTIVE
        {
            get
            {
                const string val = "Active";
                return val;
            }
        }

        public static string INACTIVE
        {
            get
            {
                const string val = "Inactive";
                return val;
            }
        }

        public static string DENY
        {
            get
            {
                const string val = "Deny";
                return val;
            }
        }

        public static string NEW
        {
            get
            {
                const string val = "N";
                return val;
            }
        }

        public static string MODIFY
        {
            get
            {
                const string val = "M";
                return val;
            }
        }

        public static string DELETE
        {
            get
            {
                const string val = "D";
                return val;
            }
        }

        public static string PENDING
        {
            get
            {
                const string val = "Pending";
                return val;
            }
        }

        public static string APPROVED
        {
            get
            {
                const string val = "Approved";
                return val;
            }
        }
        
        #region "Payment History"
        public static string PAYMENT_HISTORY_PAID
        {
            get
            {
                const string val = "Paid";
                return val;
            }
        }

        public static string PAYMENT_HISTORY_MARKED_SENT
        {
            get
            {
                const string val = "Marked Sent";
                return val;
            }
        }

        public static string PAYMENT_HISTORY_PAYMENT_RECEIPT
        {
            get
            {
                const string val = "Payment Receipt";
                return val;
            }
        }

        #endregion

        #region "Invoice History"

        public static string INVOICE_HISTORY_REMINDER
        {
            get
            {
                const string val = "Reminder";
                return val;
            }
        }

        public static string INVOICE_HISTORY_SENT
        {
            get
            {
                const string val = "Sent";
                return val;
            }
        }

        public static string INVOICE_HISTORY_DRAFT
        {
            get
            {
                const string val = "Draft";
                return val;
            }
        }

        public static string INVOICE_HISTORY_EDIT_DRAFT
        {
            get
            {
                const string val = "Edit Draft";
                return val;
            }
        }

        public static string INVOICE_HISTORY_OUTSTANDING
        {
            get
            {
                const string val = "Outstanding";
                return val;
            }
        }

        public static string INVOICE_HISTORY_EDIT_OUTSTANDING
        {
            get
            {
                const string val = "Edit Outstanding";
                return val;
            }
        }

        public static string INVOICE_HISTORY_PAID
        {
            get
            {
                const string val = "Paid";
                return val;
            }
        }

        public static string INVOICE_HISTORY_MARKED_SENT
        {
            get
            {
                const string val = "Marked Sent";
                return val;
            }
        }

        public static string INVOICE_HISTORY_COMMENTS_BY_CUSTOMER
        {
            get
            {
                const string val = "Comments By Customer";
                return val;
            }
        }

        public static string INVOICE_HISTORY_COMMENTS_BY_COMPANY
        {
            get
            {
                const string val = "Comments By Company";
                return val;
            }
        }

        #endregion

        #region "Customer"

        public static string CUSTOMER_HISTORY_INSERT
        {
            get
            {
                const string val = "Insert";
                return val;
            }
        }

        public static string CUSTOMER_HISTORY_EDIT
        {
            get
            {
                const string val = "Edit";
                return val;
            }
        }

        #endregion
    }
}