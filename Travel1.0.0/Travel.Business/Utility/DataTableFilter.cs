﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Travel.Model.Utility;

namespace Travel.Business.Utility
{
    public class DataTableFilter
    {
        public int GetTotalNoOfPages(int TotalRecord, int PerPage)
        {
            int NoOfPages = 0;
            decimal modular = (TotalRecord % PerPage);
            if (modular != 0)
                NoOfPages = (TotalRecord / PerPage) + 1;
            else
                NoOfPages = (TotalRecord / PerPage) + 1;
            return NoOfPages;
        }
        
        public List<T> CurrentPageItems<T>(List<T> footerLinkModels, int CurrentPageNo, int PerPage)
        {
            try
            {
                if (PerPage < footerLinkModels.Count && ((CurrentPageNo - 1) * PerPage) < footerLinkModels.Count)
                {
                    if ((((CurrentPageNo - 1) * PerPage) + PerPage) >= footerLinkModels.Count)
                        footerLinkModels = footerLinkModels.GetRange(((CurrentPageNo - 1) * PerPage), (footerLinkModels.Count - ((CurrentPageNo - 1) * PerPage)));
                    else
                        footerLinkModels = footerLinkModels.GetRange(((CurrentPageNo - 1) * PerPage), PerPage);
                }
            }
            catch (Exception e)
            {
                //throw;
            }
            return footerLinkModels;
        }

        public meta GetFormData(IFormCollection formFields)
        {
            meta FormData = new meta();
            FormData.SearchString = "";
            foreach (var keyValuePair in formFields.ToList())
            {
                if (keyValuePair.Key == "pagination[page]")
                {
                    FormData.page = TypeUtil.convertToInt(keyValuePair.Value);
                }
                else if (keyValuePair.Key == "pagination[pages]")
                {
                    FormData.pages = TypeUtil.convertToInt(keyValuePair.Value);
                }
                else if (keyValuePair.Key == "pagination[perpage]")
                {
                    FormData.perpage = TypeUtil.convertToInt(keyValuePair.Value);
                }
                else if (keyValuePair.Key == "pagination[total]")
                {
                    FormData.total = TypeUtil.convertToInt(keyValuePair.Value);
                }
                else if (keyValuePair.Key == "sort[field]")
                {
                    FormData.field = keyValuePair.Value;
                }
                else if (keyValuePair.Key == "sort[sort]")
                {
                    FormData.sort = keyValuePair.Value;
                }
                else if (keyValuePair.Key == "query[generalSearch]")
                {
                    FormData.SearchString = keyValuePair.Value;
                }
                else if (keyValuePair.Key == "query[Status]")
                {
                    FormData.Status = keyValuePair.Value;
                }
            }
            return FormData;
        }
    }
}
