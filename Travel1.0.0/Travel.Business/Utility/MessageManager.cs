﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Travel.Business.Utility
{
    public class MessageManager
    {
        #region "Reset Password"

        public static string RESET_PASSWORD_INVALID_USER
        {
            get
            {
                const string val = "Invalid user.";
                return val;
            }
        }

        public static string RESET_PASSWORD_REQUEST_MESSAGE
        {
            get
            {
                const string val = "Password reset request sent successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_FAIL_MESSAGE
        {
            get
            {
                const string val = "Password fail to reset.";
                return val;
            }
        }

        public static string RESET_PASSWORD_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Password reset successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_EMAIL_SUBJECT
        {
            get
            {
                const string val = "Password reset request";
                return val;
            }
        }

        #endregion

        #region "Chance password"

        public static string CHANGE_PASSWORD_SUCCESS
        {
            get
            {
                const string val = "Password change successfully.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_FAIL
        {
            get
            {
                const string val = "Password fail to change.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_INCORRECT_CURRENT_PASSWORD
        {
            get
            {
                const string val = "Incorrect current password.";
                return val;
            }
        }

        #endregion

        #region "User Management"

        public static string INSERT_USER_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "User inserted successfully.";
                return val;
            }
        }

        public static string INSERT_USER_FAIL_MESSAGE
        {
            get
            {
                const string val = "User fail to insert.";
                return val;
            }
        }

        public static string DELETE_USER_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "User deleted successfully.";
                return val;
            }
        }

        public static string DELETE_USER_FAIL_MESSAGE
        {
            get
            {
                const string val = "User fail to delete.";
                return val;
            }
        }


        #endregion

        #region "Employee"

        public static string EMPLOYEE_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee save successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail to save.";
                return val;
            }
        }

        public static string EMPLOYEE_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee edited successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail edit.";
                return val;
            }
        }

        public static string EMPLOYEE_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee deleted successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail to delete.";
                return val;
            }
        }

        public static string EMPLOYEE_UPLOAD_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee picture uploaded successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee picture fail to uploaded.";
                return val;
            }
        }

        public static string EMPLOYEE_REMOVE_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee picture remove successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_REMOVE_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee picture fail to remove.";
                return val;
            }
        }

        public static string EMPLOYEE_IMAGE_COPY_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee picture copy successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_IMAGE_COPY_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee picture fail to copy.";
                return val;
            }
        }

        #endregion        

        #region "Company"

        public static string COMPANY_BASIC_INFORMATION_SAVE_MESSAGE
        {
            get
            {
                const string val = "Company basic information saved successfully.";
                return val;
            }
        }

        public static string COMPANY_BASIC_INFORMATION_FAIL_MESSAGE
        {
            get
            {
                const string val = "Company basic informatior fail to save.";
                return val;
            }
        }

        public static string COMPANY_UPLOAD_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Company's image uploaded successfully.";
                return val;
            }
        }

        public static string COMPANY_UPLOAD_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Company's image fail to upload.";
                return val;
            }
        }
        
        public static string COMPANY_REMOVE_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Company's image remove successfully.";
                return val;
            }
        }

        public static string COMPANY_REMOVE_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Company's image fail to remove.";
                return val;
            }
        }

        #endregion
        
    }
}
