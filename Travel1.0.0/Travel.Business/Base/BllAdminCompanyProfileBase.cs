﻿using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllAdminCompanyProfileBase
    {
        public BllAdminCompanyProfileBase(IAdminService iAdminService, IModelFactory iModelFactory)
        {
            IAdminService = iAdminService;
            IModelFactory = iModelFactory;
        }

        protected IAdminService IAdminService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}