﻿
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllEmployeeBase
    {
        public BllEmployeeBase(IEmployeeService iEmployeeService, IModelFactory iModelFactory)
        {
            IEmployeeService = iEmployeeService;
            IModelFactory = iModelFactory;
        }

        protected IEmployeeService IEmployeeService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}