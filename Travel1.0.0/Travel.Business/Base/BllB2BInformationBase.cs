﻿using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllB2BInformationBase
    {
        public BllB2BInformationBase(IB2BInformationService iB2BInformationService, IModelFactory iModelFactory)
        {
            IB2BInformationService = iB2BInformationService;
            IModelFactory = iModelFactory;
        }

        protected IB2BInformationService IB2BInformationService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}