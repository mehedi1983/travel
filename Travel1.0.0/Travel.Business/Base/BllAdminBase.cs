﻿using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllAdminBase
    {
        public BllAdminBase(IAdminSettingService iAdminSettingService, IModelFactory iModelFactory)
        {
            IAdminSettingService = iAdminSettingService;
            IModelFactory = iModelFactory;
        }

        protected IAdminSettingService IAdminSettingService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}