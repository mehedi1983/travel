﻿using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllAuthBase
    {
        public BllAuthBase(IAuthService iAuthService, IModelFactory iModelFactory)
        {
            IAuthService = iAuthService;
            IModelFactory = iModelFactory;
        }

        protected IAuthService IAuthService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}