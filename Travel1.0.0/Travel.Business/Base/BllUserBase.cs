﻿
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllUserBase
    {
        public BllUserBase(IUserService iUserService, IModelFactory iModelFactory)
        {
            IUserService = iUserService;
            IModelFactory = iModelFactory;
        }

        protected IUserService IUserService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}