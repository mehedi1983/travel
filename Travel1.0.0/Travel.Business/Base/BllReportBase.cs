﻿
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Base
{
    public class BllReportBase
    {
        public BllReportBase(IReportService iAuthService, IModelFactory iModelFactory)
        {
            IReportService = iAuthService;
            IModelFactory = iModelFactory;
        }

        protected IReportService IReportService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}