﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Data.Entities.B2B;
using Travel.Model.B2B;
using Travel.Model.Directory;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllB2BInformation : BllB2BInformationBase
    {
        private readonly IConfiguration _configuration;
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllB2BInformation(IB2BInformationService iB2BInformationService, IModelFactory iModelFactory, IConfiguration Configuration) :
            base(iB2BInformationService, iModelFactory)
        {
            _configuration = Configuration;
        }

        public async Task<B2BInformationModel> ChangeBasicInformation(B2BInformationModel b2BInformationModel)
        {
            var basicInformation = IB2BInformationService.B2BInformationRepository.Get(b2BInformationModel.Id);
            basicInformation.Name = b2BInformationModel.Name;
            basicInformation.Address = b2BInformationModel.Address;
            basicInformation.MobileNumber = b2BInformationModel.MobileNumber;
            basicInformation.PhoneNumber = b2BInformationModel.PhoneNumber;
            basicInformation.CountryId = b2BInformationModel.CountryId;
            basicInformation.CityId = b2BInformationModel.CityId;
            basicInformation.ZipCode = b2BInformationModel.ZipCode;
            basicInformation.ContactPerson = b2BInformationModel.ContactPerson;
            basicInformation.ContactPersonDesignation = b2BInformationModel.ContactPersonDesignation;
            basicInformation.Email = b2BInformationModel.Email;
            basicInformation.PhoneNumber = b2BInformationModel.PhoneNumber;
            basicInformation.MobileNumber = b2BInformationModel.MobileNumber;
            basicInformation.Fax = b2BInformationModel.Fax;
            basicInformation.Website = b2BInformationModel.Website;
            basicInformation.NatureOfBusiness = b2BInformationModel.NatureOfBusiness;
            basicInformation.LicenseNo = b2BInformationModel.LicenseNo;
            basicInformation.LicenseCopy = b2BInformationModel.LicenseCopy;
            basicInformation.Status = b2BInformationModel.Status;
            basicInformation.ModifiedBy = b2BInformationModel.CreatedBy;
            basicInformation.ModifiedDate = b2BInformationModel.CreatedDate;
            IB2BInformationService.B2BInformationRepository.Update(basicInformation);
            IB2BInformationService.Commit();
            //b2BInformationModel.Logo = basicInformation.Logo;
            return b2BInformationModel;
        }

        public async Task<B2BInformation> UpdateB2BLogo(string FileName, string CompanyId, string ModifyBy, DateTime ModifyDate)
        {
            var val = IB2BInformationService.B2BInformationRepository.Get(CompanyId);
            val.Logo = FileName;
            val.ModifiedBy = ModifyBy;
            val.ModifiedDate = ModifyDate;
            IB2BInformationService.B2BInformationRepository.Update(val);
            IB2BInformationService.Commit();
            return val;
        }

        public async Task<B2BInformationModel> LoadB2BInformation(string Id)
        {
            return IModelFactory.Create(IB2BInformationService.B2BInformationRepository.Get(Id));
        }

        public async Task<B2BWithLocationListModel> LoadB2BInformationWithCityAndLocationList(string Id)
        {
            var b2BWithCityListModel = new B2BWithLocationListModel();
            var val = IB2BInformationService.B2BInformationRepository.Get(Id);

            b2BWithCityListModel.Id = val.Id;
            b2BWithCityListModel.Name = val.Name;
            b2BWithCityListModel.Address = val.Address;
            b2BWithCityListModel.CountryId = val.CountryId;
            b2BWithCityListModel.CountryName = val.Country != null ? val.Country.Name : "";
            b2BWithCityListModel.CityId = val.CityId;
            b2BWithCityListModel.CityName = val.City != null ? val.City.Name : "";
            b2BWithCityListModel.ZipCode = val.ZipCode;
            b2BWithCityListModel.ContactPerson = val.ContactPerson;
            b2BWithCityListModel.ContactPersonDesignation = val.ContactPersonDesignation;
            b2BWithCityListModel.Email = val.Email;
            b2BWithCityListModel.PhoneNumber = val.PhoneNumber;
            b2BWithCityListModel.MobileNumber = val.MobileNumber;
            b2BWithCityListModel.Fax = val.Fax;
            b2BWithCityListModel.Website = val.Website;
            b2BWithCityListModel.NatureOfBusiness = val.NatureOfBusiness;
            b2BWithCityListModel.LicenseNo = val.LicenseNo;
            b2BWithCityListModel.LicenseCopy = val.LicenseCopy;
            b2BWithCityListModel.Logo = val.Logo;
            b2BWithCityListModel.Status = val.Status;
            b2BWithCityListModel.CountrySummaryModels = IB2BInformationService.CountryRepository.Get().Select(s => new CountrySummaryModel
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            b2BWithCityListModel.CitySummaryModels = IB2BInformationService.CityRepository.GetData(gd => gd.CountryId == b2BWithCityListModel.CountryId).Select(s => new CitySummaryModel
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            return b2BWithCityListModel;
        }

        public async Task<bool> RemoveB2BLogo(string Id)
        {
            var val = IB2BInformationService.B2BInformationRepository.Get(Id);
            val.Logo = null;
            IB2BInformationService.B2BInformationRepository.Update(val);
            IB2BInformationService.Commit();
            return true;
        }

        public async Task<List<CountrySummaryModel>> LoadCountry()
        {
            var countryList = IB2BInformationService.CountryRepository
                .GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id != "0")
                .Select(s => new CountrySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            countryList.Insert(0, new CountrySummaryModel { Id = "", Name = "-Select-" });
            return countryList;
        }

        public async Task<List<CitySummaryModel>> LoadCity(string CountryId)
        {
            var cityList = IB2BInformationService.CityRepository
                .GetData(gd => gd.CountryId== CountryId)
                .Select(s => new CitySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            return cityList;
        }

        public async Task<DTSearchReturnContainer> B2BSummary(int PageSize, int PageNo, string SearchString, string FilterByStatus)
        {
            IEnumerable<B2BInformation> tempSearch = new List<B2BInformation>();
            if (FilterByStatus == OperationStatus.ACTIVE || FilterByStatus == OperationStatus.INACTIVE)
                tempSearch = IB2BInformationService.B2BInformationRepository.GetData(gd => gd.Status == FilterByStatus);
            else if (FilterByStatus == OperationStatus.ALL)
                tempSearch = IB2BInformationService.B2BInformationRepository.GetData();

            if (SearchString != "" && SearchString != null)
                tempSearch = tempSearch.Where(w => (w.Id + w.Name + w.MobileNumber + w.Email).ToUpper().Contains(SearchString.ToUpper()));
            var finalSearch = _dataTableFilter.CurrentPageItems(tempSearch.ToList(), PageNo, PageSize);

            var B2BSummaryViewModelList = finalSearch
                .Select(s => new B2BSummaryViewModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Email = s.Email,
                    MobileNumber = s.MobileNumber,
                    JoinDate = s.CreatedDate,
                    Logo = s.Logo,
                    LicenseNo = s.LicenseNo,
                    LicenseCopy = s.LicenseCopy,
                    Country = s.Country != null ? s.Country.Name : "",
                    City = s.City != null ? s.City.Name : "",
                    Status = s.Status
                }).ToList();

            DTSearchReturnContainer dtSearchReturnContainer = new DTSearchReturnContainer();
            dtSearchReturnContainer.TotalRowsCount = tempSearch.Count();
            dtSearchReturnContainer.Data = B2BSummaryViewModelList;
            return dtSearchReturnContainer;
        }

        public async Task<B2BDashboardModel> LoadB2BDashboardInfo(string B2BId)
        {
            B2BDashboardModel chefDashboardModel = new B2BDashboardModel();
            var val = IB2BInformationService.B2BInformationRepository.Get(B2BId);
            chefDashboardModel.B2BInformationModel = IModelFactory.Create(val);
            return chefDashboardModel;
        }

    }
}