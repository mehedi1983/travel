﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllAdmin : BllAdminBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();
        private IConfiguration _configuration;

        public BllAdmin(IAdminSettingService iAdminSettingService, IModelFactory iModelFactory) :
            base(iAdminSettingService, iModelFactory)
        {
        }

        public async Task<List<StatusModel>> GetStatus()
        {
            return IAdminSettingService.StatusRepository.GetData().Select(IModelFactory.Create).ToList();           
        }

    }
}