﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Data.Entities.Employee;
using Travel.Model.Directory;
using Travel.Model.Employee;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllEmployee : BllEmployeeBase
    {
        private readonly IConfiguration _configuration;
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllEmployee(IEmployeeService iEmployeeService, IModelFactory iModelFactory, IConfiguration Configuration) :
            base(iEmployeeService, iModelFactory)
        {
            _configuration = Configuration;
        }
        
        public async Task<EmployeeDefaultValueModel> EmployeeDefaultValue()
        {
            EmployeeDefaultValueModel employeeDefaultValueModel = new EmployeeDefaultValueModel();

            employeeDefaultValueModel.Countries = IEmployeeService.CountryRepository.Get().Select(s =>
                new CountrySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();

            employeeDefaultValueModel.Status = IEmployeeService.StatusRepository.Get().Select(s=> new StatusModel
            {
                Id = s.Id,
                Value = s.Value
            }).ToList();

            return employeeDefaultValueModel;
        }

        public async Task<string> GetEmployeeId()
        {
            var EmployeeId = "";
            var val = IEmployeeService.EmployeeInformationRepository.Get().ToList();
            if (val.Count > 0)
            {
                EmployeeId = "EM" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(2, 5)).ToList().Max()) + 1);
            }
            else
            {
                EmployeeId = "EM10000";
            }
            return EmployeeId;
        }

        public async Task<EmployeeWithLocationListModel> LoadEmployeeInformationWithCityAndLocationList(string Id)
        {
            var employeeWithLocationListModel = new EmployeeWithLocationListModel();
            var val = IEmployeeService.EmployeeInformationRepository.Get(Id);

            employeeWithLocationListModel.Id = val.Id;
            employeeWithLocationListModel.Email = val.Email;
            employeeWithLocationListModel.MobileNumber = val.MobileNo;
            employeeWithLocationListModel.Name = val.Name;
            employeeWithLocationListModel.Address = val.Address;
            employeeWithLocationListModel.ZipCode = val.ZipCode;
            employeeWithLocationListModel.CountryId = val.CountryId;
            employeeWithLocationListModel.CountryName = val.Country != null ? val.Country.Name : "";
            employeeWithLocationListModel.CityId = val.CityId != null ? val.City.Id : "";
            employeeWithLocationListModel.CityName = val.City != null ? val.City.Name : "";
            employeeWithLocationListModel.Picture = val.ProfilePicture;
            employeeWithLocationListModel.Status = val.Status;
            employeeWithLocationListModel.CountrySummaryModels = IEmployeeService.CountryRepository.Get().Select(s =>
                new CountrySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
            employeeWithLocationListModel.CitySummaryModels = IEmployeeService.CityRepository
                .GetData(gd => gd.CountryId == employeeWithLocationListModel.CountryId).Select(s => new CitySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
            employeeWithLocationListModel.StatusSummaryModels = IEmployeeService.StatusRepository.Get().Select(s => new StatusModel
            {
                Id = s.Id,
                Value = s.Value
            }).ToList();

            return employeeWithLocationListModel;
        }

        public async Task<EmployeeInformationModel> InsertUpdate(EmployeeInformationModel employeeInformationModel)
        {
            EmployeeInformation employeeInformation = new EmployeeInformation();
            if (employeeInformationModel.Id == null || employeeInformationModel.Id == "")
            {
                employeeInformation = IModelFactory.Create(employeeInformationModel);
                employeeInformation.Id = await GetEmployeeId();
                employeeInformation.Status = employeeInformationModel.Status != null
                    ? JsonConvert.DeserializeObject<DropDownModel>(employeeInformationModel.Status).Value
                    : OperationStatus.ACTIVE;
                employeeInformation.RecStatus = OperationStatus.NEW;
                IEmployeeService.EmployeeInformationRepository.Insert(employeeInformation);
                IEmployeeService.Commit();
                employeeInformationModel.Id = employeeInformation.Id;
            }
            else
            {
                employeeInformation = IEmployeeService.EmployeeInformationRepository.GetData(gd => gd.Id == employeeInformationModel.Id).FirstOrDefault();

                employeeInformation.Name = employeeInformationModel.Name;
                employeeInformation.Email = employeeInformationModel.Email;
                employeeInformation.Address = employeeInformationModel.Address;
                employeeInformation.CountryId = employeeInformationModel.CountryId;
                employeeInformation.CityId = employeeInformationModel.CityId;
                employeeInformation.MobileNo = employeeInformationModel.MobileNo;
                employeeInformation.Status = employeeInformationModel.Status != null
                    ? JsonConvert.DeserializeObject<DropDownModel>(employeeInformationModel.Status).Value
                    : OperationStatus.ACTIVE;
                employeeInformation.ModifiedBy = employeeInformationModel.CreatedBy;
                employeeInformation.ModifiedDate = employeeInformationModel.CreatedDate;
                employeeInformation.RecStatus = OperationStatus.MODIFY;
                IEmployeeService.EmployeeInformationRepository.Update(employeeInformation);
                IEmployeeService.Commit();
            }
            return employeeInformationModel;
        }

        public async Task<EmployeeSummaryModel> ChangeBasicInformation(EmployeeSummaryModel employeeSummaryModel)
        {
            var basicInformation = IEmployeeService.EmployeeInformationRepository.Get(employeeSummaryModel.Id);
            basicInformation.Name = employeeSummaryModel.Name;
            basicInformation.MobileNo = employeeSummaryModel.MobileNumber;
            basicInformation.FatherName = employeeSummaryModel.PhoneNumber;
            basicInformation.MotherName = employeeSummaryModel.Website;
            basicInformation.Address = employeeSummaryModel.Address;
            basicInformation.ZipCode = employeeSummaryModel.ZipCode;
            basicInformation.CountryId = employeeSummaryModel.CountryId;
            basicInformation.CityId = employeeSummaryModel.CityId;
            IEmployeeService.EmployeeInformationRepository.Update(basicInformation);
            IEmployeeService.Commit();
            employeeSummaryModel.Picture = basicInformation.ProfilePicture;
            return employeeSummaryModel;
        }

        public async Task<EmployeeInformation> UpdateEmployeeLogo(string FileName, string CompanyId, string ModifyBy,
            DateTime ModifyDate)
        {
            var val = IEmployeeService.EmployeeInformationRepository.Get(CompanyId);
            val.ProfilePicture = FileName;
            val.ModifiedBy = ModifyBy;
            val.ModifiedDate = ModifyDate;
            IEmployeeService.EmployeeInformationRepository.Update(val);
            IEmployeeService.Commit();
            return val;
        }

        public async Task<EmployeeInformationModel> LoadEmployeeInformation(string Id)
        {
            return IModelFactory.Create(IEmployeeService.EmployeeInformationRepository.Get(Id));
        }

        public async Task<bool> RemoveEmployeeLogo(string Id)
        {
            var val = IEmployeeService.EmployeeInformationRepository.Get(Id);
            val.ProfilePicture = null;
            IEmployeeService.EmployeeInformationRepository.Update(val);
            IEmployeeService.Commit();
            return true;
        }

        public async Task<List<CountrySummaryModel>> LoadCountry()
        {
            var countryList = IEmployeeService.CountryRepository
                .GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id != "0")
                .Select(s => new CountrySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            countryList.Insert(0, new CountrySummaryModel { Id = "", Name = "-Select-"});
            return countryList;
        }

        public async Task<List<CitySummaryModel>> LoadCity(string CountryId)
        {
            var locationList = IEmployeeService.CityRepository
                .GetData(gd => gd.CountryId == CountryId)
                .Select(s => new CitySummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            return locationList;
        }

        public async Task<DTSearchReturnContainer> EmployeeSummary(int PageSize, int PageNo, string SearchString,
            string FilterByStatus)
        {
            IEnumerable<EmployeeInformation> tempSearch = new List<EmployeeInformation>();
            if (FilterByStatus == OperationStatus.ACTIVE || FilterByStatus == OperationStatus.INACTIVE)
                tempSearch = IEmployeeService.EmployeeInformationRepository.GetData(gd => gd.Status == FilterByStatus);
            else if (FilterByStatus == OperationStatus.ALL)
                tempSearch = IEmployeeService.EmployeeInformationRepository.GetData();

            if (SearchString != "" && SearchString != null)
                tempSearch = tempSearch.Where(w =>
                    (w.Id + w.Name + w.MobileNo + w.Email).ToUpper().Contains(SearchString.ToUpper()));
            var finalSearch = _dataTableFilter.CurrentPageItems(tempSearch.ToList(), PageNo, PageSize);

            var ChefSummaryViewModelList = finalSearch
                .Select(s => new EmployeeSummaryViewModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Email = s.Email,
                    MobileNumber = s.MobileNo,
                    JoinDate = s.CreatedDate,
                    Picture = s.ProfilePicture,
                    CountryName = s.Country != null ? s.Country.Name : "",
                    CityName = s.City != null ? s.City.Name : "",
                    Address = s.Address,
                    Status = s.Status
                }).ToList();

            var dtSearchReturnContainer = new DTSearchReturnContainer();
            dtSearchReturnContainer.TotalRowsCount = tempSearch.Count();
            dtSearchReturnContainer.Data = ChefSummaryViewModelList;
            return dtSearchReturnContainer;
        }
        
    }
}