﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Data.Entities.B2B;
using Travel.Data.Entities.Identity;
using Travel.Model.B2B;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllAuth : BllAuthBase
    {
        private readonly IConfiguration _configuration;
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllAuth(IAuthService iAuthService, IModelFactory iModelFactory, IConfiguration Configuration) : base(
            iAuthService, iModelFactory)
        {
            _configuration = Configuration;
        }

        public async Task<List<B2BInformationModel>> GetB2BInformation()
        {
            var val = IAuthService.B2BInformationRepository.Get().ToList().Select(IModelFactory.Create).ToList();
            return val;
        }

        public async Task<B2BInformation> GetB2BInformation(string id)
        {
            var val = IAuthService.B2BInformationRepository.Get(id);
            return val;
        }

        public async Task<string> GetB2BInformationId()
        {
            var EmployeeId = "";
            var val = IAuthService.B2BInformationRepository.Get();
            if (val.Count > 0)
                EmployeeId = "AGN-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                EmployeeId = "AGN-100000";
            return EmployeeId;
        }

        public async Task<B2BInformationModel> SaveB2BAdminUser(B2BInformationModel chefInformationModel)
        {
            var fLModel = IModelFactory.Create(chefInformationModel);
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IAuthService.B2BInformationRepository.Insert(fLModel);
            IAuthService.Commit();
            return chefInformationModel;
        }

        public void SendSMS(string Receiver, string Message)
        {
            var url = _configuration.GetSection("SmsApiInformation")["WEB_URL"];
            var appid = _configuration.GetSection("SmsApiInformation")["API_ID"];
            var apikey = _configuration.GetSection("SmsApiInformation")["API_KEY"];
            var Sender = _configuration.GetSection("SmsApiInformation")["SENDER"];

            var json = @"{
            ""sender"":  ""+821023860974"",
            ""receivers"": [""+8801817000339""],
            ""content"": ""test""}";

            //string json = @"{
            //""sender"":  Sender,
            //""receivers"": ["'+ Receiver + '"],
            //""content"": "" + Message + ""}";


            var client = new WebClient();
            var creds = new NetworkCredential(appid, apikey);
            client.Credentials = creds;
            client.Headers[HttpRequestHeader.ContentType] = "application/json; charset=utf-8";

            try
            {
                var response = client.UploadString(url, json);
                Console.WriteLine(response);
            }
            catch (WebException e)
            {
                var status = ((HttpWebResponse)e.Response).StatusCode;
                Console.WriteLine("{0}", (int)status);
                Console.WriteLine("{0}", status.ToString());
            }

            //var vv = IAuthService.AppApplicationUserRepository.Get().ToList();
        }

        public async Task<bool> Delete(string Id)
        {
            IAuthService.B2BInformationRepository.DeleteById(Id);
            if (IAuthService.Commit() == 1)
                return true;
            return false;
        }

        public async Task<DTReturnContainer> GetUserList(IFormCollection formFields,
            List<ApplicationUser> applicationUsers)
        {
            var Result = applicationUsers;
            var SearchedValue = new List<ApplicationUser>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => w.UserName.ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "userName")
                    Result = Result.OrderByDescending(ob => ob.UserName).ToList();
            }
            else if (filterData.sort == "userName")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.UserName).ToList();
            }

            #endregion

            SearchedValue = Result;
            filterData.total = SearchedValue.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(SearchedValue.Count, filterData.perpage);
            SearchedValue = _dataTableFilter.CurrentPageItems(SearchedValue, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = SearchedValue;
            return container;
        }
        
    }
}