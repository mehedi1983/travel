﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Data.Entities.Identity;
using Travel.Model.Employee;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllUser : BllUserBase
    {
        private readonly IConfiguration _configuration;
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();
        private readonly UserManager<ApplicationUser> _userManager;

        public BllUser(UserManager<ApplicationUser> userManager, IUserService iUserService, IModelFactory iModelFactory) : base(
            iUserService, iModelFactory)
        {
            _userManager = userManager;
        }

        public async Task<DTSearchReturnContainer> GetList(int PageSize, int PageNo, string SearchString, string FilterByStatus)
        {
            var Users=_userManager.Users.ToList();
            if (SearchString != "" && SearchString != null)
                Users = Users.Where(w => (w.UserName + w.DisplayName).ToUpper().Contains(SearchString.ToUpper())).ToList();

            Users = Users.Where(w => w.B2BId == null && w.EmployeeId != null).ToList();

            var finalSearch = _dataTableFilter.CurrentPageItems(Users.ToList(), PageNo, PageSize);
            DTSearchReturnContainer dtSearchReturnContainer = new DTSearchReturnContainer();
            dtSearchReturnContainer.TotalRowsCount = Users.Count();
            dtSearchReturnContainer.Data = finalSearch;
            return dtSearchReturnContainer;
        }

        public async Task<List<EmployeeDropDownSummaryModel>> GetEmployeeInformation()
        {
            var val = IUserService.EmployeeInformationRepository.Get().Select(s=> new EmployeeDropDownSummaryModel
            {
                Id = s.Id,
                Email = s.Email,
                Name = s.Name
            }).ToList();
            return val;
        }
                        
    }
}