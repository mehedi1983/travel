﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Model.Factories;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllReport : BllReportBase
    {
        private IConfiguration _configuration;

        public BllReport(IReportService iReportService, IModelFactory iModelFactory, IConfiguration Configuration) :
            base(iReportService, iModelFactory)
        {
            _configuration = Configuration;
        }
        
    }
}