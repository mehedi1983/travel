﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Travel.Business.Base;
using Travel.Business.Utility;
using Travel.Model.B2B;
using Travel.Model.Factories;
using Travel.Model.Utility;
using Travel.Service.Interface;

namespace Travel.Business.Derived
{
    public class BllAdminCompanyProfile : BllAdminCompanyProfileBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();
        private IConfiguration _configuration;

        public BllAdminCompanyProfile(IAdminService iAdminService, IModelFactory iModelFactory) :
            base(iAdminService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetCompanyInformationList(IFormCollection formFields)
        {
            var Result = IAdminService.B2BInformationRepository.Get().ToList();
            var tagList = new List<B2BInformationModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against Email verification status"

            if (filterData.Status == OperationStatus.TRUE || filterData.Status == OperationStatus.FALSE)
            {
                var status = Convert.ToBoolean(filterData.Status);
                Result = Result.Where(w => w.ApplicationUsers.FirstOrDefault().EmailConfirmed == status).ToList();
            }

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "businessName")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "email")
                    Result = Result.OrderByDescending(ob => ob.Email).ToList();
                else if (filterData.field == "mobileNumber")
                    Result = Result.OrderByDescending(ob => ob.MobileNumber).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "businessName")
                    Result = Result.OrderBy(ob => ob.Name.ToString()).ToList();
                else if (filterData.field == "email")
                    Result = Result.OrderBy(ob => ob.Email).ToList();
                else if (filterData.field == "mobileNumber")
                    Result = Result.OrderBy(ob => ob.MobileNumber).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            tagList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = tagList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(tagList.Count, filterData.perpage);
            tagList = _dataTableFilter.CurrentPageItems(tagList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = tagList;
            return container;
        }
    }
}